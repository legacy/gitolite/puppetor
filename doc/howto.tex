\documentclass{article}%
\usepackage{graphicx}%
\usepackage{url}%
\usepackage{msc}%
\begin{document}%
%
\author{karsten.loesing@gmx.net}%
\title{PuppeTor\\A Java-based Tor Simulator\\User's Guide}%
\maketitle%
%

\section{Introduction}

PuppeTor\footnote{The name signifies the metaphor of a puppeteer playing with
puppets like this framework plays with Tor processes.} is a Java framework that
facilitates the
configuration of a set of local Tor processes and the execution of automatic
tests based on these processes. The intention is to make it easier for
developers to
analyze Tor's behavior in arbitrary network settings and to measure the
effects of changes to the Tor source code. Due to the automation of
configuration and execution, these experiments can be done in an unsupervised 
batch fashion.

An application that makes use of this framework starts with setting up a set of
pre-defined Tor processes: proxy, router, and directory. Though these
configurations should work in most settings, they can be altered by adding or
removing configuration entries. After deciding whether the processes shall
either create a private Tor network, or connect to the public Tor network,
processes are started. Now the application can start clients and servers and
perform requests using the local processes. In doing so, it can measure time
intervals between events originating from Tor processes and it can synchronize 
with such events.

There are two typical situations in which this framework can be useful:
\begin{enumerate}
\item Developers need to oversee the effects of their changes to the source code.
Therefore, it is useful to have a clean setting of Tor nodes in a private
network, so that all nodes are under full control of the developer.
\item Developers might want to measure the real-world performance of certain Tor
operations. Hence, they can set up nodes at the edge of the public Tor network
and conduct performance measurements, maybe in a batch of some hundreds or
thousands of runs.
\end{enumerate}

Of course, the applications described here are possible without this framework.
But this framework has certain advantages over writing own configuration files
and test scripts:
\begin{enumerate}
\item It provides developers with pre-defined configurations of nodes.
Especially the configuration of nodes in a private network with own directory
nodes is not a trivial task.
\item It takes away the need to implement synchronization of a test application
with events created by Tor processes. This, too, is a non-trivial task and can,
if not done properly, lead to deadlocks or inconsistent states.
\item It relieves the developer from the task to collect and merge log files.
Typically, every Tor process produces its own log file, so that all files might
need to be merged in chronological order to identify causal dependencies.
\end{enumerate}

Originally this framework has been designed to perform experiments on Tor hidden
services. But it should be feasible for experiments on onion routing and other
Tor services, too. If you have found an alternative usage for it, and maybe
have changed or extended it to support your own development, please feel free to
contribute your additions. And please report bugs.

\section{Installation}

PuppeTor requires a Tor executable which can be downloaded from the Subversion
repository on the Tor homepage. Though PuppeTor can also be used with the stable
Tor versions, its primary intention is to be used for development, so that it
is regularly updated to run with the current development version.
Unfortunately, this sometimes includes configuration options that
are not understood by older Tor versions and that would have to be removed
explicitly.
After downloading, you either need to make sure
that the Tor executable is in the path, or put it to the base directory of this
framework.

You also need to download the Tor controller demonstration code from the
Subversion
repository on the Tor homepage and add them to your classpath. In order to use
the Groovy shell, you further need to add the Groovy classes that are available
on the Groovy homepage to the classpath. For convenience, the JARs containing
both, the controller code and the Groovy classes, are included with PuppeTor:
\begin{itemize}
\item \verb+lib/torctl.jar+ contains the trunk version of the Tor controller
demonstration code and
\item \verb+lib/groovy-all-<version>.jar+ contains the Groovy classes.
\end{itemize}

Finally, you need to have a Java Runtime Environment version 5 or
higher installed on your machine.

\section{Examples}

This user's guide takes the approach of stepwise introducing features by showing
typical usage examples. Though these examples might not cover all features, the
remaining features should be self-explanatory afterwards, maybe with a little
help of the Javadocs.

\subsection{Accessing a public web server over Tor}

The easiest way to use Tor is to anonymously access a public web server. This is
what we do in this first example.

You can start the first example with the following command from the
base directory of this framework:

\begin{verbatim}
java -cp bin:lib/torctl.jar
    de.uniba.wiai.lspi.puppetor.examples.
    AccessingPublicWebServerOverTor
\end{verbatim}

The network configuration consists only of one proxy node with an open
SOCKS port and control port (every node needs to open its control
port, so that we can communicate with it.)

In the following, we will walk through the code that is necessary to configure
this ``network'' and perform the request to a public web server.

We start with obtaining a \texttt{Network} instance which is the central place
for each test run:

\begin{verbatim}
Network network = NetworkFactory.createNetwork("example1");
\end{verbatim}

Now we are ready to create the Tor proxy that we need to perform our request.
The node will be assigned two port numbers for its SOCKS and control port
automatically, starting at 7000 (can be changed for the network).
The name is given for logging purposes and as event source name:

\begin{verbatim}
network.createProxy("proxy");
\end{verbatim}

As we are fine with the pre-defined configuration (at least for the moment), we
can write the configurations of all nodes (which is only our proxy in this case)
to \texttt{torrc} files in the nodes' working directories:

\begin{verbatim}
network.writeConfigurations();
\end{verbatim}

Next we start the nodes of our network. (The reason for separating this step
from the
previous one is the possible investigation of the working directory content, if
required.) Starting nodes can fail for some reason and block our application
forever. Therefore, we can provide a timeout in milliseconds after which we
consider the operation as failed. Starting nodes is considered to be complete as
soon as all Tor nodes have opened their control ports and we have connected to
them:

\begin{verbatim}
network.startNodes(5000);
\end{verbatim}

From time to time, Tor cannot create a circuit without some kind of launching
assistance. Especially in a private-network setting, nodes need to reboot in
order to refresh their directory information and be able to build circuits.
Hence, we send the nodes a ``HUP'' signal in regular intervals until it tells us
that it has opened a circuit. In the following operation we can configure how
often we want to send this signal (here: 5 times) and how long we want to wait
in between (here: 10 seconds):

\begin{verbatim}
network.hupUntilUp(5, 10000);
\end{verbatim}

Now our Tor network is running.

Next is the client that will perform requests using our proxy. PuppeTor contains
a \texttt{ClientApplication} class which can start a thread to perform simple
HTTP GET requests to a given address and port. We provide it with the name that
is used for logging purposes and as event source, the address and port to send
the request to, and the SOCKS port of our proxy:

\begin{verbatim}
ClientApplication client = network.createClient("client",
    "www.google.com", 80, proxy.getSocksPort());
\end{verbatim}

Before starting the request we want to register for events coming from this
client. This is necessary as requests are performed in a separate thread in the
background, which allows more complex applications to perform multiple requests
in parallel. Therefore, we implement the interface \texttt{EventListener}
and its method \texttt{handleEvent(Event)}. In this method we are interested in
the two events \texttt{ClientEventType.CLIENT\_SENDING\_REQUEST} and
\texttt{ClientEventType.CLIENT\_REPLY\_RECEIVED}.
What we do with these events is
application-specific. In our case we record the time of the first event and
subtract it from the time of the second event to obtain the round-trip time of
our request:

\begin{verbatim}
EventListener clientEventListener =
    new EventListener() { // ... };
\end{verbatim}

Next we obtain a reference to the \texttt{EventManager} which handles all
asynchronous events coming from the Tor processes:

\begin{verbatim}
EventManager manager = network.getEventManager();
\end{verbatim}

We add our event handler to the event manager with the client name
as event source:

\begin{verbatim}
manager.addEventListener(client.getClientApplicationName(),
    clientEventListener);
\end{verbatim}

Finally, we can perform the requests to the public server. Just in case that a
request fails or times out, we state that we are willing to make three attempts
with a timeout of 20 seconds each, and that further requests should be aborted
as soon as one request succeeds (last parameter \texttt{true}).

\begin{verbatim}
client.performRequest(3, 20000, true);
\end{verbatim}

Due to the asynchronous performing of requests, we need to explicitly wait for
the requests to be completed. We can do this by invoking a method on the event
manager that blocks the invoking thread until a certain event is received. In
this case we want to be blocked until the event
\texttt{ClientEventType.CLIENT\_REQUESTS\_PERFORMED} is received from our
client:

\begin{verbatim}
manager.waitForAnyOccurence(client,
    ClientEventType.CLIENT_REQUESTS_PERFORMED);
\end{verbatim}

Next we shut down the network containing our proxy:

\begin{verbatim}
network.shutdownNodes();
\end{verbatim}

At last we need to explicitly shut down the JVM. The reason is that some objects
are accessible via RMI, and even though we are not using RMI in this examples,
Java keeps a non-daemon thread running that prevents the JVM from exiting on
its own.

\begin{verbatim}
System.exit(0);
\end{verbatim}

A typical output of example 1 might look like the following:

\begin{verbatim}
Successfully started the node!
Successfully built circuits!
Request took 1228 milliseconds
Goodbye.
\end{verbatim}

\subsection{Advertising hidden service to public Tor network}

The second example (\texttt{AdvertisingHiddenServiceToPublicTorNetwork})
advertises a hidden service to the public Tor network, but does not perform any
requests to it. In fact this can be a useful test to measure publication times
of rendezvous service descriptors.

The network configuration again consists only of one proxy node.

We start again with creating a network and a proxy node.

\begin{verbatim}
Network network = NetworkFactory.createNetwork("example2");
ProxyNode proxy = network.createProxy("proxy");
\end{verbatim}

Next we extend the configuration of the proxy by a hidden service entry. The
hidden service gets the name \texttt{hidServ} which is used for its working
directory and for logging purposes. It expects that the real service will run
on the next free port (here: \texttt{7002}) and announces to Tor
clients that it runs on the virtual port 80 (default value):

\begin{verbatim}
proxy.addHiddenService("hidServ");
\end{verbatim}

The next step stays the same as in the previous example. We write the
configuration of our proxy to its \texttt{torrc} file.

\begin{verbatim}
network.writeConfigurations();
\end{verbatim}

Before starting the proxy, we create and register an event listener that informs
us about two events: when Tor opens a circuit
(\texttt{NodeEventType.NODE\_CIRCUIT\_OPENED}) and when it has published a
rendezvous service descriptor
(\texttt{HiddenServiceEventType.BOB\_DESC\_PUBLISHED\_RECEIVED}).

\begin{verbatim}
EventListener proxyEventListener =
    new EventListener() { // ... };
\end{verbatim}

Again we register this event listener at the event manager, but this time with
the proxy as event subject.

\begin{verbatim}
EventManager manager = network.getEventManager();
manager.addEventListener(proxy.getNodeName(),
    proxyEventListener);
\end{verbatim}

The next steps are similar to the first example. We need to start the proxy and
maybe send ``HUP'' signals until it has built a circuit.

\begin{verbatim}
network.startNodes(5000);
network.hupUntilUp(5, 10000);
\end{verbatim}

As there is no event we could wait for, we fall asleep for two minutes to
observe publication of rendezvous service descriptors.

\begin{verbatim}
Thread.sleep(2L * 60L * 1000L);
\end{verbatim}

At the end we shut down the proxy and the JVM.

\begin{verbatim}
network.shutdownNodes();
System.exit(0);
\end{verbatim}

A typical output of example 2 might look like the following:

\begin{verbatim}
Successfully started the node!
Successfully built circuits!
Waiting for 2 minutes and observing RSD publications...
RSD published 45128 milliseconds after first circuit was opened
RSD published 51117 milliseconds after first circuit was opened
RSD published 51118 milliseconds after first circuit was opened
Goodbye.
\end{verbatim}

\subsection{Advertising and accessing hidden service over public Tor network}

This third example
(\texttt{AdvertisingAndAccessingHiddenServiceOverPublicTorNetwork}) is in fact
the first for which this framework originally has
been built. The setting consists of two proxies that are connected to the
public Tor network, a server that is connected as hidden server to the first
proxy, and a client that performs requests using the second proxy. This scenario
can be used to measure real-world round-trip times for requests to hidden
services.

The network configuration consists of two proxy nodes and looks like this:
\begin{itemize}
\item Proxy \texttt{proxy1}.
\item Proxy \texttt{proxy2}.
\item Server that is registered as hidden server at \texttt{proxy1}.
\item Client that performs requests using the SOCKS port of \texttt{proxy2}.
\end{itemize}

The example starts similar to the previous example with creating and configuring
two proxy nodes. A hidden service is configured at the first proxy. Then
configurations are written, nodes are started and HUP'ed until all of them have
built a circuit.

\begin{verbatim}
Network network = NetworkFactory.createNetwork("example3");
ProxyNode proxy1 = network.createProxy("proxy1");
ProxyNode proxy2 = network.createProxy("proxy2");
HiddenService hidServ1 = proxy1.addHiddenService("hidServ");
network.writeConfigurations();
network.startNodes(5000);
network.hupUntilUp(5, 10000);
\end{verbatim}

In contrast to the previous example we want to wait until the first proxy has
successfully uploaded a rendezvous service descriptor to the directory before
performing a request. From experience we can say that hidden service
initialization takes most part of the time. We want to wait for this event from
the first proxy as source for at most 3 minutes:

\begin{verbatim}
EventManager manager = network.getEventManager();
manager.waitForAnyOccurence(proxy1,
    HiddenServiceEventType.BOB_DESC_PUBLISHED_RECEIVED
    3L * 60L * 1000L);
\end{verbatim}

As soon as the first descriptor has been published, we can initialize both, 
server and client. In contrast to the first example the client will perform an
empty HTTP GET on the hidden server's onion address instead of a public web
server. The server will answer with an empty HTTP OK:

\begin{verbatim}
ServerApplication server = network.createServer("server",
    hidServ1.getServicePort());
ClientApplication client = network.createClient("client",
    hidServ1.determineOnionAddress(), hidServ1.getVirtualPort(),
    proxy2.getSocksPort());
\end{verbatim}

For convenience we create a single event listener for both, client and server.
It can distinguish client and server events by their type. We are interested in
the events \texttt{ClientEventType.CLIENT\_SENDING\_REQUEST} which is fired
from the
client before sending a request,
\texttt{ServerEventType.SERVER\_RECEIVING\_REQUEST\_SENDING\_REPLY}
which is fired by the server when a request is received, and
\texttt{ClientEventType.CLIENT\_REPLY\_RECEIVED}
which is again fired by the client when it has received a reply. However, we
need to register this event listener for both event sources, client and server.

\begin{verbatim}
EventListener clientAndServerEventListener =
    new EventListener() { // ... };
manager.addEventListener(client.getClientApplicationName(),
    clientAndServerEventListener);
manager.addEventListener(server.getServerApplicationName(),
    clientAndServerEventListener);
\end{verbatim}

Now we instruct the server to listen for incoming requests and the client to
perform such requests.

\begin{verbatim}
server.startListening();
client.performRequest(5, 45000, true);
\end{verbatim}

We wait for the client to complete all requests.

\begin{verbatim}
manager.waitForAnyOccurence(client,
    ClientEventType.CLIENT_REQUESTS_PERFORMED);
\end{verbatim}

Finally, we shut down all nodes and the JVM.

\begin{verbatim}
network.shutdownNodes();
System.exit(0);
\end{verbatim}

A typical output of example 3 might look like the following:

\begin{verbatim}
Successfully started nodes!
Successfully built circuits!
Successfully published an RSD!
Request took 14941 milliseconds from client to server!
Request took 15927 milliseconds for the round-trip and
    986 milliseconds from server to client!
Goodbye.
\end{verbatim}

\subsection{Advertising and accessing hidden service over private Tor network}

The fourth example
(\texttt{AdvertisingAndAccessingHiddenServiceOverPrivateTorNetwork}) is a
little more complex. It involves setting up a private
Tor network including own directory nodes. The overall task stays the same as in
the previous example: perform a request to a hidden service and see what
happens. In this case performance does not mean too much, because there is
no network latency on the local host. But in contrast to a public-network
setting we could observe what happens on all routers and directory
nodes during the request.

The network configuration now consists of three router nodes, which are used for
performing onion routing and misused as proxies, two directory nodes, a client,
and a server:
\begin{itemize}
\item Router \texttt{router1}.
\item Router \texttt{router2}.
\item Router \texttt{router3}.
\item Directory \texttt{dir1}.
\item Directory \texttt{dir2}.
\item Server that is registered as hidden server at \texttt{router1}.
\item Client that performs requests using the SOCKS port of \texttt{router3}.
\end{itemize}

Creating and configuring these nodes is done similar to all previous examples
and requires no further explanation:

\begin{verbatim}
Network network = NetworkFactory.createNetwork("example4");
RouterNode router1 = network.createRouter("router1");
network.createRouter("router2");
RouterNode router3 = network.createRouter("router3");
network.createDirectory("dir1");
network.createDirectory("dir2");
HiddenService hidServ1 = router1.addHiddenService("hidServ");
\end{verbatim}

However, the next step does require further explanation. The main difficulty
of setting up a private Tor network
lies in the fact that all nodes need to be configured so that they accept our
own directory nodes instead of the pre-configured directory nodes from the
public Tor network. This configuration requires the fingerprints of all
directory nodes. These fingerprints are written to disk by the directory nodes
as soon as they are started. But the directories need to be configured with
these fingerprints before
being started, too, in order to prevent them from becoming part of the public
Tor network. And now we have the chicken or the egg dilemma.

The non-trivial solution is to configure the directory nodes with a fake
directory configuration and start them using the \texttt{--list-fingerprint}
option. Hence they write a \texttt{fingerprint} file to disk and shut down
immediately. This fingerprint can be read, and all nodes can be
configured to use the directory using this fingerprint.

A second, non-trivial task is to authorize routers and directory nodes.
Therefore, an authoritative directory needs to know all fingerprints of
authorized nodes, also before starting the directory nodes. They are stored in
the \texttt{approved-routers} file in the
working directory of the directory node.

The complete configuration task is encapsulated in the following method for
convenience.

\begin{verbatim}
network.configureAsPrivateNetwork();
\end{verbatim}

Afterwards, the process is more or less similar to the previous example:

\begin{verbatim}
network.writeConfigurations();
network.startNodes(15000);
network.hupUntilUp(10, 10000);
EventManager manager = network.getEventManager();
manager.waitForAnyOccurence(router1.getNodeName(),
    HiddenServiceEventType.BOB_DESC_PUBLISHED_RECEIVED,
    3L * 60L * 1000L);
ServerApplication server = network.createServer("server",
    hidServ1.getServicePort());
ClientApplication client = network.createClient("client",
    hidServ1.getServicePort(), hidServ1.getVirtualPort(),
    router3.getSocksPort());
EventListener clientAndServerEventListener =
    new EventListener() { // ... };
manager.addEventListener(client, clientAndServerEventListener);
manager.addEventListener(server, clientAndServerEventListener);
server.startListening();
client.performRequest(5, 45000, true);
manager.waitForAnyOccurence(client,
    ClientEventType.CLIENT_REQUESTS_PERFORMED);
network.shutdownNodes();
\end{verbatim}

A typical output of example 4 might look like the following:

\begin{verbatim}
Successfully started nodes!
Successfully built circuits!
Successfully published an RSD!
Started server
Handling event: Sending request.
Handling event: Receiving request.
Handling event: Received response.
Handling event: Requests performed.
Goodbye.
\end{verbatim}

\subsection{Controlling a network using the Groovy shell}

The first four examples demonstrated how PuppeTor can be used in a batch-like
fashion. This fifth example (\texttt{LongRunningNetwork}) will show how to use
it interactively by making a
PuppeTor network accessible via RMI and connecting to it with a command shell
that uses the Java-based script language Groovy.

Therefore, we change the fourth example so that it only creates a private
network and makes it available via RMI. Apart from the last step these these
steps don't require further explanation.

\begin{verbatim}
Network network = NetworkFactory.createNetwork("example5");
network.createRouter("router1");
network.createRouter("router2");
network.createRouter("router3");
network.createDirectory("dir1");
network.createDirectory("dir2");
network.configureAsPrivateNetwork();
network.writeConfigurations();
network.startNodes(5000);
network.hupUntilUp(10, 10000);
\end{verbatim}

The last step is to bind the \texttt{Network} object to a locally running RMI
registry under its network name, so that it can be found by RMI clients:

\begin{verbatim}
network.bindAtRmiregistry();
\end{verbatim}

Starting this example requires that the \texttt{rmiregistry} has been started
on the local host before.

Further, Java needs two VM arguments, namely the
location of an RMI policy file and the codebase:

\begin{itemize}
\item \verb+-Djava.security.policy=res/myrmipolicy+
\item \verb+-Djava.rmi.server.codebase=file:///<puppetor-path>/bin/+
\end{itemize}

The typical output of the first part of example 5 should look like the
following:

\begin{verbatim}
Successfully started nodes!
Successfully built circuits!
Bound at rmiregistry to name "example5"!
Waiting until the end of time... Interrupt with Ctrl-C!
\end{verbatim}

Next, we can connect to the network from our Groovy shell. Groovy is a dynamic
language that is based on Java with a lot of powerful extensions. Groovy
commands can be interpreted and executed on the fly, and the Groovy shell
can keep a state of bound variables to work with. A main advantage of using this
shell is to interactively work with PuppeTor networks without the need to
recompile and re-run the Java classes.

The shell is started with the following command:

\begin{verbatim}
java -cp bin:lib/torctl.jar:lib/groovy-all-1.0.jar
    de.uniba.wiai.lspi.puppetor.groovy.RmiPuppetzShell 
\end{verbatim}

The first thing to do is to connect to the already running network and bind it
to a local variable:

\begin{verbatim}
network = shell.connectNetwork("127.0.0.1", "example5");
\end{verbatim}

Next, we want to add a hidden service to \texttt{router1}, rewrite its
configuration and send it a HUP command to reload it:

\begin{verbatim}
router1 = network.getNode("router1");
hidServ1 = router1.addHiddenService("hidServ");
router1.writeConfiguration();
router1.hup();
\end{verbatim}

Now we want to create a server that will listen on the service port of
\texttt{hidServ1} and a client that will perform requests to that server:

\begin{verbatim}
server = network.createServer("server",
    hidServ1.getServicePort());
client = network.createClient("client",
    hidServ1.determineOnionAddress(), hidServ1.getVirtualPort(),
    router3.getSocksPort());
\end{verbatim}

And finally, we let the server start listening and the client start performing
requests to our hidden server:

\begin{verbatim}
server.startListening();
client.startRequests(5, 45000, true);
\end{verbatim}

The result is a number of events that should contain the following events
in the \texttt{example5} tab (without Tor log statements):

\begin{verbatim}
Sending request.
Receiving request.
Received response.
Requests performed.
\end{verbatim}

\subsection{Merging two private networks via RMI}

The sixth example uses the RMI capability of PuppeTor to merge two network
configurations, so that the Tor nodes in the two networks consider all nodes to
be in a single private network. There are at least two applications for doing
this:
\begin{enumerate}
\item In some situations Tor nodes
need to be running for a certain time before a test can be executed. For
example it is necessary that directory nodes in a private network are running
for about 15 to 20 minutes before proxy nodes believe in the directory
information they obtain from them. Therefore, one can run a
long-term network and merge a short-running network with it
that is only used for a single test.
\item Some tests might require a big number of nodes which cannot be run on a
single host. Therefore, the nodes could be distributed among multiple
physical hosts to use their combined resources.
\end{enumerate}

This sixth example requires the long-running network of \texttt{example5} to be
still running,
creates a second network of two router nodes, and merges it with the
long-running network. Afterwards it provides a hidden service on the first
router node and accesses it via the second router node.

The example starts with configuring two router nodes, one of them providing a
hidden service:

\begin{verbatim}
Network network = NetworkFactory.createNetwork("example6", 7200);
RouterNode router4 = network.createRouter("router4");
RouterNode router5 = network.createRouter("router5");
HiddenService hidServ1 = router4.addHiddenService("hidServ");
\end{verbatim}

In the next step, we connect to the remote network and configure both networks
to exchange directory strings and router fingerprints, so that the Tor nodes
will consider all nodes to be part of a merged network:

\begin{verbatim}
Network remoteNetwork =
    NetworkFactory.connectToNetwork("127.0.0.1", "example5");
network.configureAsInterconnectedPrivateNetwork(remoteNetwork);
\end{verbatim}

Next, we have to rewrite the configurations of the remote nodes and make the
directories reload their own configurations, including the extended
\texttt{approved-routers} files:

\begin{verbatim}
remoteNetwork.writeConfigurations();
remoteNetwork.hupAllDirectories();
\end{verbatim}

Now we can write the configurations of the nodes in the local network and start
them as usual. Afterwards, we wait for the first rendezvous service descriptor
to be published, before starting the server and performing the client requests.
Finally, we shut down the local nodes:

\begin{verbatim}
network.writeConfigurations();
network.startNodes(5000);
network.hupUntilUp(20, 5000);
manager.waitForAnyOccurence(router4.getNodeName(),
    HiddenServiceEventType.BOB_DESC_PUBLISHED_RECEIVED,
    3L * 60L * 1000L);
ServerApplication server = network.createServer("server",
    hidServ1.getServicePort());
ClientApplication client = network.createClient("client",
    hidServ1.determineOnionAddress(), hidServ1.getVirtualPort(),
    router5.getSocksPort());
EventManager manager = network.getEventManager();
EventListener listener = new EventListener() { // ... };
manager.addEventListener(listener);
server.startListening();
client.startRequests(5, 45000, true);
manager.waitForAnyOccurence(client.getClientApplicationName(),
    ClientEventType.CLIENT_REQUESTS_PERFORMED);
network.shutdownNodes();
\end{verbatim}

The output of this example should be equal to that of example 4:

\begin{verbatim}
Successfully started nodes!
Successfully built circuits!
Successfully published an RSD!
Started server
Handling event: Sending request.
Handling event: Receiving request.
Handling event: Received response.
Handling event: Requests performed.
Goodbye.
\end{verbatim}

This example can also be run with proxy nodes. However, in that case
\texttt{example5} must be started at least 20 minutes before \texttt{example6}!
Further, the directories in the remote network would neither have to rewrite
their configurations nor be HUP'ed. The advantage of using proxies instead of
routers is that they don't leave behind orphaned router descriptors as this
example does.

\section{Events and messages for hidden services}

PuppeTor contains a number of pre-defined event types to observe how Tor behaves
when setting up and accessing a hidden service. Therefore, PuppeTor parses the
log statements coming from all started Tor processes and decides whether to fire
an event or not. All necessary log statements are contained in the current Tor
trunk on log level \texttt{INFO} or higher. Figure \ref{setup} shows the events
and exchanged
messages for setting up a hidden service, figure \ref{download} the descriptor
download, and figure \ref{connect} the connection establishment.

\begin{figure}
\caption{Hidden service setup\label{setup}}
\begin{msc}{Hidden service setup}
\declinst{a}{Alice's OP}{}
\declinst{r}{Rendezvous Point}{}
\declinst{d}{Dir/HSDir}{}
\declinst{i}{Introduction Point}{}
\declinst{b}{Bob's OP}{}
\action{BOB\_BUILT\_INTRO\_CIRC\_SENDING\_ESTABLISH\_INTRO}{b}
\nextlevel[2]
\mess{ESTABLISH\_INTRO}{b}{i}[1]
\nextlevel[2]
\action{IPO\_RECEIVED\_ESTABLISH\_INTRO\_SENDING\_INTRO\_ESTABLISHED}{i}
\nextlevel[2]
\mess{INTRO\_ESTABLISHED}{i}{b}[1]
\nextlevel[2]
\action{BOB\_INTRO\_ESTABLISHED\_RECEIVED}{b}
\nextlevel[2]
\action{BOB\_SENDING\_PUBLISH\_DESC}{b}
\nextlevel[2]
\mess{POST}{b}{d}[1]
\nextlevel[2]
\action{DIR\_PUBLISH\_DESC\_RECEIVED}{d}
\nextlevel[2]
\mess{OK}{d}{b}[1]
\nextlevel[2]
\action{BOB\_DESC\_PUBLISHED\_RECEIVED}{b}
\nextlevel[2]
\end{msc}
\end{figure}

\begin{figure}
\caption{Hidden service decriptor download\label{download}}
\begin{msc}{Hidden service decriptor download}
\declinst{a}{Alice's OP}{}
\declinst{r}{Rendezvous Point}{}
\declinst{d}{Dir/HSDir}{}
\declinst{i}{Introduction Point}{}
\declinst{b}{Bob's OP}{}
\action{ALICE\_ONION\_REQUEST\_RECEIVED}{a}
\nextlevel[2]
\action{ALICE\_SENDING\_FETCH\_DESC}{a}
\nextlevel[2]
\mess{GET}{a}{d}[1]
\nextlevel[2]
\action{DIR\_FETCH\_DESC\_RECEIVED}{d}
\nextlevel[2]
\mess{OK}{d}{a}[1]
\nextlevel[2]
\action{ALICE\_DESC\_FETCHED\_RECEIVED}{a}
\nextlevel[2]
\end{msc}
\end{figure}

\begin{figure}
\caption{Hidden service connection establishment\label{connect}}
\begin{msc}{Hidden service connection establishment}
\declinst{a}{Alice's OP}{}
\declinst{r}{Rendezvous Point}{}
\declinst{d}{Dir/HSDir}{}
\declinst{i}{Introduction Point}{}
\declinst{b}{Bob's OP}{}
\action{ALICE\_BUILT\_REND\_CIRC\_SENDING\_ESTABLISH\_RENDEZVOUS}{a}
\nextlevel[2]
\mess{ESTABLISH\_RENDEZVOUS}{a}{r}[1]
\nextlevel[2]
\action{RPO\_RECEIVED\_ESTABLISH\_RENDEZVOUS\_SENDING\_RENDEZVOUS\_ESTABLISHED}{r}
\nextlevel[2]
\mess{RENDEZVOUS\_ESTABLISHED}{r}{a}[1]
\nextlevel[2]
\action{ALICE\_RENDEZVOUS\_ESTABLISHED\_RECEIVED}{a}
\nextlevel[2]
\action{ALICE\_BUILT\_INTRO\_CIRC}{a}
\nextlevel[2]
\action{ALICE\_SENDING\_INTRODUCE1}{a}
\nextlevel[2]
\mess{INTRODUCE1}{a}{i}[1]
\nextlevel[2]
\action{IPO\_RECEIVED\_INTRODUCE1\_SENDING\_INTRODUCE2\_AND\_INTRODUCE\_ACK}{i}
\nextlevel[2]
\mess{INTRODUCE2}{i}{b}[1]
\mess{INTRODUCE\_ACK}{i}{a}[1]
\nextlevel[2]
\action{ALICE\_INTRODUCE\_ACK\_RECEIVED}{a}
\action{BOB\_INTRODUCE2\_RECEIVED}{b}
\nextlevel[2]
\action{BOB\_BUILT\_REND\_CIRC\_SENDING\_RENDEZVOUS1}{b}
\nextlevel[2]
\mess{RENDEZVOUS1}{b}{r}[1]
\nextlevel[2]
\action{RPO\_RECEIVING\_RENDEZVOUS1\_SENDING\_RENDEZVOUS2}{r}
\nextlevel[2]
\mess{RENDEZVOUS2}{r}{a}[1]
\nextlevel[2]
\action{ALICE\_RENDEZVOUS2\_RECEIVED\_APP\_CONN\_OPENED}{a}
\nextlevel[2]
\action{BOB\_APP\_CONN\_OPENED}{b}
\nextlevel[2]
\nextlevel
\end{msc}
\end{figure}

\section{Known issues}

There is a small list of problems and open questions that require more
investigation:

\begin{itemize}
\item When logging to stdout is set to something lower than \texttt{notice}, the
application does not succeed.
\item Fight the TODOs\ldots
\end{itemize}

\end{document} 
