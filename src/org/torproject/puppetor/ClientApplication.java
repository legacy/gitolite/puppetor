/*
 * Copyright 2007 Karsten Loesing
 * Copyright 2008-2009 Karsten Loesing and Sebastian Hahn
 * See LICENSE for licensing information
 */
package org.torproject.puppetor;

import java.rmi.Remote;
import java.rmi.RemoteException;

/**
 * The <code>ClientApplication</code> can be used to simulate simple
 * <code>HTTP GET</code> requests by a virtual local client. Therefore, an
 * address and a port are given to which the client shall connect. Requests are
 * performed by a background thread, so that multiple requests could be
 * performed at the same time.
 * 
 * @author kloesing
 */
public interface ClientApplication extends Remote {

    /**
     * <p>
     * Performs one or more HTTP requests to a previously provided address and
     * port. All requests are performed by a thread in the background, so that
     * this method returns immediately. That thread will try for
     * <code>retries</code> times to make the request with a timeout of
     * <code>timeoutForEachRetry</code> milliseconds each. If an attempt is
     * not successful, the thread nevertheless waits for the timeout to expire
     * before performing the next attempt. If <code>stopOnSuccess</code> is
     * set to <code>true</code>, the thread will quit performing requests
     * immediately after the first successful request.
     * </p>
     * 
     * <p>
     * For each sent request the application fires a
     * <event>ClientEventType.CLIENT_SENDING_REQUEST</code> event. On receiving
     * a reply it fires an event of type <code>ClientEventType.CLIENT_REPLY_RECEIVED</code>,
     * if a request is not successful or times out, a <code>ClientEventType.CLIENT_GAVE_UP_REQUEST</code>
     * event is fired. After all requests have been performed (either
     * successfully, or not) an event of type <code>ClientEventType.CLIENT_REQUESTS_PERFORMED</code>
     * is fired.
     * </p>
     * 
     * TODO may this method only be invoked once?!
     * 
     * @param retries
     *            The number of retries that this client will perform. Must be
     *            <code>1</code> or greater.
     * @param timeoutForEachRetry
     *            The timeout for each request. If a request is not successful,
     *            the thread nevertheless waits for the timeout to expire. Must
     *            not be negative.
     * @param stopOnSuccess
     *            If set to <code>true</code>, the client quits performing
     *            requests after the first successful request, if <code>false</code>
     *            it continues until all <code>retries</code> have been
     *            processed.
     * @throws IllegalArgumentException
     *             Thrown if an invalid value is given for either of the
     *             parameters.
     * @throws RemoteException
     *             Thrown if an error occurs when accessed remotely.
     */
    public abstract void startRequests(int retries, long timeoutForEachRetry,
            boolean stopOnSuccess) throws RemoteException;

    /**
     * Stops all requests that are currently running.
     * 
     * @throws IllegalStateException
     *             Thrown if no requests have been started before.
     * @throws RemoteException
     *             Thrown if an error occurs when accessed remotely.
     */
    public abstract void stopRequest() throws RemoteException;

    /**
     * Returns the name of this client.
     * 
     * @return The name of this client.
     * @throws RemoteException
     *             Thrown if an error occurs when accessed remotely.
     */
    public abstract String getClientApplicationName() throws RemoteException;

    /**
     * Returns the SOCKS port of the local Tor node to which requests are sent.
     * 
     * @return The SOCKS port of the local Tor node to which requests are sent.
     * @throws RemoteException
     *             Thrown if an error occurs when accessed remotely.
     */
    public abstract int getSocksPort() throws RemoteException;

    /**
     * Returns the target name for the requests sent by this client; can be
     * either a server name/address or an onion address.
     * 
     * @return The target name for the requests sent by this client.
     * @throws RemoteException
     *             Thrown if an error occurs when accessed remotely.
     */
    public abstract String getTargetName() throws RemoteException;

    /**
     * Returns the target port for the requests sent by this client; can be
     * either a server port or a virtual port of a hidden service.
     * 
     * @return The target port for the requests sent by this client.
     * @throws RemoteException
     *             Thrown if an error occurs when accessed remotely.
     */
    public abstract int getTargetPort() throws RemoteException;
}
