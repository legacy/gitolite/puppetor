/*
 * Copyright 2007 Karsten Loesing
 * Copyright 2008-2009 Karsten Loesing and Sebastian Hahn
 * See LICENSE for licensing information
 */
package org.torproject.puppetor;

import java.rmi.RemoteException;

/**
 * A <code>RouterNode</code> represents a Tor process that is configured to
 * both, relay traffic from a local application to the Tor network and to route
 * traffic on behalf of remote applications. It inherits most of its
 * configuration and behavior from its superclass <code>ProxyNode</code> and
 * adds some router-specific configurations and behavior.
 * 
 * @author kloesing
 */
public interface RouterNode extends ProxyNode {

    /**
     * Returns the dir port of this node.
     * 
     * @return The dir port of this node.
     * @throws RemoteException
     *             Thrown if an error occurs when accessed remotely.
     */
    public abstract int getDirPort() throws RemoteException;

    /**
     * Returns the onion port of this node.
     * 
     * @return The onion port of this node.
     * @throws RemoteException
     *             Thrown if an error occurs when accessed remotely.
     */
    public abstract int getOrPort() throws RemoteException;

    /**
     * <p>
     * Returns the fingerprint string of this node, formatted like
     * <code>nickname 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000</code>.
     * </p>
     * 
     * <p>
     * The fingerprint is determined by a background thread that is started as
     * soon as the node is instantiated. If this background thread has not
     * finished when this method is invoked, the invoking thread will be blocked
     * until the fingerprint is available (or determining it has failed,
     * whereupon an exception will be thrown).
     * </p>
     * 
     * @return The fingerprint of this node.
     * @throws PuppeTorException
     *             Thrown if either the temporary <code>torrc.temp</code>
     *             configuration file cannot be written, the Tor process cannot
     *             be started temporarily, or the fingerprint file cannot be
     *             read.
     * @throws RemoteException
     *             Thrown if an error occurs when accessed remotely.
     */
    public abstract String getFingerprint() throws PuppeTorException,
            RemoteException;
}
