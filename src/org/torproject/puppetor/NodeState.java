/*
 * Copyright 2007 Karsten Loesing
 * Copyright 2008-2009 Karsten Loesing and Sebastian Hahn
 * See LICENSE for licensing information
 */
package org.torproject.puppetor;

/**
 * The <code>NodeState</code> constitutes the state of a single Tor node. In
 * contrast to <code>EventType</code> the node states depend only on the
 * methods that have been invoked on these objects, and not on asynchronous
 * state changes. Most operations of <code>ProxyNode</code> and its subclasses
 * require a certain <code>NodeState</code> as precondition and may ensure
 * another <code>NodeState</code> as postcondition. There is a prescribed
 * order of states.
 * 
 * @author kloesing
 */
public enum NodeState {

    /**
     * The configuration of this node has not been written to disk. This is the
     * initial state of a <code>ProxyNode</code> or one of its subclasses.
     */
    CONFIGURING,

    /**
     * The configuration of this node has been written to disk, but the Tor
     * process has not been started, yet. This state could be useful to review
     * the configuration that has been written to disk.
     */
    CONFIGURATION_WRITTEN,

    /**
     * The node has been started and is running.
     */
    RUNNING,

    /**
     * The node had been started and shut down. It cannot be started at a later
     * time anymore.
     */
    SHUT_DOWN
}
