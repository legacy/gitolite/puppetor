/*
 * Copyright 2007 Karsten Loesing
 * Copyright 2008-2009 Karsten Loesing and Sebastian Hahn
 * See LICENSE for licensing information
 */
package org.torproject.puppetor;

/**
 * Event types that can be fired by a server application running as thread in
 * the background.
 */
@SuppressWarnings("serial")
public class ServerEventType implements EventType {

    /**
     * String identifying the type of the event type.
     */
    String typeString;

    /**
     * Creates a new event type with the given type string.
     * 
     * @param typeString
     *            String identifying the type of the event type.
     */
    public ServerEventType(String typeString) {
        this.typeString = typeString;
    }

    public String getTypeName() {
        return this.typeString;
    }

    /**
     * The server application has received a request and sent a reply to it;
     * this event is fired internally and not parsed from a log statement from
     * Tor.
     */
    public static final ServerEventType SERVER_RECEIVING_REQUEST_SENDING_REPLY = new ServerEventType(
            "SERVER_RECEIVING_REQUEST_SENDING_REPLY");
}