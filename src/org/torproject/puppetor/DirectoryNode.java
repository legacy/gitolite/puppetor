/*
 * Copyright 2007 Karsten Loesing
 * Copyright 2008-2009 Karsten Loesing and Sebastian Hahn
 * See LICENSE for licensing information
 */
package org.torproject.puppetor;

import java.rmi.RemoteException;
import java.util.Set;

/**
 * A <code>DirectoryNode</code> represents a Tor process that acts as
 * <code>RouterNode</code> and which is further a directory authoritative
 * server for the (private) Tor network. It inherits most of the configuration
 * and behavior from <code>RouterNode</code> and adds some directory-specific
 * configurations and behavior.
 * 
 * @author kloesing
 */
public interface DirectoryNode extends RouterNode {

    /**
     * Combines the fingerprint of this node to a <code>DirServer</code>
     * string that can be used to configure this or other nodes to use this node
     * as directory server.
     * 
     * @return <code>DirServer</code> string to configure a node to use this
     *         node as directory server.
     * @throws PuppeTorException
     *             Thrown if a problem occurs when determining the fingerprint
     *             of this node.
     * @throws RemoteException
     *             Thrown if an error occurs when accessed remotely.
     */
    public abstract String getDirServerString() throws PuppeTorException,
            RemoteException;

    /**
     * Adds the given (possibly empty) set of onion router fingerprints to the
     * set of approved routers to confirm to directory clients, that the given
     * routers can be trusted. Changes are only stored locally and not written
     * to the <code>approved-routers</code> file to disk which will be done
     * when writing the configuration of this node.
     * 
     * @param approvedRouters
     *            The set of approved routers to be added. Each provided string
     *            must be formatted as
     *            <code>nickname 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000</code>.
     * @throws IllegalArgumentException
     *             Thrown if <code>null</code> is passed as parameter.
     * @throws RemoteException
     *             Thrown if an error occurs when accessed remotely.
     */
    public void addApprovedRouters(Set<String> approvedRouters)
            throws RemoteException;
}
