/*
 * Copyright 2007 Karsten Loesing
 * Copyright 2008-2009 Karsten Loesing and Sebastian Hahn
 * See LICENSE for licensing information
 */
package org.torproject.puppetor;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.List;

/**
 * The <code>EventManager</code> is the central place for a test run to manage
 * asynchronous events by Tor processes and client or server applications
 * running as threads in the background. A test application can either register
 * event listeners to be notified asynchronously about events when they occur,
 * or synchronize with an event by being blocked until a certain event occurs.
 * 
 * @author kloesing
 */
public interface EventManager extends Remote {

    /**
     * Registers the given <code>listener</code> as event listener for events
     * originating from the given <code>source</code>. This method returns a
     * list of all previously fired events by this source, so that each event
     * fired by this source is either included in the returned list or
     * signalized in a later invocation on the event listener, but not in both.
     * This prevents race conditions by eliminating the gap between registration
     * of an event handler and asking if an event has been fired before
     * registering.
     * 
     * @param source
     *            The name of the source of events that the listener is
     *            interested in. May not be <code>null</code> and must be the
     *            name of a previously created node, client, or server.
     * @param listener
     *            The listener that wants to be notified about events from the
     *            given <code>source</code>. If the <code>listener</code>
     *            is already registered for the same <code>source</code>,
     *            nothing happens, i.e. the <code>listener</code> will not
     *            receive multiple invocations for the same event. May not be
     *            <code>null</code>.
     * @return A list of all previously fired events for the given
     *         <code>source</code>. If no event has been fired before, an
     *         empty list is returned instead of <code>null</code>.
     * @throws IllegalArgumentException
     *             Thrown if <code>null</code> is passed for either of the
     *             parameters or if the <code>source</code> is unknown.
     * @throws RemoteException
     *             Thrown if an error occurs when accessed remotely.
     */
    public abstract List<Event> addEventListener(String source,
            EventListener listener) throws RemoteException;

    /**
     * Registers the given <code>listener</code> as event listener for future
     * events originating from any source.
     * 
     * @param listener
     *            The listener that wants to be notified about events from the
     *            given <code>source</code>. If the <code>listener</code>
     *            is already registered for all sources, nothing happens, i.e.
     *            the <code>listener</code> will not receive multiple
     *            invocations for the same event. May not be <code>null</code>.
     * @throws IllegalArgumentException
     *             Thrown if <code>null</code> is passed for the parameter.
     * @throws RemoteException
     *             Thrown if an error occurs when accessed remotely.
     */
    public abstract void addEventListener(EventListener listener)
            throws RemoteException;

    /**
     * Returns the list of all previously observed events from the given
     * <code>source</code>.
     * 
     * @param source
     *            The source of the events that the invoking thread is
     *            interested in. May not be <code>null</code> and must be the
     *            name of a previously created node, client, or server.
     * @return List of all previously observed events from the given
     *         <code>source</code>.
     * @throws IllegalArgumentException
     *             Thrown if <code>null</code> is passed as parameter or if
     *             the <code>source</code> is unknown.
     * @throws RemoteException
     *             Thrown if an error occurs when accessed remotely.
     */
    public abstract List<Event> getEventHistory(String source)
            throws RemoteException;

    /**
     * Returns whether the given <code>eventType</code> has been observed from
     * the given <code>source</code> before, or not.
     * 
     * @param source
     *            The source of the event that the invoking thread is interested
     *            in. May not be <code>null</code> and must be the name of a
     *            previously created node, client, or server.
     * @param eventType
     *            The event type that the invoking thread is interested in. May
     *            not be <code>null</code>.
     * @throws IllegalArgumentException
     *             Thrown if <code>null</code> is passed for either of the
     *             parameters or if <code>source</code> is unknown.
     * @return <code>true</code> if the event has been observed from the
     *         source before, <code>false</code> otherwise.
     * @throws RemoteException
     *             Thrown if an error occurs when accessed remotely.
     */
    public abstract boolean hasEventOccured(String source, EventType eventType)
            throws RemoteException;

    /**
     * Removes the given <code>listener</code> as event listener from all
     * previously registered sources. If this listener is not registered for any
     * source, nothing happens.
     * 
     * @param listener
     *            The listener that shall be removed from the list of registered
     *            listeners. May not be <code>null</code>.
     * @throws IllegalArgumentException
     *             Thrown if <code>null</code> is passed as parameter.
     * @throws RemoteException
     *             Thrown if an error occurs when accessed remotely.
     */
    public abstract void removeEventListener(EventListener listener)
            throws RemoteException;

    /**
     * Checks if the given <code>eventType</code> has been observed from the
     * given <code>source</code> before; if not, blocks the invoking thread
     * until the next event of this type is fired from that source. Note that
     * this method does not restrict waiting to a timeout, so that it could
     * potentially block forever!
     * 
     * @param source
     *            The source of the event that the invoking thread is willing to
     *            wait for. May not be <code>null</code> and must be the name
     *            of a previously created node, client, or server.
     * @param eventType
     *            The event type that the invoking thread is willing to wait for
     *            from the given <code>source</code>. May not be
     *            <code>null</code>.
     * @throws IllegalArgumentException
     *             Thrown if <code>null</code> is passed for either of the
     *             parameters or if the <code>source</code> is unknown.
     * @throws RemoteException
     *             Thrown if an error occurs when accessed remotely.
     */
    public abstract void waitForAnyOccurence(String source, EventType eventType)
            throws RemoteException;

    /**
     * Checks if the given <code>eventType</code> has been observed from the
     * given <code>source</code> before; if not, blocks the invoking thread
     * until the next event of this type is fired from that source or the given
     * timeout of <code>maximumTimeToWaitInMillis</code> milliseconds has
     * expired.
     * 
     * @param source
     *            The source of the event that the invoking thread is willing to
     *            wait for. May not be <code>null</code> and must be the name
     *            of a previously created node, client, or server.
     * @param eventType
     *            The event type that the invoking thread is willing to wait for
     *            from the given <code>source</code>. May not be
     *            <code>null</code>.
     * @param maximumTimeToWaitInMillis
     *            The maximum time to wait in milliseconds. A positive value or
     *            zero restricts waiting to this time. If this value is
     *            negative, we will wait potentially forever.
     * @return <code>true</code> if an event of the given type has been fired
     *         by the <code>source</code> within the given timeout,
     *         <code>false</code> otherwise.
     * @throws IllegalArgumentException
     *             Thrown if an invalid value is passed for either of the
     *             parameters or if the <code>source</code> is unknown.
     * @throws RemoteException
     *             Thrown if an error occurs when accessed remotely.
     */
    public abstract boolean waitForAnyOccurence(String source,
            EventType eventType, long maximumTimeToWaitInMillis)
            throws RemoteException;

    /**
     * Blocks the invoking thread until the next <code>event</code> is fired
     * from the given <code>source</code>. This method only waits for the
     * next occurence of an event, regardless of previous occurrences. Note that
     * this method does not restrict waiting to a timeout, so that it could
     * potentially block forever!
     * 
     * @param source
     *            The source of the event that the invoking thread is willing to
     *            wait for. May not be <code>null</code> and must be the name
     *            of a previously created node, client, or server.
     * @param eventType
     *            The event type that the invoking thread is willing to wait for
     *            from the given <code>source</code>. May not be
     *            <code>null</code>.
     * @throws IllegalArgumentException
     *             Thrown if <code>null</code> is passed for either of the
     *             parameters or if the <code>source</code> is unknown.
     * @throws RemoteException
     *             Thrown if an error occurs when accessed remotely.
     */
    public abstract void waitForNextOccurence(String source, EventType eventType)
            throws RemoteException;

    /**
     * Blocks the invoking thread until the next <code>event</code> is fired
     * from the given <code>source</code> or the given timeout of
     * <code>maximumTimeToWaitInMillis</code> milliseconds has expired. This method
     * only waits for the next occurence of an event, regardless of previous
     * occurrences.
     * 
     * @param source
     *            The source of the event that the invoking thread is willing to
     *            wait for. May not be <code>null</code> and must be the name
     *            of a previously created node, client, or server.
     * @param eventType
     *            The event type that the invoking thread is willing to wait for
     *            from the given <code>source</code>. May not be
     *            <code>null</code>.
     * @param maximumTimeToWaitInMillis
     *            The maximum time to wait in milliseconds. A positive value or
     *            zero restricts waiting to this time. If this value is
     *            negative, we will wait potentially forever.
     * @return <code>true</code> if an event of the given type has been fired
     *         by the <code>source</code> within the given timeout,
     *         <code>false</code> otherwise.
     * @throws IllegalArgumentException
     *             Thrown if an invalid value is passed for either of the
     *             parameters or if the <code>source</code> is unknown.
     * @throws RemoteException
     *             Thrown if an error occurs when accessed remotely.
     */
    public abstract boolean waitForNextOccurence(String source,
            EventType eventType, long maximumTimeToWaitInMillis)
            throws RemoteException;

    /**
     * Registers a new event type by passing a pattern string that can be
     * applied to a regular expression when parsing Tor log statements. This is
     * useful for log statements that are only included in modified Tor
     * versions. Therefore, the event type may be an instance of a self-defined
     * class that implements <code>EventType</code>.
     * 
     * @param patternString
     *            The pattern string that will be used for parsing Tor log
     *            statements; the syntax corresponds to java.util.regex.Pattern.
     * @param eventType
     *            The event type of the event that will be fired when a log
     *            statement was parsed that includes the given pattern.
     * @throws RemoteException
     *             Thrown if an error occurs when accessed remotely.
     */
    public abstract void registerEventTypePattern(String patternString,
            EventType eventType) throws RemoteException;
}
