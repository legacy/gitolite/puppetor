/*
 * Copyright 2007 Karsten Loesing
 * Copyright 2008-2009 Karsten Loesing and Sebastian Hahn
 * See LICENSE for licensing information
 */
package org.torproject.puppetor;

import java.io.Serializable;

/**
 * The super interface of possible event types that are fired on a state change
 * of an asynchronous system component, e.g. a Tor process or a client/server
 * application running as thread in the background.
 * 
 * @author kloesing
 */
public interface EventType extends Serializable {

    /**
     * Returns a string representation of the event type name for display
     * purposes.
     * 
     * @return String representation of the event type name.
     */
    public abstract String getTypeName();
}
