/*
 * Copyright 2007 Karsten Loesing
 * Copyright 2008-2009 Karsten Loesing and Sebastian Hahn
 * See LICENSE for licensing information
 */
package org.torproject.puppetor;

import java.rmi.Remote;
import java.rmi.RemoteException;

/**
 * This interface must be implemented by any object in a test application that
 * shall be registered as event listener.
 * 
 * @author kloesing
 */
public interface EventListener extends Remote {

    /**
     * Is invoked when an asynchronous event is fired by the source (or one of
     * the sources) for which this listener was registered.
     * 
     * @param event
     *            The event that was fired.
     * @throws RemoteException
     *             Thrown if an error occurs when accessed remotely.
     */
    public void handleEvent(Event event) throws RemoteException;

}
