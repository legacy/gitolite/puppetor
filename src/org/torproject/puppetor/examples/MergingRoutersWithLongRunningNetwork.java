/*
 * Copyright 2007 Patrick Geyer, Johannes Jungkunst, Karsten Loesing, and
 * Stefan Schilling
 * Copyright 2008-2009 Karsten Loesing and Sebastian Hahn
 * See LICENSE for licensing information
 */
package org.torproject.puppetor.examples;

import java.rmi.RemoteException;

import org.torproject.puppetor.ClientApplication;
import org.torproject.puppetor.ClientEventType;
import org.torproject.puppetor.Event;
import org.torproject.puppetor.EventListener;
import org.torproject.puppetor.EventManager;
import org.torproject.puppetor.HiddenService;
import org.torproject.puppetor.HiddenServiceEventType;
import org.torproject.puppetor.Network;
import org.torproject.puppetor.NetworkFactory;
import org.torproject.puppetor.PuppeTorException;
import org.torproject.puppetor.RouterNode;
import org.torproject.puppetor.ServerApplication;
import org.torproject.puppetor.ServerEventType;


/**
 * Example for a private network of routers without directories that is merged
 * with another, long-running private network that contains routers and
 * directories. Note that <code>LongRunningNetwork</code> must be running in
 * order to run this example.
 * 
 * @author pgeyer, jjungkunst, sschilling
 */
public class MergingRoutersWithLongRunningNetwork {

    /**
     * Sets up and runs the test.
     * 
     * @param args
     *            Command-line arguments (ignored).
     * @throws PuppeTorException
     *             Thrown if there is a problem with the JVM-external Tor
     *             processes that we cannot handle.
     * @throws RemoteException
     *             Thrown if an error occurs when accessed remotely.
     */
    public static void main(String[] args) throws PuppeTorException,
            RemoteException {

        // create a network to initialize the test case
        Network network = NetworkFactory.createNetwork("example6", 7200);

        // create three router nodes
        RouterNode router4 = network.createRouter("router4");
        RouterNode router5 = network.createRouter("router5");

        // obtain reference to event manager to be able to respond to events
        EventManager manager = network.getEventManager();

        // register event listener
        EventListener listener = new EventListener() {
            public void handleEvent(Event event) {
                if (event.getType() instanceof ClientEventType
                        || event.getType() instanceof ServerEventType) {
                    System.out.println("Handling event: " + event.getMessage());
                }
            }
        };
        manager.addEventListener(listener);

        // obtain a reference on the remote long-running network
        Network remoteNetwork = NetworkFactory.connectToNetwork("127.0.0.1",
                "example5");

        // merge configuration with long-running network
        network.configureAsInterconnectedPrivateNetwork(remoteNetwork);

        // add hidden service
        HiddenService hidServ1 = router4.addHiddenService("hidServ");

        // write configurations and hup all directory nodes in the other network
        remoteNetwork.writeConfigurations();
        remoteNetwork.hupAllDirectories();

        // write configurations of router nodes
        network.writeConfigurations();

        // start proxy node and wait until it has opened a circuit with a
        // timeout of 5 seconds
        if (!network.startNodes(5000)) {
            System.out.println("Failed to start the nodes!");
            System.exit(0);
        }
        System.out.println("Successfully started nodes!");

        // hup until proxy has built circuits (20 retries, 5 seconds timeout
        // each)
        if (!network.hupUntilUp(20, 5000)) {

            // failed to build circuits
            System.out.println("Failed to build circuits!");
            System.exit(0);
        }
        System.out.println("Successfully built circuits!");

        // wait for 3 minutes that the proxy has published its first RSD
        if (!manager.waitForAnyOccurence(router4.getNodeName(),
                HiddenServiceEventType.BOB_DESC_PUBLISHED_RECEIVED,
                3L * 60L * 1000L)) {
            // failed to publish an RSD
            System.out.println("Failed to publish an RSD!");
            System.exit(0);
        }
        System.out.println("Successfully published an RSD!");

        // create server application
        ServerApplication server = network.createServer("server", hidServ1
                .getServicePort());

        // start server
        server.startListening();
        System.out.println("Started server");

        // create client application
        ClientApplication client = network.createClient("client", hidServ1
                .determineOnionAddress(), hidServ1.getVirtualPort(), router5
                .getSocksPort());

        // perform at most five request with a timeout of 45 seconds each
        client.startRequests(5, 45000, true);

        // wait for request to be performed
        manager.waitForAnyOccurence(client.getClientApplicationName(),
                ClientEventType.CLIENT_REQUESTS_PERFORMED);

        // shut down nodes
        network.shutdownNodes();

        // Shut down the JVM
        System.out.println("Goodbye.");
        System.exit(0);
    }
}