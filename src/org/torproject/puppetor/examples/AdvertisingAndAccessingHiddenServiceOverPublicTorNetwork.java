/*
 * Copyright 2007 Karsten Loesing
 * Copyright 2008-2009 Karsten Loesing and Sebastian Hahn
 * See LICENSE for licensing information
 */
package org.torproject.puppetor.examples;

import java.rmi.RemoteException;

import org.torproject.puppetor.ClientApplication;
import org.torproject.puppetor.ClientEventType;
import org.torproject.puppetor.Event;
import org.torproject.puppetor.EventListener;
import org.torproject.puppetor.EventManager;
import org.torproject.puppetor.HiddenService;
import org.torproject.puppetor.HiddenServiceEventType;
import org.torproject.puppetor.Network;
import org.torproject.puppetor.NetworkFactory;
import org.torproject.puppetor.ProxyNode;
import org.torproject.puppetor.PuppeTorException;
import org.torproject.puppetor.ServerApplication;
import org.torproject.puppetor.ServerEventType;


/**
 * Example for advertising and accessing a hidden service over the public Tor
 * network.
 * 
 * @author kloesing
 */
public class AdvertisingAndAccessingHiddenServiceOverPublicTorNetwork {

    /**
     * Sets up and runs the test.
     * 
     * @param args
     *            Command-line arguments (ignored).
     * @throws PuppeTorException
     *             Thrown if there is a problem with the JVM-external Tor
     *             processes that we cannot handle.
     * @throws RemoteException
     *             Thrown if an error occurs when accessed remotely.
     */
    public static void main(String[] args) throws PuppeTorException,
            RemoteException {

        // create a network to initialize the test case
        Network network = NetworkFactory.createNetwork("example3");

        // create two proxy nodes
        ProxyNode proxy1 = network.createProxy("proxy1");
        ProxyNode proxy2 = network.createProxy("proxy2");

        // add hidden service to the configuration of proxy1
        HiddenService hidServ1 = proxy1.addHiddenService("hidServ");

        // write configuration of proxy nodes
        network.writeConfigurations();

        // start nodes and wait until they have opened a circuit with a timeout
        // of 5 seconds
        if (!network.startNodes(5000)) {

            // failed to start the proxy
            System.out.println("Failed to start nodes!");
            System.exit(0);
        }
        System.out.println("Successfully started nodes!");

        // hup until nodes have built circuits (5 retries, 10 seconds timeout
        // each)
        if (!network.hupUntilUp(5, 10000)) {

            // failed to build circuits
            System.out.println("Failed to build circuits!");
            System.exit(0);
        }
        System.out.println("Successfully built circuits!");

        // obtain reference to event manager to be able to respond to events
        EventManager manager = network.getEventManager();

        // wait for 3 minutes that the proxy has published its first RSD
        if (!manager.waitForAnyOccurence(proxy1.getNodeName(),
                HiddenServiceEventType.BOB_DESC_PUBLISHED_RECEIVED,
                3L * 60L * 1000L)) {

            // failed to publish an RSD
            System.out.println("Failed to publish an RSD!");
            System.exit(0);
        }
        System.out.println("Successfully published an RSD!");

        // create server application
        ServerApplication server = network.createServer("server", hidServ1
                .getServicePort());

        // create client application
        ClientApplication client = network.createClient("client", hidServ1
                .determineOnionAddress(), hidServ1.getVirtualPort(), proxy2
                .getSocksPort());

        // create event listener to listen for client and server application
        // events
        EventListener clientAndServerEventListener = new EventListener() {

            private long requestReceivedAtServer;

            // remember time when request was sent and when it was received
            private long requestSentFromClient;

            public void handleEvent(Event event) {
                if (event.getType() == ClientEventType.CLIENT_SENDING_REQUEST) {
                    requestSentFromClient = event.getOccurrenceTime();
                } else if (event.getType() == ServerEventType.SERVER_RECEIVING_REQUEST_SENDING_REPLY) {
                    requestReceivedAtServer = event.getOccurrenceTime();
                    System.out.println("Request took "
                            + (requestReceivedAtServer - requestSentFromClient)
                            + " milliseconds from client to server!");
                } else if (event.getType() == ClientEventType.CLIENT_REPLY_RECEIVED) {
                    System.out
                            .println("Request took "
                                    + (event.getOccurrenceTime() - requestSentFromClient)
                                    + " milliseconds for the round-trip and "
                                    + (event.getOccurrenceTime() - requestReceivedAtServer)
                                    + " milliseconds from server to client!");
                }
            }
        };

        // register event handler for client and server application events
        manager.addEventListener(client.getClientApplicationName(),
                clientAndServerEventListener);
        manager.addEventListener(server.getServerApplicationName(),
                clientAndServerEventListener);

        // start server
        server.startListening();

        // perform at most five request with a timeout of 45 seconds each
        client.startRequests(5, 45000, true);

        // block this thread as long as client requests are running
        manager.waitForAnyOccurence(client.getClientApplicationName(),
                ClientEventType.CLIENT_REQUESTS_PERFORMED);

        // shut down proxy
        network.shutdownNodes();

        // Shut down the JVM
        System.out.println("Goodbye.");
        System.exit(0);
    }
}
