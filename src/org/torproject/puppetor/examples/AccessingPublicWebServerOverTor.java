/*
 * Copyright 2007 Karsten Loesing
 * Copyright 2008-2009 Karsten Loesing and Sebastian Hahn
 * See LICENSE for licensing information
 */
package org.torproject.puppetor.examples;

import java.rmi.RemoteException;

import org.torproject.puppetor.ClientApplication;
import org.torproject.puppetor.ClientEventType;
import org.torproject.puppetor.Event;
import org.torproject.puppetor.EventListener;
import org.torproject.puppetor.EventManager;
import org.torproject.puppetor.Network;
import org.torproject.puppetor.NetworkFactory;
import org.torproject.puppetor.ProxyNode;
import org.torproject.puppetor.PuppeTorException;


/**
 * Example for accessing a public web server (here: <code>www.google.com</code>)
 * over Tor to measure the access time.
 * 
 * @author kloesing
 */
public class AccessingPublicWebServerOverTor {

    /**
     * Sets up and runs the test.
     * 
     * @param args
     *            Command-line arguments (ignored).
     * @throws PuppeTorException
     *             Thrown if there is a problem with the JVM-external Tor
     *             processes that we cannot handle.
     * @throws RemoteException
     *             Thrown if an error occurs when accessed remotely.
     */
    public static void main(String[] args) throws PuppeTorException,
            RemoteException {

        // though we only need a single proxy, we always need to create a
        // network to initialize a test case.
        Network network = NetworkFactory.createNetwork("example1");

        // create a single proxy node with name "proxy"
        ProxyNode proxy = network.createProxy("proxy");

        // write configuration of proxy node
        network.writeConfigurations();

        // start proxy node and wait until it has opened a circuit with a
        // timeout of 5 seconds
        if (!network.startNodes(5000)) {

            // failed to start the proxy
            System.out.println("Failed to start the node!");
            return;
        }
        System.out.println("Successfully started the node!");

        // hup until proxy has built circuits (5 retries, 10 seconds timeout
        // each)
        if (!network.hupUntilUp(5, 10000)) {

            // failed to build circuits
            System.out.println("Failed to build circuits!");
            System.exit(0);
        }
        System.out.println("Successfully built circuits!");

        // create client application
        ClientApplication client = network.createClient("client",
                "www.google.com", 80, proxy.getSocksPort());

        // create event listener to listen for client application events
        EventListener clientEventListener = new EventListener() {

            // remember time when request was sent
            private long before;

            public void handleEvent(Event event) {
                if (event.getType() == ClientEventType.CLIENT_SENDING_REQUEST) {
                    before = System.currentTimeMillis();
                } else if (event.getType() == ClientEventType.CLIENT_REPLY_RECEIVED) {
                    System.out.println("Request took "
                            + (System.currentTimeMillis() - before)
                            + " milliseconds");
                }
            }
        };

        // obtain reference to event manager to be able to respond to events
        EventManager manager = network.getEventManager();

        // register event handler for client application events
        manager.addEventListener(client.getClientApplicationName(),
                clientEventListener);

        // perform at most three request with a timeout of 20 seconds each
        client.startRequests(3, 20000, true);

        // block this thread as long as client requests are running
        manager.waitForAnyOccurence(client.getClientApplicationName(),
                ClientEventType.CLIENT_REQUESTS_PERFORMED);

        // wait a second before shutting down the proxy
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
        }

        // shut down proxy
        network.shutdownNodes();

        // wait another second before exiting the application
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
        }

        // Shut down the JVM
        System.out.println("Goodbye.");
        System.exit(0);
    }
}
