/*
 * Copyright 2007 Karsten Loesing
 * Copyright 2008-2009 Karsten Loesing and Sebastian Hahn
 * See LICENSE for licensing information
 */
package org.torproject.puppetor.examples;

import java.rmi.RemoteException;

import org.torproject.puppetor.ClientApplication;
import org.torproject.puppetor.ClientEventType;
import org.torproject.puppetor.Event;
import org.torproject.puppetor.EventListener;
import org.torproject.puppetor.EventManager;
import org.torproject.puppetor.HiddenService;
import org.torproject.puppetor.HiddenServiceEventType;
import org.torproject.puppetor.Network;
import org.torproject.puppetor.NetworkFactory;
import org.torproject.puppetor.PuppeTorException;
import org.torproject.puppetor.RouterNode;
import org.torproject.puppetor.ServerApplication;


/**
 * Example for advertising and accessing a hidden service over a private Tor
 * network.
 * 
 * @author kloesing
 */
public class AdvertisingAndAccessingHiddenServiceOverPrivateTorNetwork {

    /**
     * Sets up and runs the test.
     * 
     * @param args
     *            Command-line arguments (ignored).
     * @throws PuppeTorException
     *             Thrown if there is a problem with the JVM-external Tor
     *             processes that we cannot handle.
     * @throws RemoteException
     *             Thrown if an error occurs when accessed remotely.
     */
    public static void main(String[] args) throws PuppeTorException,
            RemoteException {

        // create a network to initialize the test case
        Network network = NetworkFactory.createNetwork("example4");

        // create three router nodes
        RouterNode router1 = network.createRouter("router1");
        network.createRouter("router2");
        RouterNode router3 = network.createRouter("router3");

        // create only one directory node
        network.createDirectory("dir1");

        // add hidden service
        HiddenService hidServ1 = router1.addHiddenService("hidServ");

        // configure nodes of this network to be part of a private network
        network.configureAsPrivateNetwork();

        // write node configurations
        network.writeConfigurations();

        // start nodes and wait until they have opened a circuit with a timeout
        // of 5 seconds
        if (!network.startNodes(5000)) {

            // failed to start the nodes
            System.out.println("Failed to start nodes!");
            System.exit(0);
        }
        System.out.println("Successfully started nodes!");

        // hup until nodes have built circuits (6 retries, 12 minutes timeout
        // each)
        if (!network.hupUntilUp(6, 12L * 60L * 1000L)) {

            // failed to build circuits
            System.out.println("Failed to build circuits!");
            System.exit(0);
        }
        System.out.println("Successfully built circuits!");

        // obtain reference to event manager to be able to respond to events
        EventManager manager = network.getEventManager();

        // wait for 1 hour that the proxy has published its first RSD
        if (!manager.waitForAnyOccurence(router1.getNodeName(),
                HiddenServiceEventType.BOB_DESC_PUBLISHED_RECEIVED,
                1L * 60L * 60L * 1000L)) {
            // failed to publish an RSD
            System.out.println("Failed to publish an RSD!");
            System.exit(0);
        }
        System.out.println("Successfully published an RSD!");

        // create server application
        ServerApplication server = network.createServer("server", hidServ1
                .getServicePort());

        // create client application
        ClientApplication client = network.createClient("client", hidServ1
                .determineOnionAddress(), hidServ1.getVirtualPort(), router3
                .getSocksPort());

        // register event listener
        EventListener clientAndServerEventListener = new EventListener() {
            public void handleEvent(Event event) {
                System.out.println("Handling event: " + event.getMessage());
            }
        };
        manager.addEventListener(client.getClientApplicationName(),
                clientAndServerEventListener);
        manager.addEventListener(server.getServerApplicationName(),
                clientAndServerEventListener);

        // start server
        server.startListening();
        System.out.println("Started server");

        // perform at most five request with a timeout of 45 seconds each
        client.startRequests(5, 45000, true);

        // wait for request to be performed
        manager.waitForAnyOccurence(client.getClientApplicationName(),
                ClientEventType.CLIENT_REQUESTS_PERFORMED);

        // shut down nodes
        network.shutdownNodes();

        // Shut down the JVM
        System.out.println("Goodbye.");
        System.exit(0);
    }
}
