/*
 * Copyright 2007 Patrick Geyer, Johannes Jungkunst, Karsten Loesing, and
 * Stefan Schilling
 * Copyright 2008-2009 Karsten Loesing and Sebastian Hahn
 * See LICENSE for licensing information
 */
package org.torproject.puppetor.examples;

import java.rmi.RemoteException;

import org.torproject.puppetor.Network;
import org.torproject.puppetor.NetworkFactory;
import org.torproject.puppetor.PuppeTorException;


/**
 * Example for starting and running a private network of routers and directories
 * that may be accessed via RMI, e.g. to be merged with another network.
 * 
 * Note that the <code>rmiregistry</code> must be running in order to run this
 * example.
 * 
 * TODO further requirements for starting this example
 * 
 * @author pgeyer, jjungkunst, sschilling
 */
public class LongRunningNetwork {

    /**
     * Sets up and runs the test.
     * 
     * @param args
     *            Command-line arguments (ignored).
     * @throws PuppeTorException
     *             Thrown if there is a problem with the JVM-external Tor
     *             processes that we cannot handle.
     * @throws RemoteException
     *             Thrown if an error occurs when accessed remotely.
     */
    public static void main(String[] args) throws PuppeTorException,
            RemoteException {

        // create a network to initialize the test case
        Network network = NetworkFactory.createNetwork("example5");

        // create three router nodes
        network.createRouter("router1");
        network.createRouter("router2");
        network.createRouter("router3");

        // create two directory nodes
        network.createDirectory("dir1");
        network.createDirectory("dir2");

        // configure nodes of this network to be part of a private network
        network.configureAsPrivateNetwork();

        // write node configurations
        network.writeConfigurations();

        // start proxy node and wait until it has opened a circuit with a
        // timeout of 5 seconds
        if (!network.startNodes(5000)) {
            System.out.println("Failed to start nodes!");
            System.exit(0);
        }
        System.out.println("Successfully started nodes!");

        // hup until nodes have built circuits (10 retries, 10 seconds timeout
        // each)
        if (!network.hupUntilUp(10, 10000)) {

            // failed to build circuits
            System.out.println("Failed to build circuits!");
            System.exit(0);
        }
        System.out.println("Successfully built circuits!");

        // bind the network to rmiregistry
        if (!network.bindAtRmiregistry()) {

            // failed to bind at rmiregistry
            System.out.println("Failed to bind at rmiregistry! "
                    + "Is rmiregistry running?");
            System.exit(0);
        }
        System.out.println("Bound at rmiregistry to name \""
                + network.getNetworkName() + "\"!");

        // wait until the end of time
        System.out.println("Waiting until the end of time... "
                + "Interrupt with Ctrl-C!");
        try {
            Thread.sleep(Long.MAX_VALUE);
        } catch (InterruptedException e) {
            // do nothing
        }
    }
}