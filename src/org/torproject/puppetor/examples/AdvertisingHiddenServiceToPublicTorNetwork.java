/*
 * Copyright 2007 Karsten Loesing
 * Copyright 2008-2009 Karsten Loesing and Sebastian Hahn
 * See LICENSE for licensing information
 */
package org.torproject.puppetor.examples;

import java.rmi.RemoteException;

import org.torproject.puppetor.Event;
import org.torproject.puppetor.EventListener;
import org.torproject.puppetor.EventManager;
import org.torproject.puppetor.HiddenServiceEventType;
import org.torproject.puppetor.Network;
import org.torproject.puppetor.NetworkFactory;
import org.torproject.puppetor.NodeEventType;
import org.torproject.puppetor.ProxyNode;
import org.torproject.puppetor.PuppeTorException;


/**
 * Example for advertising a hidden service to the public Tor network and
 * observing the publication of rendezvous service descriptors.
 * 
 * @author kloesing
 */
public class AdvertisingHiddenServiceToPublicTorNetwork {

    /**
     * Sets up and runs the test.
     * 
     * @param args
     *            Command-line arguments (ignored).
     * @throws PuppeTorException
     *             Thrown if there is a problem with the JVM-external Tor
     *             processes that we cannot handle.
     * @throws RemoteException
     *             Thrown if an error occurs when accessed remotely.
     */
    public static void main(String[] args) throws PuppeTorException,
            RemoteException {

        // create a network to initialize the test case
        Network network = NetworkFactory.createNetwork("example2");
        
        // create a single proxy node
        ProxyNode proxy = network.createProxy("proxy");

        // add hidden service to the configuration
        proxy.addHiddenService("hidServ");

        // write configuration of proxy node
        network.writeConfigurations();

        // create event listener to listen for events from our proxy
        EventListener proxyEventListener = new EventListener() {

            // remember time when request was sent
            private long circuitOpened = -1;

            public void handleEvent(Event event) {
                if (event.getType() == NodeEventType.NODE_CIRCUIT_OPENED) {
                    if (circuitOpened == -1) {
                        circuitOpened = System.currentTimeMillis();
                    }
                } else if (event.getType() == HiddenServiceEventType.BOB_DESC_PUBLISHED_RECEIVED) {
                    System.out.println("RSD published "
                            + (System.currentTimeMillis() - circuitOpened)
                            + " milliseconds after first circuit was opened");
                }
            }
        };

        // obtain reference to event manager to be able to respond to events
        EventManager manager = network.getEventManager();

        // register event handler for proxy events
        manager.addEventListener(proxy.getNodeName(), proxyEventListener);

        // start proxy node and wait until it has opened a circuit with a
        // timeout of 5 seconds
        if (!network.startNodes(5000)) {

            // failed to start the proxy
            System.out.println("Failed to start the node!");
            System.exit(0);
        }
        System.out.println("Successfully started the node!");

        // hup until proxy has built circuits (5 retries, 10 seconds timeout
        // each)
        if (!network.hupUntilUp(5, 10000)) {

            // failed to build circuits
            System.out.println("Failed to build circuits!");
            System.exit(0);
        }
        System.out.println("Successfully built circuits!");

        // let it run for 2 minutes and observe when RSDs are published...
        System.out
                .println("Waiting for 2 minutes and observing RSD publications...");

        try {
            Thread.sleep(2L * 60L * 1000L);
        } catch (InterruptedException e) {
            // do nothing
        }

        // shut down proxy
        network.shutdownNodes();

        // Shut down the JVM
        System.out.println("Goodbye.");
        System.exit(0);
    }
}
