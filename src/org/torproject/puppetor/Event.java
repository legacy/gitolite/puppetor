/*
 * Copyright 2007 Karsten Loesing
 * Copyright 2008-2009 Karsten Loesing and Sebastian Hahn
 * See LICENSE for licensing information
 */
package org.torproject.puppetor;

import java.io.Serializable;

/**
 * An <code>Event</code> is created for every state change of an asynchronous
 * system component, e.g. a Tor process or a client/server application running
 * as thread in the background. In contrast to <code>NodeState</code> an
 * <code>Event</code> cannot be a pre- or postconditions for a method
 * invocation. There is no prescribed order in which events are fired by a
 * certain process or application. Some events can be fired only once, others
 * possibly multiple times. All management operations for events are contained
 * in the <code>EventManager</code>.
 * 
 * @author kloesing
 */
public interface Event extends Serializable {

    /**
     * Returns the name of the source of this event, which can be a Tor process
     * or a client/server application running as thread in the background.
     * 
     * @return The event source.
     */
    public abstract String getSource();

    /**
     * Returns the event type.
     * 
     * @return The event type.
     */
    public abstract EventType getType();

    /**
     * Returns the original message that lead to firing this event.
     * 
     * @return The original message.
     */
    public abstract String getMessage();

    /**
     * Returns the occurrence time of this event, which is either the parsed
     * time from a Tor log statement, or the current system time when a
     * client/server application fired this event.
     * 
     * @return The occurrence time.
     */
    public abstract long getOccurrenceTime();
}
