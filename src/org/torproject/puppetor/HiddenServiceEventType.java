/*
 * Copyright 2007 Karsten Loesing
 * Copyright 2008-2009 Karsten Loesing and Sebastian Hahn
 * See LICENSE for licensing information
 */
package org.torproject.puppetor;

/**
 * Event types that can be fired by all Tor processes performing hidden-service
 * operations.
 */
@SuppressWarnings("serial")
public class HiddenServiceEventType implements EventType {

    /**
     * String identifying the type of the event type.
     */
    String typeString;

    /**
     * Creates a new event type with the given type string.
     * 
     * @param typeString
     *            String identifying the type of the event type.
     */
    public HiddenServiceEventType(String typeString) {
        this.typeString = typeString;
    }

    public String getTypeName() {
        return this.typeString;
    }

    /**
     * Alice has received an onion request; this event is parsed from a log
     * statement in connection_ap_handshake_rewrite_and_attach().
     */
    public static final HiddenServiceEventType ALICE_ONION_REQUEST_RECEIVED = new HiddenServiceEventType(
            "ALICE_ONION_REQUEST_RECEIVED");

    /**
     * Alice builds a circuit to a hidden service directory and sends a fetch
     * request for a hidden descriptor; this event is parsed from log
     * statements in rend_client_refetch_renddesc() for v0 descriptors and
     * directory_get_from_hs_dir() for v2 descriptors.
     */
    public static final HiddenServiceEventType ALICE_SENDING_FETCH_DESC = new HiddenServiceEventType(
            "ALICE_SENDING_FETCH_DESC");

    /**
     * Alice receives a reply to a previous fetch request for a hidden service
     * descriptors from a directory server; this event is parsed from a log
     * statement in connection_dir_client_reached_eof().
     */
    public static final HiddenServiceEventType ALICE_DESC_FETCHED_RECEIVED = new HiddenServiceEventType(
            "ALICE_DESC_FETCHED_RECEIVED");

    /**
     * Alice has built a circuit to a rendezvous point and sends an
     * ESTABLISH_RENDEZVOUS cell; this event is parsed from a log statement in
     * rend_client_send_establish_rendezvous().
     */
    public static final HiddenServiceEventType ALICE_BUILT_REND_CIRC_SENDING_ESTABLISH_RENDEZVOUS = new HiddenServiceEventType(
            "ALICE_BUILT_REND_CIRC_SENDING_ESTABLISH_RENDEZVOUS");

    /**
     * Alice receives a RENDEZVOUS_ESTABLISHED cell from a rendezvous point;
     * this event is parsed from a log statement in
     * rend_client_rendezvous_acked().
     */
    public static final HiddenServiceEventType ALICE_RENDEZVOUS_ESTABLISHED_RECEIVED = new HiddenServiceEventType(
            "ALICE_RENDEZVOUS_ESTABLISHED_RECEIVED");

    /**
     * Alice has built a circuit to an introduction point (which does not
     * automatically lead to sending an INTRODUCE1 cell, because the rendezvous
     * circuit might not be ready); this event is parsed from a log statement in
     * rend_client_introcirc_has_opened().
     */
    public static final HiddenServiceEventType ALICE_BUILT_INTRO_CIRC = new HiddenServiceEventType(
            "ALICE_BUILT_INTRO_CIRC");

    /**
     * Alice sends an INTRODUCE1 cell to an introduction point; this event is
     * parsed from a log statement in rend_client_send_introduction().
     */
    public static final HiddenServiceEventType ALICE_SENDING_INTRODUCE1 = new HiddenServiceEventType(
            "ALICE_SENDING_INTRODUCE1");

    /**
     * Alice has received an INTRODUCE_ACK cell as an acknowledgement to a
     * previously sent INTRODUCE1 cell; this event is parsed from a log
     * statement in rend_client_introduction_acked().
     */
    public static final HiddenServiceEventType ALICE_INTRODUCE_ACK_RECEIVED = new HiddenServiceEventType(
            "ALICE_INTRODUCE_ACK_RECEIVED");

    /**
     * Alice has received a RENDEZVOUS2 cell and can now open an application
     * connection to the client; this event is parsed from a log statement in
     * rend_client_receive_rendezvous().
     */
    public static final HiddenServiceEventType ALICE_RENDEZVOUS2_RECEIVED_APP_CONN_OPENED = new HiddenServiceEventType(
            "ALICE_RENDEZVOUS2_RECEIVED_APP_CONN_OPENED");

    /**
     * Bob has built a circuit to an introduction point and sends an
     * ESTABLISH_INTRO cell; this event is parsed from a log statement in
     * rend_service_intro_has_opened().
     */
    public static final HiddenServiceEventType BOB_BUILT_INTRO_CIRC_SENDING_ESTABLISH_INTRO = new HiddenServiceEventType(
            "BOB_BUILT_INTRO_CIRC_SENDING_ESTABLISH_INTRO");

    /**
     * Bob has received an INTRO_ESTABLISHED cell, i.e. a node has confirmed to
     * work as introduction point; this event is parsed from a log statement in
     * rend_service_intro_established().
     */
    public static final HiddenServiceEventType BOB_INTRO_ESTABLISHED_RECEIVED = new HiddenServiceEventType(
            "BOB_INTRO_ESTABLISHED_RECEIVED");

    /**
     * Bob posts a hidden service descriptor to the directory servers; this
     * event is parsed from a log statement in upload_service_descriptor().
     */
    public static final HiddenServiceEventType BOB_SENDING_PUBLISH_DESC = new HiddenServiceEventType(
            "BOB_SENDING_PUBLISH_DESC");

    /**
     * Bob received a response from a directory server to a previous publish
     * request; this event is parsed from a log statement in
     * connection_dir_client_reached_eof().
     */
    public static final HiddenServiceEventType BOB_DESC_PUBLISHED_RECEIVED = new HiddenServiceEventType(
            "BOB_DESC_PUBLISHED_RECEIVED");

    /**
     * Bob has received an INTRODUCE2 cell, i.e. a node wants to establish a
     * connection, and will now try to establish a circuit to the client's
     * rendezvous point; this event is parsed from a log statement in
     * rend_service_introduce().
     */
    public static final HiddenServiceEventType BOB_INTRODUCE2_RECEIVED = new HiddenServiceEventType(
            "BOB_INTRODUCE2_RECEIVED");

    /**
     * Bob has built a circuit to a rendezvous point and sends a RENDEZVOUS1
     * cell; this event is parsed from a log statement in
     * rend_service_rendezvous_has_opened().
     */
    public static final HiddenServiceEventType BOB_BUILT_REND_CIRC_SENDING_RENDEZVOUS1 = new HiddenServiceEventType(
            "BOB_BUILT_REND_CIRC_SENDING_RENDEZVOUS1");

    /**
     * Bob opens a connection to the actual hidden server; this event is parsed
     * from a log statement in connection_exit_begin_conn().
     */
    public static final HiddenServiceEventType BOB_APP_CONN_OPENED = new HiddenServiceEventType(
            "BOB_APP_CONN_OPENED");

    /**
     * The directory server has received a descriptor post request; this event
     * is parsed from a log statement in directory_handle_command_post().
     */
    public static final HiddenServiceEventType DIR_PUBLISH_DESC_RECEIVED = new HiddenServiceEventType(
            "DIR_PUBLISH_DESC_RECEIVED");

    /**
     * The directory server has received a descriptor fetch request; this event
     * is parsed from a log statement in directory_handle_command_get().
     */
    public static final HiddenServiceEventType DIR_FETCH_DESC_RECEIVED = new HiddenServiceEventType(
            "DIR_FETCH_DESC_RECEIVED");

    /**
     * The node received an ESTABLISH_INTRO cell, i.e. was requested to work as
     * introduction point, and replies with an INTRO_ESTABLISHED cell; this
     * event is parsed from a log statement in rend_mid_establish_intro().
     */
    public static final HiddenServiceEventType IPO_RECEIVED_ESTABLISH_INTRO_SENDING_INTRO_ESTABLISHED = new HiddenServiceEventType(
            "IPO_RECEIVED_ESTABLISH_INTRO_SENDING_INTRO_ESTABLISHED");

    /**
     * The introduction point received an INTRODUCE1 cell and reacts by sending
     * an INTRODUCE2 cell to Bob and an INTRODUCE_ACK cell to Alice; this event
     * is parsed from a log statement in rend_mid_introduce().
     */
    public static final HiddenServiceEventType IPO_RECEIVED_INTRODUCE1_SENDING_INTRODUCE2_AND_INTRODUCE_ACK = new HiddenServiceEventType(
            "IPO_RECEIVED_INTRODUCE1_SENDING_INTRODUCE2_AND_INTRODUCE_ACK");

    /**
     * The node received an ESTABLISH_RENDEZVOUS cell, i.e. was requested to
     * work as rendezvous point, and replies with a RENDEZVOUS_ESTABLISHED cell;
     * this event is parsed from a log statement in
     * rend_mid_establish_rendezvous().
     */
    public static final HiddenServiceEventType RPO_RECEIVED_ESTABLISH_RENDEZVOUS_SENDING_RENDEZVOUS_ESTABLISHED = new HiddenServiceEventType(
            "RPO_RECEIVED_ESTABLISH_RENDEZVOUS_SENDING_RENDEZVOUS_ESTABLISHED");

    /**
     * The rendezvous point received a RENDEZVOUS1 cell and reacts by sending a
     * RENDEZVOUS2 cell to Alice; this event is parsed from a log statement in
     * rend_mid_rendezvous().
     */
    public static final HiddenServiceEventType RPO_RECEIVING_RENDEZVOUS1_SENDING_RENDEZVOUS2 = new HiddenServiceEventType(
            "RPO_RECEIVING_RENDEZVOUS1_SENDING_RENDEZVOUS2");

}

