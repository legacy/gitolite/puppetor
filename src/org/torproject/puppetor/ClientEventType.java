/*
 * Copyright 2007 Karsten Loesing
 * Copyright 2008-2009 Karsten Loesing and Sebastian Hahn
 * See LICENSE for licensing information
 */
package org.torproject.puppetor;

/**
 * Event types that can be fired by a client application running as thread in
 * the background.
 */
@SuppressWarnings("serial")
public class ClientEventType implements EventType {

    /**
     * String identifying the type of the event type.
     */
    String typeString;

    /**
     * Creates a new event type with the given type string.
     * 
     * @param typeString
     *            String identifying the type of the event type.
     */
    public ClientEventType(String typeString) {
        this.typeString = typeString;
    }

    public String getTypeName() {
        return this.typeString;
    }

    /**
     * The client application is sending a request; this event is fired
     * internally and not parsed from a log statement from Tor.
     */
    public static final ClientEventType CLIENT_SENDING_REQUEST = new ClientEventType(
            "CLIENT_SENDING_REQUEST");

    /**
     * The client application has received a reply to a previously sent request;
     * this event is fired internally and not parsed from a log statement from
     * Tor.
     */
    public static final ClientEventType CLIENT_REPLY_RECEIVED = new ClientEventType(
            "CLIENT_REPLY_RECEIVED");

    /**
     * The client application has given up waiting for the reply to a previously
     * sent request; this event is fired internally and not parsed from a log
     * statement from Tor.
     */
    public static final ClientEventType CLIENT_GAVE_UP_REQUEST = new ClientEventType(
            "CLIENT_GAVE_UP_REQUEST");

    /**
     * The client application has completed a series of requests, whether they
     * were successful or not; this event is fired internally and not parsed
     * from a log statement from Tor.
     */
    public static final ClientEventType CLIENT_REQUESTS_PERFORMED = new ClientEventType(
            "CLIENT_REQUESTS_PERFORMED");
}
