/*
 * Copyright 2007 Karsten Loesing
 * Copyright 2008-2009 Karsten Loesing and Sebastian Hahn
 * See LICENSE for licensing information
 */
package org.torproject.puppetor;

import java.io.File;
import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.List;

/**
 * <p>
 * A <code>ProxyNode</code> represents a Tor process that is configured as
 * onion proxy, i.e. to relay traffic from a local application to the Tor
 * network and vice versa, and does not route traffic on behalf of remote
 * applications. It is the superclass for other node types that extend the
 * configuration of a </code>ProxyNode</code>.
 * </p>
 * 
 * <p>
 * <b>Pay extra attention when using in private network!</b> Using proxy nodes
 * in private networks in the same way as router nodes will fail! Tor has two
 * different strategies for downloading network status documents: Directory
 * caches (router nodes) download these documents after every HUP signal and
 * then accept all contained router entries. But directory clients (proxy nodes)
 * only download network status documents, if the most recent download lies at
 * least 30 minutes in the past, and then accept only those of the contained
 * router entries that are at least 10 minutes old. However, when starting all
 * nodes of a private network at once, directories cannot contain 10 minutes old
 * router descriptors. You have at least the following options to cope with this
 * problem:
 * </p>
 * 
 * <ul>
 * <li>Use router nodes instead of proxy nodes,</li>
 * <li>start proxy nodes with a delay of at least 10 minutes to be sure that
 * the router descriptors stored at directory authorities will be accepted by
 * directory clients, or</li>
 * <li>change the constants <code>ESTIMATED_PROPAGATION_TIME</code> and
 * <code>NETWORKSTATUS_CLIENT_DL_INTERVAL</code> in Tor to values smaller than
 * your overall HUP time for starting the network.</li>
 * </ul>
 * 
 * @author kloesing
 */
public interface ProxyNode extends Remote {

    /**
     * Adds the entries for a hidden service to the configuration of this node.
     * 
     * @param serviceName
     *            Name of the hidden service that will be used as name for the
     *            hidden service directory. May neither be <code>null</code>
     *            or a zero-length string.
     * @param servicePort
     *            The TCP port on which the service will be available for
     *            requests. This can, but need not be different from the virtual
     *            port that is announced to clients. May not be negative or
     *            greater than 65535.
     * @param virtualPort
     *            The virtual TCP port that this hidden service runs on as it is
     *            announced to clients. May not be negative or greater than
     *            65535.
     * @return <code>HiddenService</code> object containing the configuration
     *         of the created hidden service.
     * @throws IllegalArgumentException
     *             Thrown if an invalid value is given for either of the
     *             parameters.
     * @throws RemoteException
     *             Thrown if an error occurs when accessed remotely.
     */
    public abstract HiddenService addHiddenService(String serviceName,
            int servicePort, int virtualPort) throws RemoteException;

    /**
     * Adds the entries for a hidden service with virtual port 80 to the
     * configuration of this node.
     * 
     * @param serviceName
     *            Name of the hidden service that will be used as name for the
     *            hidden service directory. May neither be <code>null</code>
     *            or a zero-length string.
     * @param servicePort
     *            The TCP port on which the service will be available for
     *            requests. This can, but need not be different from the virtual
     *            port that is announced to clients. May not be negative or
     *            greater than 65535.
     * @return <code>HiddenService</code> object containing the configuration
     *         of the created hidden service.
     * @throws IllegalArgumentException
     *             Thrown if an invalid value is given for either of the
     *             parameters.
     * @throws RemoteException
     *             Thrown if an error occurs when accessed remotely.
     */
    public abstract HiddenService addHiddenService(String serviceName,
            int servicePort) throws RemoteException;

    /**
     * Adds the entries for a hidden service with an automatically assigned
     * service port and virtual port 80 to the configuration of this node.
     * 
     * service port automatically assigned virtual port 80
     * 
     * @param serviceName
     *            Name of the hidden service that will be used as name for the
     *            hidden service directory. May neither be <code>null</code>
     *            or a zero-length string.
     * @return <code>HiddenService</code> object containing the configuration
     *         of the created hidden service.
     * @throws IllegalArgumentException
     *             Thrown if an invalid value is given for the parameter.
     * @throws RemoteException
     *             Thrown if an error occurs when accessed remotely.
     */
    public abstract HiddenService addHiddenService(String serviceName)
            throws RemoteException;

    /**
     * Adds the given configuration string, consisting of "<configuration key>
     * <configuration value>", to the configuration of this node.
     * 
     * @param configurationString
     *            The configuration string to be added.
     * @throws IllegalArgumentException
     *             Thrown if the given configuration string is either
     *             <code>null</code>, a zero-length string, or does not
     *             consist of configuration key and value.
     * @throws RemoteException
     *             Thrown if an error occurs when accessed remotely.
     */
    public abstract void addConfiguration(String configurationString)
            throws RemoteException;

    /**
     * Adds the given configuration strings, each consisting of "<configuration
     * key> <configuration value>", to the configuration of this node.
     * 
     * @param configurationStrings
     *            A list of the configuration strings to be added.
     * @throws IllegalArgumentException
     *             Thrown if the given list is <code>null</code>, or any of
     *             the contained strings is either <code>null</code>, a
     *             zero-length string, or does not consist of configuration key
     *             and value.
     * @throws RemoteException
     *             Thrown if an error occurs when accessed remotely.
     */
    public abstract void addConfigurations(List<String> configurationStrings)
            throws RemoteException;

    /**
     * Replaces the first configuration string, consisting of "<configuration
     * key> <configuration value>", that contains the same configuration key as
     * <code>configurationString</code> by this new configuration string; if
     * multiple occurrences of the given configuration key are found, only the
     * first occurrence is replaced; if no configuration can be found, the
     * configuration string is appended.
     * 
     * @param configurationString
     *            The replacing configuration string.
     * @throws IllegalArgumentException
     *             Thrown if the given configuration string is either
     *             <code>null</code>, a zero-length string, or does not
     *             consist of configuration key and value.
     * @throws RemoteException
     *             Thrown if an error occurs when accessed remotely.
     */
    public abstract void replaceConfiguration(String configurationString)
            throws RemoteException;

    /**
     * Removes all configuration strings containing the given configuration key
     * in "<configuration key> <configuration value>", regardless of their
     * configuration value.
     * 
     * @param configurationKey
     *            The configuration key to remove.
     * @throws IllegalArgumentException
     *             Thrown if the given configuration key is either
     *             <code>null</code> or a zero-length key.
     * @throws RemoteException
     *             Thrown if an error occurs when accessed remotely.
     */
    public abstract void removeConfiguration(String configurationKey)
            throws RemoteException;

    /**
     * Returns the name of this node.
     * 
     * @return The name of this node.
     * @throws RemoteException
     *             Thrown if an error occurs when accessed remotely.
     */
    public abstract String getNodeName() throws RemoteException;

    /**
     * Returns the state of this node.
     * 
     * @return The state of this node.
     * @throws RemoteException
     *             Thrown if an error occurs when accessed remotely.
     */
    public abstract NodeState getNodeState() throws RemoteException;

    /**
     * Sends a HUP command to the process via its control port to restart it;
     * can only be done if the node has already been started, i.e. is in state
     * <code>NodeState.RUNNING</code>!
     * 
     * @throws PuppeTorException
     *             Thrown if an I/O problem occurs while sending the HUP signal.
     * @throws IllegalStateException
     *             Thrown if node is not in state <code>NodeState.RUNNING</code>.
     * @throws RemoteException
     *             Thrown if an error occurs when accessed remotely.
     */
    public abstract void hup() throws PuppeTorException, RemoteException;

    /**
     * Shuts down the Tor process corresponding to this node immediately. This
     * is done by sending the <code>SHUTDOWN</code> signal twice, so that
     * those nodes extending <code>ProxyNode</code> which have opened their OR
     * port shutdown immediately, too.
     * 
     * @throws IllegalStateException
     *             Thrown if this node is not in state
     *             <code>NodeState.RUNNING</code>.
     * @throws PuppeTorException
     *             Thrown if an I/O problem occurs while sending the
     *             <code>SHUTDOWN</code> signal.
     * @throws RemoteException
     *             Thrown if an error occurs when accessed remotely.
     */
    public abstract void shutdown() throws PuppeTorException, RemoteException;

    /**
     * Starts the Tor process for this node and connects to the control port as
     * soon as it is opened. <b>In order for this method to succeed it is
     * absolutely necessary, that logging on the console is not changed in the
     * configuration of this node to a higher level than NOTICE, because the
     * output is parsed to see when the control port is opened.</b>
     * 
     * @param maximumTimeToWaitInMillis
     *            Maximum time in milliseconds we will wait for the Tor process
     *            to be started and the control port being opened. If this value
     *            is negative or zero, we will wait potentially forever.
     * @return <code>true</code> if the node could be started successfully,
     *         <code>false</code> otherwise.
     * @throws IllegalStateException
     *             Thrown if node is not in state
     *             <code>NodeState.CONFIGURATION_WRITTEN</code>, i.e. if
     *             either configuration has not been written or the process has
     *             already been started.
     * @throws PuppeTorException
     *             Thrown if either the process could not be started, or the
     *             connection to the control port could not be established.
     * @throws RemoteException
     *             Thrown if an error occurs when accessed remotely.
     */
    public abstract boolean startNode(long maximumTimeToWaitInMillis)
            throws PuppeTorException, RemoteException;

    /**
     * Writes the configuration of this node to the <code>torrc</code> file in
     * its working directory and changes the state to
     * <code>NodeState.CONFIGURATION_WRITTEN</code>, if it was in state
     * <code>NodeState.CONFIGURING</code> before.
     * 
     * @throws PuppeTorException
     *             Thrown if the configuration file <code>torrc</code> cannot
     *             be written to disk.
     * @throws RemoteException
     *             Thrown if an error occurs when accessed remotely.
     */
    public abstract void writeConfiguration() throws PuppeTorException,
            RemoteException;

    /**
     * Returns the SOCKS port of this node.
     * 
     * @return The SOCKS port of this node.
     * @throws RemoteException
     *             Thrown if an error occurs when accessed remotely.
     */
    public abstract int getSocksPort() throws RemoteException;

    /**
     * Returns the control port of this node.
     * 
     * @return The control port of this node.
     * @throws RemoteException
     *             Thrown if an error occurs when accessed remotely.
     */
    public abstract int getControlPort() throws RemoteException;

    /**
     * Returns (a copy of) the list of strings containing the configuration of
     * this node.
     * 
     * @return (A copy of) the list of strings containing the configuration of
     *         this node.
     * @throws RemoteException
     *             Thrown if an error occurs when accessed remotely.
     */
    public abstract List<String> getConfiguration() throws RemoteException;

    /**
     * Configures this node to use the given tor executable rather than the
     * one that is in the system path.
     *
     * @param executable
     *            The tor executable that will be used.
     * @throws IllegalStateException
     *             Thrown if node is not in state
     *             <code>NodeState.CONFIGURING</code>.
     * @throws IllegalArgumentException
     *             Thrown if the passed file does not exist or is a directory.
     */
    public abstract void setTorExecutable(File executable) throws RemoteException;

}
