/*
 * Copyright 2007 Karsten Loesing
 * Copyright 2008-2009 Karsten Loesing and Sebastian Hahn
 * See LICENSE for licensing information
 */
package org.torproject.puppetor;

/**
 * Event types that can be fired by all Tor processes.
 */
@SuppressWarnings("serial")
public class NodeEventType implements EventType {

    /**
     * String identifying the type of the event type.
     */
    String typeString;

    /**
     * Creates a new event type with the given type string.
     * 
     * @param typeString
     *            String identifying the type of the event type.
     */
    public NodeEventType(String typeString) {
        this.typeString = typeString;
    }

    public String getTypeName() {
        return this.typeString;
    }

    /**
     * The node was started and we managed to connect to its control port; this
     * event is fired internally and not parsed from a log statement from Tor.
     */
    public static final NodeEventType NODE_STARTED = new NodeEventType(
            "NODE_STARTED");

    /**
     * The node has opened its control port; this event is parsed from a log
     * statement in connection_create_listener().
     */
    public static final NodeEventType NODE_CONTROL_PORT_OPENED = new NodeEventType(
            "NODE_CONTROL_PORT_OPENED");

    /**
     * The node which has successfully opened a circuit; this event is parsed
     * from a log statement in circuit_send_next_onion_skin().
     */
    public static final NodeEventType NODE_CIRCUIT_OPENED = new NodeEventType(
            "NODE_CIRCUIT_OPENED");

    /**
     * The node was stopped; this event is fired internally and not parsed from
     * a log statement from Tor.
     */
    public static final NodeEventType NODE_STOPPED = new NodeEventType(
            "NODE_STOPPED");
}