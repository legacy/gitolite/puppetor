/*
 * Copyright 2007 Karsten Loesing
 * Copyright 2008-2009 Karsten Loesing and Sebastian Hahn
 * See LICENSE for licensing information
 */
package org.torproject.puppetor;

import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;

import org.torproject.puppetor.impl.NetworkImpl;


/**
 * The <code>NetworkFactory</code> is a concrete factory that can create
 * <code>Network</code> instances.
 * 
 * TODO At the moment, this class uses the concrete class NetworkImpl to
 * implement its only factory method. If we want to make this a real abstract
 * factory, we need to replace the concrete constructor by reading the class
 * name of the class implementing Network from a property file and invoking its
 * constructor using reflection. Currently, this is the only place where we
 * reference a class from the impl package.
 * 
 * @author kloesing
 */
public abstract class NetworkFactory {

    /**
     * Creates a new network that is required for a test run. The new network is
     * initially unpopulated and creates its own working directory at
     * test-env/randomTestID/. The network automatically assigns port numbers to
     * newly created nodes starting at <code>7000</code>.
     * 
     * @param networkName
     *            Name of this network configuration.
     * @return A new network instance.
     * @throws RemoteException
     *             Thrown if an error occurs when accessed remotely.
     */
    public static Network createNetwork(String networkName)
            throws RemoteException {
        return new NetworkImpl(networkName);
    }

    /**
     * Creates a new network that is required for a test run. The new network is
     * initially unpopulated and creates its own working directory at
     * test-env/randomTestID/. The network automatically assigns port numbers to
     * newly created nodes starting at <code>startPort</code>.
     * 
     * @param networkName
     *            Name of this network configuration.
     * @param startPort
     *            The initial value for automatically assigned port numbers of
     *            nodes created by this <code>Network</code>; must be a value
     *            between <code>1024</code> and <code>65535</code>.
     *            Applications need to ensure that there are enough ports left
     *            to the maximum number port <code>65535</code> for all
     *            created nodes.
     * @return A new network instance.
     * @throws RemoteException
     *             Thrown if an error occurs when accessed remotely.
     */
    public static Network createNetwork(String networkName, int startPort)
            throws RemoteException {
        return new NetworkImpl(networkName, startPort);
    }

    /**
     * Connects to a remote network and returns a reference on it.
     * 
     * @param remoteHost
     *            The IP address and port of the remote <code>rmiregistry</code>.
     * @param bindingName
     *            The name under which the other network is bound at the remote
     *            <code>rmiregistry</code>.
     * @return Reference on the remote network.
     * @throws PuppeTorException
     *             Thrown if an error occurs when looking up the remote network
     *             at the remote <code>rmiregistry</code>.
     * @throws RemoteException
     *             Thrown if an error occurs when accessed remotely.
     */
    public static Network connectToNetwork(String remoteHost, String bindingName)
            throws PuppeTorException, RemoteException {
        Network remoteNetwork = null;
        try {
            remoteNetwork = (Network) Naming.lookup("rmi://" + remoteHost + "/"
                    + bindingName);
        } catch (MalformedURLException e) {
            PuppeTorException ex = new PuppeTorException(
                    "Cannot connect to remote network!", e);
            throw ex;
        } catch (NotBoundException e) {
            PuppeTorException ex = new PuppeTorException(
                    "Cannot connect to remote network!", e);
            throw ex;
        }
        return remoteNetwork;
    }
}
