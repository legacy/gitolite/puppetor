/*
 * Copyright 2007 Karsten Loesing
 * Copyright 2008-2009 Karsten Loesing and Sebastian Hahn
 * See LICENSE for licensing information
 */
package org.torproject.puppetor;

import java.rmi.Remote;
import java.rmi.RemoteException;

/**
 * The <code>ServerApplication</code> can be used as simple HTTP server that
 * answers all <code>HTTP GET</code> requests by empty <code>HTTP OK</code>
 * replies. Therefore, a thread will be started to listen for incoming requests
 * in the background.
 * 
 * @author kloesing
 */
public interface ServerApplication extends Remote {

    /**
     * Starts listening for incoming <code>HTTP GET</code> requests from
     * clients. Any incoming request is answered by an empty
     * <code>HTTP OK</code> reply. This method may only be invoked when the
     * server is currently not in listening state!
     * 
     * @throws IllegalStateException
     *             Thrown if the server is currently not in listening state.
     * @throws RemoteException
     *             Thrown if an error occurs when accessed remotely.
     */
    public abstract void startListening() throws RemoteException;

    /**
     * Stops listening for requests. This method may only be invoked when the
     * server is currently in listening state!
     * 
     * @throws IllegalStateException
     *             Thrown if the server is currently in listening state.
     * @throws RemoteException
     *             Thrown if an error occurs when accessed remotely.
     */
    public abstract void stopListening() throws RemoteException;

    /**
     * Returns whether this server is currently in listening state.
     * 
     * @return The listening state of this server.
     * @throws RemoteException
     *             Thrown if an error occurs when accessed remotely.
     */
    public abstract boolean isListening() throws RemoteException;

    /**
     * Returns the name of this server.
     * 
     * @return The name of this server.
     * @throws RemoteException
     *             Thrown if an error occurs when accessed remotely.
     */
    public abstract String getServerApplicationName() throws RemoteException;

    /**
     * Returns the port on which this server listens.
     * 
     * @return The port on which this server listens.
     * @throws RemoteException
     *             Thrown if an error occurs when accessed remotely.
     */
    public abstract int getServerPort() throws RemoteException;
}
