/*
 * Copyright 2007 Karsten Loesing
 * Copyright 2008-2009 Karsten Loesing and Sebastian Hahn
 * See LICENSE for licensing information
 */
package org.torproject.puppetor;

import java.io.File;
import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.List;
import java.util.Map;

/**
 * A Network instance constitutes the central object of any test run and is
 * aware of the node configuration. It creates all nodes for this configuration
 * and is able to perform common operations on these nodes. Apart from the
 * factory methods, all other operations could also be performed manually by an
 * application using the appropriate interfaces.
 * 
 * @author kloesing
 */
public interface Network extends Remote {

    /**
     * <p>
     * Configures this network as private Tor network by exchanging directory
     * strings and router fingerprints between the nodes of this network.
     * Afterwards, the nodes will be able to run a private Tor network,
     * separated from public directory servers and onion routers.
     * </p>
     * 
     * <p>
     * The configuration is done in two steps:
     * <ol>
     * <li>Directory strings of directory nodes are added to the configurations
     * of all nodes in the other network.</li>
     * <li>Router fingerprints of all router and directory nodes are added to
     * the <code>approved-routers</code> files of the directory nodes.</li>
     * </ol>
     * </p>
     * 
     * <p>
     * This operation may be invoked in any state of the contained nodes.
     * However, a network that does not have directory nodes of its own but
     * relies on directory nodes of a merged network <b>should not be started
     * before being configured as private network!</b> Otherwise it would
     * connect to the public Tor network before being merged with the other
     * private Tor network. However, it may also be invoked at a later time,
     * e.g. to admit new nodes.
     * </p>
     * 
     * <p>
     * This operation does not write any configurations to disk and neither
     * starts a nodes nor sends HUP signals to running nodes. These operations
     * are left to the application, so that they have more control over the
     * network behavior.
     * </p>
     * 
     * <p>
     * Applications need to ensure that there are enough directory nodes (2) and
     * router nodes (3) in the network to allow normal operation.
     * </p>
     * 
     * @throws PuppeTorException
     *             Thrown if an I/O problem occurs while determining the nodes'
     *             fingerprints.
     * @throws RemoteException
     *             Thrown if an error occurs when accessed remotely.
     */
    public abstract void configureAsPrivateNetwork() throws PuppeTorException,
            RemoteException;

    /**
     * <p>
     * Merges this network with another private Tor network by exchanging
     * directory strings and router fingerprints. Afterwards, the nodes in both
     * networks will consider the two networks a single, larger private Tor
     * network.
     * </p>
     * 
     * <p>
     * The configuration is done in two steps:
     * <ol>
     * <li>Directory strings of all directory nodes in this network are added
     * to the configurations of all nodes in the other network and vice versa.</li>
     * <li>Router fingerprints of all router and directory nodes in this
     * network are added to the <code>approved-routers</code> files of all
     * directory nodes in the other network and vice versa.</li>
     * </ol>
     * </p>
     * 
     * <p>
     * This operation may be invoked in any state of the contained nodes.
     * However, a network that does not have directory nodes of its own but
     * relies on directory nodes of a merged network <b>should not be started
     * before merging!</b> Otherwise it would connect to the public Tor network
     * before being merged with the other private Tor network. However, it may
     * also be invoked at a later time, e.g. to admit new nodes.
     * </p>
     * 
     * <p>
     * This operation does not write any configurations to disk and neither
     * starts a nodes nor sends HUP signals to running nodes. These operations
     * are left to the application, so that they have more control over the
     * network behavior.
     * </p>
     * 
     * <p>
     * Note that this operation is only effective if there are directory nodes
     * in either of the two networks. Otherwise, no information will be
     * exchanged between the two networks. Applications need to ensure that
     * there are in total enough directory nodes (2) and router nodes (3) in
     * both networks to allow normal operation.
     * </p>
     * 
     * @param remoteNetwork
     *            The remote network to merge this network with.
     * @throws RemoteException
     *             Thrown if an error occurs when accessed remotely.
     * @throws PuppeTorException
     *             Thrown if an I/O problem occurs while reading directory
     *             strings or router fingerprints, while writing the new
     */
    public abstract void configureAsInterconnectedPrivateNetwork(
            Network remoteNetwork) throws RemoteException, PuppeTorException;

    /**
     * Creates a new client application, but does not yet perform a request.
     * 
     * @param clientApplicationName
     *            The name for this client application, which is used for
     *            logging purposes and as event source. May neither be
     *            <code>null</code> or a zero-length string. The name needs to
     *            be unique in this network.
     * @param targetAddress
     *            The target for requests sent by this client application. Can
     *            be a publicly available URL or an onion address. May neither
     *            be <code>null</code> or a zero-length string.
     * @param targetPort
     *            The TCP port for requests sent by this client application. If
     *            the target address is an onion address, this port is the
     *            virtual port that the hidden service has announced. May not be
     *            negative or greater than 65535.
     * @param socksPort
     *            The TCP port on which a local Tor process is waiting for
     *            incoming SOCKS requests. May not be negative or greater than
     *            65535.
     * @return Reference to the created client application.
     * @throws IllegalArgumentException
     *             Thrown if an invalid value is given for either of the
     *             parameters.
     * @throws RemoteException
     *             Thrown if an error occurs when accessed remotely.
     */
    public abstract ClientApplication createClient(
            String clientApplicationName, String targetAddress, int targetPort,
            int socksPort) throws RemoteException;

    /**
     * Creates a new directory node with automatically assigned ports and adds
     * it to the network, but does not yet write its configuration to disk or
     * start the corresponding Tor process.
     * 
     * @param nodeName
     *            The name for this node, which is used as name for the working
     *            directory, for logging purposes, as node nickname, and as
     *            event source. May neither be <code>null</code> or have zero
     *            or more than 19 alpha-numeric characters. The node name needs
     *            to be unique in this network.
     * @return Reference to the created directory node.
     * @throws IllegalArgumentException
     *             Thrown if an invalid value is given as node name.
     * @throws RemoteException
     *             Thrown if an error occurs when accessed remotely.
     */
    public abstract DirectoryNode createDirectory(String nodeName)
            throws RemoteException;

    /**
     * Creates a new directory node with automatically assigned ports that will
     * listen on the given IP address and adds it to the network, but does not
     * yet write its configuration to disk or start the corresponding Tor
     * process.
     * 
     * @param nodeName
     *            The name for this node, which is used as name for the working
     *            directory, for logging purposes, as node nickname, and as
     *            event source. May neither be <code>null</code> or have zero
     *            or more than 19 alpha-numeric characters. The node name needs
     *            to be unique in this network.
     * @param serverIpAddress
     *            The IP address on which the node will listen. Must be a valid
     *            IP v4 address in dotted decimal notation. May not be
     *            <code>null</code>.
     * @return Reference to the created directory node.
     * @throws IllegalArgumentException
     *             Thrown if an invalid value is given as node name.
     * @throws RemoteException
     *             Thrown if an error occurs when accessed remotely.
     */
    public abstract DirectoryNode createDirectory(String nodeName,
            String serverIpAddress) throws RemoteException;

    /**
     * Creates a new directory node and adds it to the network, but does not yet
     * write its configuration to disk or start the corresponding Tor process.
     * 
     * @param nodeName
     *            The name for this node, which is used as name for the working
     *            directory, for logging purposes, as node nickname, and as
     *            event source. May neither be <code>null</code> or have zero
     *            or more than 19 alpha-numeric characters. The node name needs
     *            to be unique in this network.
     * @param controlPort
     *            The TCP port on which the corresponding Tor process will wait
     *            for a controller. May not be negative or greater than 65535.
     * @param socksPort
     *            The TCP port on which the corresponding Tor process will wait
     *            for incoming SOCKS requests. May not be negative or greater
     *            than 65535.
     * @param orPort
     *            The TCP port on which the corresponding Tor process will wait
     *            for incoming requests from other onion routers. May not be
     *            negative or greater than 65535.
     * @param dirPort
     *            The TCP port on which the corresponding Tor process will wait
     *            for incoming directory requests. May not be negative or
     *            greater than 65535.
     * @return Reference to the created directory node.
     * @throws IllegalArgumentException
     *             Thrown if an invalid value is given for either of the
     *             parameters.
     * @throws RemoteException
     *             Thrown if an error occurs when accessed remotely.
     */
    public abstract DirectoryNode createDirectory(String nodeName,
            int controlPort, int socksPort, int orPort, int dirPort)
            throws RemoteException;

    /**
     * Creates a new directory node that will listen on the given IP address and
     * adds it to the network, but does not yet write its configuration to disk
     * or start the corresponding Tor process.
     * 
     * @param nodeName
     *            The name for this node, which is used as name for the working
     *            directory, for logging purposes, as node nickname, and as
     *            event source. May neither be <code>null</code> or have zero
     *            or more than 19 alpha-numeric characters. The node name needs
     *            to be unique in this network.
     * @param controlPort
     *            The TCP port on which the corresponding Tor process will wait
     *            for a controller. May not be negative or greater than 65535.
     * @param socksPort
     *            The TCP port on which the corresponding Tor process will wait
     *            for incoming SOCKS requests. May not be negative or greater
     *            than 65535.
     * @param orPort
     *            The TCP port on which the corresponding Tor process will wait
     *            for incoming requests from other onion routers. May not be
     *            negative or greater than 65535.
     * @param dirPort
     *            The TCP port on which the corresponding Tor process will wait
     *            for incoming directory requests. May not be negative or
     *            greater than 65535.
     * @param serverIpAddress
     *            The IP address on which the node will listen. Must be a valid
     *            IP v4 address in dotted decimal notation. May not be
     *            <code>null</code>.
     * @return Reference to the created directory node.
     * @throws IllegalArgumentException
     *             Thrown if an invalid value is given for either of the
     *             parameters.
     * @throws RemoteException
     *             Thrown if an error occurs when accessed remotely.
     */
    public abstract DirectoryNode createDirectory(String nodeName,
            int controlPort, int socksPort, int orPort, int dirPort,
            String serverIpAddress) throws RemoteException;

    /**
     * Creates a new <code>ProxyNode</code> with automatically assigned ports
     * and adds it to the network, but does not yet write its configuration to
     * disk or start the corresponding Tor process.
     * 
     * @param nodeName
     *            The name for this node, which is used as name for the working
     *            directory, for logging purposes, and as event source. May
     *            neither be <code>null</code> or have zero or more than 19
     *            alpha-numeric characters. The node name needs to be unique in
     *            this network.
     * @return Reference to the created proxy node.
     * @throws IllegalArgumentException
     *             Thrown if an invalid value is given as node name.
     * @throws RemoteException
     *             Thrown if an error occurs when accessed remotely.
     */
    public abstract ProxyNode createProxy(String nodeName)
            throws RemoteException;

    /**
     * Creates a new <code>ProxyNode</code> and adds it to the network, but
     * does not yet write its configuration to disk or start the corresponding
     * Tor process.
     * 
     * @param nodeName
     *            The name for this node, which is used as name for the working
     *            directory, for logging purposes, and as event source. May
     *            neither be <code>null</code> or have zero or more than 19
     *            alpha-numeric characters. The node name needs to be unique in
     *            this network.
     * @param controlPort
     *            The TCP port on which the corresponding Tor process will wait
     *            for a controller. May not be negative or greater than 65535.
     * @param socksPort
     *            The TCP port on which the corresponding Tor process will wait
     *            for incoming SOCKS requests. May not be negative or greater
     *            than 65535.
     * @return Reference to the created proxy node.
     * @throws IllegalArgumentException
     *             Thrown if an invalid value is given for either of the
     *             parameters.
     * @throws RemoteException
     *             Thrown if an error occurs when accessed remotely.
     */
    public abstract ProxyNode createProxy(String nodeName, int controlPort,
            int socksPort) throws RemoteException;

    /**
     * Creates a new <code>RouterNode</code> with automatically assigned ports
     * and adds it to the network, but does not yet write its configuration to
     * disk or start the corresponding Tor process.
     * 
     * @param nodeName
     *            The name for this node, which is used as name for the working
     *            directory, for logging purposes, as node nickname, and as
     *            event source. May neither be <code>null</code> or have zero
     *            or more than 19 alpha-numeric characters. The node name needs
     *            to be unique in this network.
     * @return Reference to the created router node.
     * @throws IllegalArgumentException
     *             Thrown if an invalid value is given as node name.
     * @throws RemoteException
     *             Thrown if an error occurs when accessed remotely.
     */
    public abstract RouterNode createRouter(String nodeName)
            throws RemoteException;

    /**
     * Creates a new <code>RouterNode</code> and adds it to the network, but
     * does not yet write its configuration to disk or start the corresponding
     * Tor process.
     * 
     * @param nodeName
     *            The name for this node, which is used as name for the working
     *            directory, for logging purposes, as node nickname, and as
     *            event source. May neither be <code>null</code> or have zero
     *            or more than 19 alpha-numeric characters. The node name needs
     *            to be unique in this network.
     * @param controlPort
     *            The TCP port on which the corresponding Tor process will wait
     *            for a controller. May not be negative or greater than 65535.
     * @param socksPort
     *            The TCP port on which the corresponding Tor process will wait
     *            for incoming SOCKS requests. May not be negative or greater
     *            than 65535.
     * @param orPort
     *            The TCP port on which the corresponding Tor process will wait
     *            for incoming requests from other onion routers. May not be
     *            negative or greater than 65535.
     * @param dirPort
     *            The TCP port on which the corresponding Tor process will wait
     *            for incoming directory requests which in fact are requests for
     *            the mirrored directory. May not be negative or greater than
     *            65535.
     * @return Reference to the created router node.
     * @throws IllegalArgumentException
     *             Thrown if an invalid value is given for either of the
     *             parameters.
     * @throws RemoteException
     *             Thrown if an error occurs when accessed remotely.
     */
    public abstract RouterNode createRouter(String nodeName, int controlPort,
            int socksPort, int orPort, int dirPort) throws RemoteException;

    /**
     * Creates a new <code>RouterNode</code> with automatically assigned ports
     * that will listen on the given IP address and adds it to the network, but
     * does not yet write its configuration to disk or start the corresponding
     * Tor process.
     * 
     * @param nodeName
     *            The name for this node, which is used as name for the working
     *            directory, for logging purposes, as node nickname, and as
     *            event source. May neither be <code>null</code> or have zero
     *            or more than 19 alpha-numeric characters. The node name needs
     *            to be unique in this network.
     * @param serverIpAddress
     *            The IP address on which the node will listen. Must be a valid
     *            IP v4 address in dotted decimal notation. May not be
     *            <code>null</code>.
     * @return Reference to the created router node.
     * @throws IllegalArgumentException
     *             Thrown if an invalid value is given as node name.
     * @throws RemoteException
     *             Thrown if an error occurs when accessed remotely.
     */
    public abstract RouterNode createRouter(String nodeName,
            String serverIpAddress) throws RemoteException;

    /**
     * Creates a new <code>RouterNode</code> that will listen on the given IP
     * address and adds it to the network, but does not yet write its
     * configuration to disk or start the corresponding Tor process.
     * 
     * @param nodeName
     *            The name for this node, which is used as name for the working
     *            directory, for logging purposes, as node nickname, and as
     *            event source. May neither be <code>null</code> or have zero
     *            or more than 19 alpha-numeric characters. The node name needs
     *            to be unique in this network.
     * @param controlPort
     *            The TCP port on which the corresponding Tor process will wait
     *            for a controller. May not be negative or greater than 65535.
     * @param socksPort
     *            The TCP port on which the corresponding Tor process will wait
     *            for incoming SOCKS requests. May not be negative or greater
     *            than 65535.
     * @param orPort
     *            The TCP port on which the corresponding Tor process will wait
     *            for incoming requests from other onion routers. May not be
     *            negative or greater than 65535.
     * @param dirPort
     *            The TCP port on which the corresponding Tor process will wait
     *            for incoming directory requests which in fact are requests for
     *            the mirrored directory. May not be negative or greater than
     *            65535.
     * @param serverIpAddress
     *            The IP address on which the node will listen. Must be a valid
     *            IP v4 address in dotted decimal notation. May not be
     *            <code>null</code>.
     * @return Reference to the created router node.
     * @throws IllegalArgumentException
     *             Thrown if an invalid value is given for either of the
     *             parameters.
     * @throws RemoteException
     *             Thrown if an error occurs when accessed remotely.
     */
    public abstract RouterNode createRouter(String nodeName, int controlPort,
            int socksPort, int orPort, int dirPort, String serverIpAddress)
            throws RemoteException;

    /**
     * Creates a new <code>ServerApplication</code> with automatically
     * assigned ports, but does not start listening for incoming requests.
     * 
     * @param serverApplicationName
     *            The name for this server application, which is used for
     *            logging purposes and as event source. May neither be
     *            <code>null</code> or a zero-length string. The name needs to
     *            be unique in this network.
     * @return Reference to the created server application.
     * @throws IllegalArgumentException
     *             Thrown if an invalid value is given as server application
     *             name.
     * @throws RemoteException
     *             Thrown if an error occurs when accessed remotely.
     */
    public abstract ServerApplication createServer(String serverApplicationName)
            throws RemoteException;

    /**
     * Creates a new <code>ServerApplication</code>, but does not start
     * listening for incoming requests.
     * 
     * @param serverApplicationName
     *            The name for this server application, which is used for
     *            logging purposes and as event source. May neither be
     *            <code>null</code> or a zero-length string. The name needs to
     *            be unique in this network.
     * @param serverPort
     *            The TCP port on which the server will wait for incoming
     *            requests. May not be negative or greater than 65535.
     * @return Reference to the created server application.
     * @throws IllegalArgumentException
     *             Thrown if an invalid value is given for either of the
     *             parameters.
     * @throws RemoteException
     *             Thrown if an error occurs when accessed remotely.
     */
    public abstract ServerApplication createServer(
            String serverApplicationName, int serverPort)
            throws RemoteException;

    /**
     * Returns a reference on the (single) event manager for this network.
     * 
     * @return Reference on the (single) event manager for this network.
     * @throws RemoteException
     *             Thrown if an error occurs when accessed remotely.
     */
    public abstract EventManager getEventManager() throws RemoteException;

    /**
     * Returns (a copy of) the map containing the names of all directory nodes
     * as keys and the corresponding directory nodes as values.
     * 
     * @return Map containing all directory nodes.
     * @throws RemoteException
     *             Thrown if an error occurs when accessed remotely.
     */
    public abstract Map<String, DirectoryNode> getAllDirectoryNodes()
            throws RemoteException;

    /**
     * Returns (a copy of) the map containing the names of all router nodes
     * (only those that are not acting as directory nodes at the same time) as
     * keys and the corresponding router nodes as values.
     * 
     * @return Map containing all router nodes.
     * @throws RemoteException
     *             Thrown if an error occurs when accessed remotely.
     */
    public abstract Map<String, RouterNode> getAllRouterNodes()
            throws RemoteException;

    /**
     * Returns (a copy of) the map containing the names of all proxy nodes (only
     * those that are not acting as router or directory nodes at the same time)
     * as keys and the corresponding proxy nodes as values.
     * 
     * @return Map containing all proxy nodes.
     * @throws RemoteException
     *             Thrown if an error occurs when accessed remotely.
     */
    public abstract Map<String, ProxyNode> getAllProxyNodes()
            throws RemoteException;

    /**
     * Returns (a copy of) the map containing the names of all nodes as keys and
     * the corresponding proxy nodes as values.
     * 
     * @return Map containing all nodes.
     * @throws RemoteException
     *             Thrown if an error occurs when accessed remotely.
     */
    public abstract Map<String, ProxyNode> getAllNodes() throws RemoteException;

    /**
     * Returns the node with name <code>nodeName</code> or <code>null</code>
     * if no such node exists.
     * 
     * @param nodeName
     *            The node name to look up.
     * @return The node with name <code>nodeName</code>.
     * @throws RemoteException
     *             Thrown if an error occurs when accessed remotely.
     */
    public abstract ProxyNode getNode(String nodeName) throws RemoteException;

    /**
     * <p>
     * Sends a HUP signal to all nodes in the network in regular intervals and
     * blocks the invoking thread until all nodes have reported to have
     * successfully opened a circuit.
     * </p>
     * 
     * <p>
     * First, the method waits for <code>hupInterval</code> milliseconds for
     * the nodes to have successfully opened a circuit. If they do not succeed
     * within this time, a HUP signal is sent to all nodes and the method waits
     * for another <code>hupInterval</code> milliseconds. In total, the method
     * sends at most <code>tries</code> HUP signals before giving up and
     * returning with <code>false</code>. Thus, the maximum waiting time is
     * <code>(tries + 1)</code> times <code>hupInterval</code>. As soon as
     * all nodes have successfully opened circuits, the method returns with
     * <code>true</code>. This operation can only be invoked, if all nodes in
     * the network are in state <code>NodeState.RUNNING</code>.
     * </p>
     * 
     * @param tries
     *            The maximum number of HUP signals that are sent to the Tor
     *            processes. Negative values are not allowed. A value of zero
     *            means to wait only for the given time of
     *            <code>hupInterval</code> milliseconds without sending a HUP
     *            signal. Typical values depend on the network being a public or
     *            private Tor network and range about 3 to 5 tries.
     * @param hupInterval
     *            The time in milliseconds that the method will wait between
     *            sending HUP signals. Negative values are not allowed.
     *            Typically, values should not be smaller than 5 seconds to
     *            permit Tor to stabilize.
     * @throws IllegalStateException
     *             Thrown if at least one node is not in state
     *             <code>NodeState.RUNNING</code>.
     * @throws IllegalArgumentException
     *             Thrown if a negative value is given for either
     *             <code>tries</code> or <code>hupInterval</code>.
     * @throws PuppeTorException
     *             Thrown if an I/O problem occurs while sending HUP signals.
     * @return <code>true</code> if all nodes have reported to have
     *         successfully opened a circuit, <code>false</code> otherwise.
     * @throws RemoteException
     *             Thrown if an error occurs when accessed remotely.
     */
    public abstract boolean hupUntilUp(int tries, long hupInterval)
            throws PuppeTorException, RemoteException;

    /**
     * Sends a HUP signal to all nodes in the network once. This operation can
     * only be invoked, if all nodes in the network are in state
     * <code>NodeState.RUNNING</code>.
     * 
     * @throws IllegalStateException
     *             Thrown if at least one node is not in state
     *             <code>NodeState.RUNNING</code>.
     * @throws PuppeTorException
     *             Thrown if an I/O problem occurs while sending HUP signals.
     * @throws RemoteException
     *             Thrown if an error occurs when accessed remotely.
     */
    public abstract void hupAllNodes() throws PuppeTorException,
            RemoteException;

    /**
     * Sends a HUP signal to all directory nodes in the network once. This
     * operation can only be invoked, if all directory nodes in the network are
     * in state <code>NodeState.RUNNING</code>.
     * 
     * @throws IllegalStateException
     *             Thrown if at least one directory node is not in state
     *             <code>NodeState.RUNNING</code>.
     * @throws PuppeTorException
     *             Thrown if an I/O problem occurs while sending HUP signals.
     * @throws RemoteException
     *             Thrown if an error occurs when accessed remotely.
     */
    public abstract void hupAllDirectories() throws PuppeTorException,
            RemoteException;

    /**
     * Attempts to shut down all running nodes. The method blocks until all
     * shutdown requests have been sent and either returns, or throws the first
     * exception that has been observed when shutting down nodes. The method can
     * be assumed to return very quickly. If there are no running nodes in this
     * network, this operation has no effect.
     * 
     * @throws PuppeTorException
     *             Thrown if an I/O problem occurs while shutting down the
     *             nodes.
     * @throws RemoteException
     *             Thrown if an error occurs when accessed remotely.
     */
    public abstract void shutdownNodes() throws PuppeTorException,
            RemoteException;

    /**
     * Attempts to start all nodes within a given timeout of
     * <code>maximumTimeToWaitInMillis</code> milliseconds. The method returns
     * as soon as all nodes have started and opened their control port so that
     * we can connect to them. It returns a boolean that states whether the
     * operation was either successful or has timed out. This operation can only
     * be invoked, if all nodes in the network have written their configuration,
     * i.e. are not in state <code>NodeState.CONFIGURING</code> anymore.
     * 
     * @param maximumTimeToWaitInMillis
     *            The maximum time to wait in milliseconds. A positive value or
     *            zero restricts waiting to this time. Negative values are not
     *            allowed. Typical values are in the range of a few seconds.
     * @return <code>true</code> if all nodes could be started successfully,
     *         <code>false</code> if a timeout has occured.
     * @throws IllegalStateException
     *             Thrown if at least one node in the network is still in state
     *             <code>NodeState.CONFIGURING</code>.
     * @throws IllegalArgumentException
     *             Thrown if a negative value is given for
     *             <code>maximumTimeToWaitInMillis</code>.
     * @throws PuppeTorException
     *             Thrown if an I/O problem occurs while starting the nodes.
     * @throws RemoteException
     *             Thrown if an error occurs when accessed remotely.
     */
    public abstract boolean startNodes(long maximumTimeToWaitInMillis)
            throws PuppeTorException, RemoteException;

    /**
     * Writes the configurations for all nodes in the network to disk, including
     * <code>torrc</code> and <code>approved-routers</code> files. This
     * method is assumed to return very quickly. In case of a private network,
     * <code>configureAsPrivateNetwork</code> should be invoked in advance to
     * this method!
     * 
     * @throws PuppeTorException
     *             Thrown if an I/O problem occurs while writing to the nodes'
     *             working directories.
     * @throws RemoteException
     *             Thrown if an error occurs when accessed remotely.
     */
    public abstract void writeConfigurations() throws PuppeTorException,
            RemoteException;

    /**
     * Returns the working directory of this network configuration which is in
     * <code>test-env/networkName/</code>.
     * 
     * @return Working directory of this network.
     * @throws RemoteException
     *             Thrown if an error occurs when accessed remotely.
     */
    public abstract File getWorkingDirectory() throws RemoteException;

    /**
     * Returns all configuration strings of the template of a node class that
     * will be added to future instances of this node class and its subclasses.
     * Note that the result only contains those configuration strings that are
     * added by this node class to possible superclasses and that parameterized
     * configuration strings, e.g. port configurations, are not included.
     * 
     * @param nodeClass
     *            The class which will be configured with the returned template
     *            configuration; may not be <code>null</code>.
     * @return The template configuration for the given node class.
     * @throws IllegalArgumentException
     *             Thrown if an invalid value is given for the parameter.
     * @throws RemoteException
     *             Thrown if an error occurs when accessed remotely.
     */
    public abstract List<String> getTemplateConfiguration(
            Class<? extends ProxyNode> nodeClass) throws RemoteException;

    /**
     * Adds a configuration string to the template of a node class, so that it
     * will be added to future instances of this node class and its subclasses.
     * 
     * @param nodeClass
     *            The class of nodes of which future instances will have the
     *            given configuration string; may not be <code>null</code>.
     * @param templateConfigurationString
     *            The configuration string to add; may neither be
     *            <code>null</code> nor a zero-length string, and must consist
     *            of configuration key and value.
     * @throws IllegalArgumentException
     *             Thrown if an invalid value is given for either of the
     *             parameters.
     * @throws RemoteException
     *             Thrown if an error occurs when accessed remotely.
     */
    public abstract void addTemplateConfiguration(
            Class<? extends ProxyNode> nodeClass,
            String templateConfigurationString) throws RemoteException;

    /**
     * Removes a configuration string from the template of a node class, so that
     * it will not be added to future instances of this node class and its
     * subclasses.
     * 
     * @param nodeClass
     *            The class of nodes of which future instances will have the
     *            given configuration string; may not be <code>null</code>.
     * @param templateConfigurationKey
     *            The configuration key to remove; may neither be
     *            <code>null</code> nor a zero-length key.
     * @throws IllegalArgumentException
     *             Thrown if an invalid value is given for either of the
     *             parameters.
     * @throws RemoteException
     *             Thrown if an error occurs when accessed remotely.
     */
    public abstract void removeTemplateConfiguration(
            Class<? extends ProxyNode> nodeClass,
            String templateConfigurationKey) throws RemoteException;

    /**
     * Returns the name of this network.
     * 
     * @return The name of this network.
     * @throws RemoteException
     *             Thrown if an error occurs when accessed remotely.
     */
    public abstract String getNetworkName() throws RemoteException;

    /**
     * Binds the network at the local <code>rmiregistry</code> to make it
     * remotely available and returns whether binding was successful.
     * 
     * @return <code>true</code> if binding was successful, <code>false</code>
     *         otherwise.
     * @throws PuppeTorException
     *             Thrown if an error occurs while binding to the
     *             <code>rmiregistry</code>.
     * @throws RemoteException
     *             Thrown if an error occurs when accessed remotely (though this
     *             might never happen as the object is not bound, yet).
     */
    public abstract boolean bindAtRmiregistry() throws RemoteException;

}
