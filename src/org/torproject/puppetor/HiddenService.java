/*
 * Copyright 2007 Karsten Loesing
 * Copyright 2008-2009 Karsten Loesing and Sebastian Hahn
 * See LICENSE for licensing information
 */
package org.torproject.puppetor;

import java.rmi.Remote;
import java.rmi.RemoteException;

/**
 * A <code>HiddenService</code> instance contains all configurations of a
 * hidden service that is registered at a node.
 * 
 * @author kloesing
 */
public interface HiddenService extends Remote {

    /**
     * Determines the onion address for a previously added hidden service with
     * name <code>serviceName</code>. Requires that the node has been
     * started, i.e. is in state <code>NodeState.RUNNING</code>, and is
     * configured to provide this hidden service.
     * 
     * @return The onion address string consisting of 16 base32 chars plus
     *         ".onion" for hidden service versions 0 and 1 or 16 base32 chars
     *         plus "." plus 24 base32 chars plus ".onion" for hidden service
     *         version 2.
     * @throws IllegalArgumentException
     *             Thrown if <code>null</code> or a zero-length string is
     *             passed as parameter.
     * @throws IllegalStateException
     *             Thrown if the node at which this hidden service is configured
     *             is not in state <code>NodeState.RUNNING</code>.
     * @throws PuppeTorException
     *             Thrown if the onion address of this hidden service could not
     *             be read, which is also the case when the node's configuration
     *             has not been written and the node has not been HUP'ed after
     *             configuring the hidden service.
     * @throws RemoteException
     *             Thrown if an error occurs when accessed remotely.
     */
    public abstract String determineOnionAddress() throws PuppeTorException,
            RemoteException;

    /**
     * Returns the name of the hidden service.
     * 
     * @return The name of the hidden service.
     * @throws RemoteException
     *             Thrown if an error occurs when accessed remotely.
     */
    public String getServiceName() throws RemoteException;

    /**
     * Returns the port on which the service listens for requests.
     * 
     * @return The port on which the service listens for requests.
     * @throws RemoteException
     *             Thrown if an error occurs when accessed remotely.
     */
    public int getServicePort() throws RemoteException;

    /**
     * Returns the virtual port that this hidden service runs on as it is
     * announced to clients.
     * 
     * @return The virtual port of this hidden service.
     * @throws RemoteException
     *             Thrown if an error occurs when accessed remotely.
     */
    public int getVirtualPort() throws RemoteException;
}
