/*
 * Copyright 2007 Karsten Loesing
 * Copyright 2008-2009 Karsten Loesing and Sebastian Hahn
 * See LICENSE for licensing information
 */
package org.torproject.puppetor;

/**
 * The <code>PuppeTorException</code> comprises all kinds of checked
 * exceptions that occur when interacting with the JVM-external Tor processes or
 * with the local file system. Any occurence of this exception denotes either a
 * configuration problem that can only be solved outside of the JVM, or an
 * unexpected problem. In contrast to this, all kinds of programming errors of
 * an application using this API (invoking a method with wrong parameter values,
 * in wrong state, etc.) will instead cause appropriate runtime exceptions from
 * the Java API.
 * 
 * @author kloesing
 */
@SuppressWarnings("serial")
public class PuppeTorException extends Exception {

    /**
     * Creates a <code>PuppeTorException</code> without detail message or
     * cause.
     */
    public PuppeTorException() {
        super();
    }

    /**
     * Creates a <code>PuppeTorException</code> with the given detail
     * <code>message</code> and <code>cause</code>.
     * 
     * @param message
     *            The detail message of this exception.
     * @param cause
     *            The cause for this exception.
     */
    public PuppeTorException(String message, Throwable cause) {
        super(message, cause);
    }

    /**
     * Creates a <code>PuppeTorException</code> with the given detail
     * <code>message</code>, but without a <code>cause</code>.
     * 
     * @param message
     *            The detail message of this exception.
     */
    public PuppeTorException(String message) {
        super(message);
    }

    /**
     * Creates a <code>PuppeTorException</code> with the given
     * <code>cause</code>, but without a detail message.
     * 
     * @param cause
     *            The cause for this exception.
     */
    public PuppeTorException(Throwable cause) {
        super(cause);
    }
}
