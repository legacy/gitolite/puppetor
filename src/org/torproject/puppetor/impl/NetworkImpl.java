/*
 * Copyright 2007 Karsten Loesing
 * Copyright 2008-2009 Karsten Loesing and Sebastian Hahn
 * See LICENSE for licensing information
 */
package org.torproject.puppetor.impl;

import java.io.File;
import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.RMISecurityManager;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.torproject.puppetor.ClientApplication;
import org.torproject.puppetor.DirectoryNode;
import org.torproject.puppetor.Event;
import org.torproject.puppetor.EventListener;
import org.torproject.puppetor.EventManager;
import org.torproject.puppetor.Network;
import org.torproject.puppetor.NodeEventType;
import org.torproject.puppetor.NodeState;
import org.torproject.puppetor.ProxyNode;
import org.torproject.puppetor.PuppeTorException;
import org.torproject.puppetor.RouterNode;
import org.torproject.puppetor.ServerApplication;


/**
 * Implementation of <code>Network</code>.
 * 
 * @author kloesing
 */
@SuppressWarnings("serial")
public class NetworkImpl extends UnicastRemoteObject implements Network {

    /**
     * Internal thread class that is used to start Tor processes in parallel.
     */
    private class NodeStarter extends Thread {

        /**
         * The exception, if one is caught while trying to start the node.
         */
        Exception caughtException;

        /**
         * The maximum time to wait for the Tor process to start in
         * milliseconds.
         */
        private long maximumTimeToWaitInMillis;

        /**
         * The node for which the Tor process shall be started.
         */
        private ProxyNode node;

        /**
         * Flag that denotes whether starting the Tor process was successful.
         */
        boolean success = false;

        /**
         * Creates a new <code>NodeStarter</code> for node <code>node</code>
         * that will wait for <code>maximumTimeToWaitInMillis</code>
         * milliseconds to start a Tor process, but that is not started, yet.
         * 
         * @param node
         *            The node for which the Tor process shall be started.
         * @param maximumTimeToWaitInMillis
         *            The maximum time to wait for the Tor process to start in
         *            milliseconds.
         */
        NodeStarter(ProxyNode node, long maximumTimeToWaitInMillis) {

            // log entering
            logger.entering(this.getClass().getName(), "NodeStarter",
                    new Object[] { node, maximumTimeToWaitInMillis });

            // store parameters
            this.node = node;
            this.maximumTimeToWaitInMillis = maximumTimeToWaitInMillis;

            // log exiting
            logger.exiting(this.getClass().getName(), "NodeStarter");
        }

        @Override
        public void run() {

            // log entering
            logger.entering(this.getClass().getName(), "run");

            try {
                // try to start node
                this.success = this.node
                        .startNode(this.maximumTimeToWaitInMillis);
            } catch (PuppeTorException e) {
                logger.log(Level.SEVERE,
                        "Caught an exception while starting node "
                                + node.toString() + "!");
                this.caughtException = e;
            } catch (RemoteException e) {
                logger.log(Level.SEVERE,
                        "Caught a remote exception while starting node "
                                + node.toString() + "!");
                this.caughtException = e;
            }

            // log exiting
            logger.exiting(this.getClass().getName(), "run");
        }
    }

    /**
     * Event manager to which all events concerning this network are notified.
     */
    private EventManagerImpl eventManager;

    /**
     * Logger for this network which is called "network." plus the name of this
     * network.
     */
    private Logger logger;

    /**
     * Contains the name of this network configuration which is the String
     * conversion of System.currentTimeMillis() of the network creation time.
     */
    private String networkName;

    /**
     * All nodes contained in this network.
     */
    private Map<String, ProxyNode> nodes;

    /**
     * Directory that contains status information of all nodes contained in this
     * network.
     */
    private File workingDir;

    /**
     * The counter for automatically assigned port numbers created by this
     * <code>Network</code>.
     */
    private int portCounter = 7000;

    /**
     * Creates an initially unpopulated Tor network and creates a new working
     * directory for it at test-env/randomTestID/.
     * 
     * @param networkName
     *            Name of this network configuration. May neither be
     *            <code>null</code> or a zero-length string.
     * @param startPort
     *            The initial value for automatically assigned port numbers of
     *            nodes created by this <code>Network</code>; must be a value
     *            between 1024 and 65535.
     * @throws IllegalArgumentException
     *             Thrown if the given <code>networkName</code> is either
     *             <code>null</code> or a zero-length string, or if an illegal
     *             number is given for <code>startPort</code>.
     * @throws RemoteException
     *             Thrown if an error occurs when accessed remotely.
     */
    public NetworkImpl(String networkName, int startPort)
            throws RemoteException {

        // initialize using overloaded constructor
        this(networkName);

        // check if start port is valid
        if (startPort < 1024 || startPort > 65535) {
            throw new IllegalArgumentException("Invalid startPort: "
                    + startPort);
        }

        // remember parameter
        this.portCounter = startPort;

        // log exiting
        this.logger.exiting(this.getClass().getName(), "NetworkImpl");
    }

    /**
     * Creates an initially unpopulated Tor network and creates a new working
     * directory for it at test-env/randomTestID/.
     * 
     * @param networkName
     *            Name of this network configuration. May neither be
     *            <code>null</code> or a zero-length string.
     * @throws IllegalArgumentException
     *             Thrown if the given <code>networkName</code> is either
     *             <code>null</code> or a zero-length string.
     * @throws RemoteException
     *             Thrown if an error occurs when accessed remotely.
     */
    public NetworkImpl(String networkName) throws RemoteException {

        // check if networkName can be used as logger name
        if (networkName == null || networkName.length() == 0) {
            throw new IllegalArgumentException("Invalid networkName: "
                    + networkName);
        }

        // create logger
        this.logger = Logger.getLogger("network." + networkName);

        // log entering
        this.logger.entering(this.getClass().getName(), "NetworkImpl",
                networkName);

        // TODO is this necessary?!
        this.logger.setLevel(Level.ALL);

        // remember parameter
        this.networkName = networkName;

        // create working directory
        this.workingDir = new File("test-env/" + System.currentTimeMillis());
        this.workingDir.mkdirs();
        this.logger.log(Level.FINE, "Created working directory \""
                + this.workingDir.getAbsolutePath() + "\"");

        // TODO if we want to log to file, set this... somehow...
        // this.logFile = new File(this.workingDir.getAbsolutePath()
        // + "\\events.log");

        // initialize data structures
        this.nodes = new HashMap<String, ProxyNode>();

        // create event manager
        this.eventManager = new EventManagerImpl(this.networkName);

        // log exiting
        this.logger.exiting(this.getClass().getName(), "NetworkImpl");
    }

    /**
     * Returns whether all nodes in this network are up, or not.
     * 
     * @return <code>true</code> if all nodes are up, <code>false</code> if
     *         at least one node is not up.
     * @throws RemoteException
     *             Thrown if an error occurs when accessed remotely.
     */
    private boolean allNodesUp() throws RemoteException {

        // log entering
        this.logger.entering(this.getClass().getName(), "allNodesUp");

        // fail on first node that is not up
        for (ProxyNode node : this.nodes.values()) {
            if (!eventManager.hasEventOccured(node.getNodeName(),
                    NodeEventType.NODE_CIRCUIT_OPENED)) {

                // log exiting and return false
                this.logger.exiting(this.getClass().getName(), "allNodesUp");
                return false;
            }
        }

        // log exiting and return true
        this.logger.exiting(this.getClass().getName(), "allNodesUp");
        return true;
    }

    public void configureAsPrivateNetwork() throws PuppeTorException,
            RemoteException {

        // log entering
        this.logger.entering(this.getClass().getName(),
                "configureAsPrivateNetwork");

        for (ProxyNode node : this.nodes.values()) {
            if (node.getNodeState() == NodeState.CONFIGURING) {
                // add to configuration
                node.addConfiguration("TestingTorNetwork 1");
            }
        }

        // read DirServer strings for all directories
        List<String> authorizedDirectoriesFingerprints = new ArrayList<String>();
        for (ProxyNode node : this.nodes.values()) {
            if (node instanceof DirectoryNode) {
                DirectoryNode dirNode = (DirectoryNode) node;
                authorizedDirectoriesFingerprints.add(dirNode
                        .getDirServerString());
            }
        }

        // configure nodes
        for (ProxyNode node : this.nodes.values()) {
            if (node.getNodeState() == NodeState.CONFIGURING) {

                // add to configuration
                node.addConfigurations(authorizedDirectoriesFingerprints);
            }
        }

        // read fingerprints for all directories and routers
        HashSet<String> approvedRoutersFingerprints = new HashSet<String>();
        for (ProxyNode node : this.nodes.values()) {
            if (node instanceof RouterNode) {
                RouterNode routerOrDirNode = (RouterNode) node;
                approvedRoutersFingerprints.add(routerOrDirNode
                        .getFingerprint());
            }
        }

        // write fingerprints for all directories and routers to the
        // approved-routers file
        for (ProxyNode node : this.nodes.values()) {
            if (node instanceof DirectoryNode) {
                DirectoryNode dirNode = (DirectoryNode) node;
                dirNode.addApprovedRouters(approvedRoutersFingerprints);
            }
        }

        // log exiting
        this.logger.exiting(this.getClass().getName(),
                "configureAsPrivateNetwork");
    }

    public ClientApplication createClient(String clientApplicationName,
            String targetAddress, int targetPort, int socksPort)
            throws RemoteException {

        // log entering
        this.logger.entering(this.getClass().getName(), "createClient",
                new Object[] { clientApplicationName, targetAddress,
                        targetPort, socksPort });

        // create client; parameter checking is done in constructor
        ClientApplicationImpl client = new ClientApplicationImpl(this,
                clientApplicationName, targetAddress, targetPort, socksPort);

        // add name to event manager as event source
        this.eventManager.addEventSource(clientApplicationName);

        // log exiting and return client
        this.logger.exiting(this.getClass().getName(), "createClient", client);
        return client;
    }

    public DirectoryNode createDirectory(String nodeName, int controlPort,
            int socksPort, int orPort, int dirPort, String serverIpAddress)
            throws RemoteException {

        // log entering
        this.logger.entering(this.getClass().getName(), "createDirectory",
                new Object[] { nodeName, controlPort, socksPort, orPort,
                        dirPort, serverIpAddress });

        // create directory node; parameter checking is done in constructor
        DirectoryNode dir = new DirectoryNodeImpl(this, nodeName, controlPort,
                socksPort, orPort, dirPort, serverIpAddress);

        // add new directory node to nodes collection
        this.nodes.put(nodeName, dir);

        // add name to event manager as event source
        this.eventManager.addEventSource(nodeName);

        // log exiting and return directory node
        this.logger.exiting(this.getClass().getName(), "createDirectory", dir);
        return dir;
    }

    public DirectoryNode createDirectory(String nodeName, int controlPort,
            int socksPort, int orPort, int dirPort) throws RemoteException {

        // log entering
        this.logger.entering(this.getClass().getName(), "createDirectory",
                new Object[] { nodeName, controlPort, socksPort, orPort,
                        dirPort });

        // invoke overloaded method
        DirectoryNode dir = this.createDirectory(nodeName, controlPort,
                socksPort, orPort, dirPort, "127.0.0.1");

        // log exiting and return directory node
        this.logger.exiting(this.getClass().getName(), "createDirectory", dir);
        return dir;
    }

    public DirectoryNode createDirectory(String nodeName, String serverIpAddress)
            throws RemoteException {

        // log entering
        this.logger.entering(this.getClass().getName(), "createDirectory",
                new Object[] { nodeName, serverIpAddress });

        // invoke overloaded method
        DirectoryNode dir = this.createDirectory(nodeName, this.portCounter++,
                this.portCounter++, this.portCounter++, this.portCounter++,
                serverIpAddress);

        // log exiting and return directory node
        this.logger.exiting(this.getClass().getName(), "createDirectory", dir);
        return dir;
    }

    public DirectoryNode createDirectory(String nodeName)
            throws RemoteException {

        // log entering
        this.logger.entering(this.getClass().getName(), "createDirectory",
                nodeName);

        // invoke overloaded method
        DirectoryNode dir = this.createDirectory(nodeName, this.portCounter++,
                this.portCounter++, this.portCounter++, this.portCounter++,
                "127.0.0.1");

        // log exiting and return directory node
        this.logger.exiting(this.getClass().getName(), "createDirectory", dir);
        return dir;
    }

    public ProxyNode createProxy(String nodeName, int controlPort, int socksPort)
            throws RemoteException {

        // log entering
        this.logger.entering(this.getClass().getName(), "createProxy",
                new Object[] { nodeName, controlPort, socksPort });

        // create proxy node; parameter checking is done in constructor
        ProxyNode proxy = new ProxyNodeImpl(this, nodeName, controlPort,
                socksPort);

        // add new proxy node to nodes collection
        this.nodes.put(nodeName, proxy);

        // add name to event manager as event source
        this.eventManager.addEventSource(nodeName);

        // log exiting and return proxy node
        this.logger.exiting(this.getClass().getName(), "createProxy", proxy);
        return proxy;
    }

    public ProxyNode createProxy(String nodeName) throws RemoteException {

        // log entering
        this.logger
                .entering(this.getClass().getName(), "createProxy", nodeName);

        // invoke overloaded method
        ProxyNode proxy = this.createProxy(nodeName, this.portCounter++,
                this.portCounter++);

        // log exiting and return proxy node
        this.logger.exiting(this.getClass().getName(), "createProxy", proxy);
        return proxy;
    }

    public RouterNode createRouter(String nodeName, int controlPort,
            int socksPort, int orPort, int dirPort, String serverIpAddress)
            throws RemoteException {

        // log entering
        this.logger.entering(this.getClass().getName(), "createRouter",
                new Object[] { nodeName, controlPort, socksPort, orPort,
                        dirPort, serverIpAddress });

        // create router node; parameter checking is done in constructor
        RouterNode router = new RouterNodeImpl(this, nodeName, controlPort,
                socksPort, orPort, dirPort, serverIpAddress);

        // add new router node to nodes collection
        this.nodes.put(nodeName, router);

        // add name to event manager as event source
        this.eventManager.addEventSource(nodeName);

        // log exiting and return router node
        this.logger.exiting(this.getClass().getName(), "createRouter", router);
        return router;
    }

    public RouterNode createRouter(String nodeName, int controlPort,
            int socksPort, int orPort, int dirPort) throws RemoteException {

        // log entering
        this.logger.entering(this.getClass().getName(), "createRouter",
                new Object[] { nodeName, controlPort, socksPort, orPort,
                        dirPort });

        // invoke overloaded method
        DirectoryNode dir = this.createDirectory(nodeName, controlPort,
                socksPort, orPort, dirPort, "127.0.0.1");

        // log exiting and return directory node
        this.logger.exiting(this.getClass().getName(), "createRouter", dir);
        return dir;
    }

    public RouterNode createRouter(String nodeName, String serverIpAddress)
            throws RemoteException {

        // log entering
        this.logger.entering(this.getClass().getName(), "createRouter",
                new Object[] { nodeName, serverIpAddress });

        // invoke overloaded method
        RouterNode dir = this.createRouter(nodeName, this.portCounter++,
                this.portCounter++, this.portCounter++, this.portCounter++,
                serverIpAddress);

        // log exiting and return directory node
        this.logger.exiting(this.getClass().getName(), "createRouter", dir);
        return dir;
    }

    public RouterNode createRouter(String nodeName) throws RemoteException {

        // log entering
        this.logger.entering(this.getClass().getName(), "createRouter",
                nodeName);

        // invoke overloaded method
        RouterNode router = this.createRouter(nodeName, this.portCounter++,
                this.portCounter++, this.portCounter++, this.portCounter++,
                "127.0.0.1");

        // log exiting and return router node
        this.logger.exiting(this.getClass().getName(), "createRouter", router);
        return router;
    }

    public ServerApplication createServer(String serverApplicationName,
            int serverPort) throws RemoteException {

        // log entering
        this.logger.entering(this.getClass().getName(), "createServer",
                new Object[] { serverApplicationName, serverPort });

        // create server; parameter checking is done in constructor
        ServerApplicationImpl server = new ServerApplicationImpl(this,
                serverApplicationName, serverPort);

        // add name to event manager as event source
        this.eventManager.addEventSource(serverApplicationName);

        // log exiting and return server
        this.logger.exiting(this.getClass().getName(), "createServer", server);
        return server;
    }

    public ServerApplication createServer(String serverApplicationName)
            throws RemoteException {

        // log entering
        this.logger.entering(this.getClass().getName(), "createServer",
                serverApplicationName);

        // invoke overloaded method
        ServerApplication server = this.createServer(serverApplicationName,
                this.portCounter++);

        // log exiting and return server
        this.logger.exiting(this.getClass().getName(), "createServer", server);
        return server;
    }

    public EventManager getEventManager() {
        return this.eventManager;
    }

    /**
     * Returns the implementation instance of the event manager of this network.
     * 
     * @return The implementation instance of the event manager of this network.
     */
    EventManagerImpl getEventManagerImpl() {
        return this.eventManager;
    }

    public File getWorkingDirectory() {
        return this.workingDir;
    }

    public ProxyNode getNode(String nodeName) {
        return this.nodes.get(nodeName);
    }

    public Map<String, ProxyNode> getAllProxyNodes() {
        Map<String, ProxyNode> result = new HashMap<String, ProxyNode>();
        for (String nodeName : this.nodes.keySet()) {
            ProxyNode node = this.nodes.get(nodeName);
            if (!(node instanceof RouterNode)) {
                result.put(nodeName, node);
            }
        }
        return result;
    }

    public Map<String, RouterNode> getAllRouterNodes() {
        Map<String, RouterNode> result = new HashMap<String, RouterNode>();
        for (String nodeName : this.nodes.keySet()) {
            ProxyNode node = this.nodes.get(nodeName);
            if (node instanceof RouterNode && !(node instanceof DirectoryNode)) {
                result.put(nodeName, (RouterNode) node);
            }
        }
        return result;
    }

    public Map<String, DirectoryNode> getAllDirectoryNodes() {
        Map<String, DirectoryNode> result = new HashMap<String, DirectoryNode>();
        for (String nodeName : this.nodes.keySet()) {
            ProxyNode node = this.nodes.get(nodeName);
            if (node instanceof DirectoryNode) {
                result.put(nodeName, (DirectoryNode) node);
            }
        }
        return result;
    }

    public Map<String, ProxyNode> getAllNodes() {
        return new HashMap<String, ProxyNode>(nodes);
    }

    public boolean hupUntilUp(int tries, long hupInterval)
            throws PuppeTorException, RemoteException {

        // log entering
        this.logger.entering(this.getClass().getName(), "hupUntilUp",
                new Object[] { tries, hupInterval });

        // check if all nodes are running
        for (ProxyNode node : this.nodes.values()) {
            if (node.getNodeState() != NodeState.RUNNING) {
                IllegalStateException e = new IllegalStateException(
                        "All nodes must be running before sending them HUP "
                                + "commands!");
                this.logger
                        .throwing(this.getClass().getName(), "hupUntilUp", e);
                throw e;
            }
        }

        // check if nodes are already up; if so, return immediately
        if (allNodesUp()) {

            // log exiting and return true
            this.logger.exiting(this.getClass().getName(), "hupUntilUp", true);
            return true;
        }

        // create and register a new event handler for each node
        final Thread sleepingThread = Thread.currentThread();
        for (ProxyNode node : this.nodes.values()) {
            eventManager.addEventListener(node.getNodeName(),
                    new EventListener() {
                        public void handleEvent(Event event) {
                            if (event.getType() == NodeEventType.NODE_CIRCUIT_OPENED) {
                                sleepingThread.interrupt();
                                eventManager.removeEventListener(this);
                            }
                        }
                    });
        }

        // walk through wait-check-hup loop until there are no tries left
        for (int i = 0; i < tries; i++) {

            // determine how long to sleep
            long endOfSleeping = System.currentTimeMillis() + hupInterval;
            long now;

            // unless all nodes have reported to be up, wait for the given
            // maximum time
            while ((now = System.currentTimeMillis()) < endOfSleeping) {

                // sleep
                try {
                    Thread.sleep(endOfSleeping - now);
                } catch (InterruptedException e) {
                    // do nothing about it
                }

                // check if nodes are up now
                if (allNodesUp()) {

                    // log exiting and return true
                    this.logger.exiting(this.getClass().getName(),
                            "hupUntilUp", true);
                    return true;
                }
            }

            this.logger.log(Level.FINE, "Sending HUP to nodes");
            // send a HUP signal to all nodes
            for (ProxyNode node : this.nodes.values()) {
                this.logger.log(Level.FINE, "Sending HUP to node "
                        + node.toString());
                node.hup();
            }

            // continue in loop
        }

        // no retries left and not all nodes are up; log exiting and return
        // failure
        this.logger.exiting(this.getClass().getName(), "hupUntilUp", false);
        return false;
    }

    public void hupAllNodes() throws PuppeTorException, RemoteException {

        // log entering
        this.logger.entering(this.getClass().getName(), "hupAllNodes");

        // check if all nodes are running
        for (ProxyNode node : this.nodes.values()) {
            if (node.getNodeState() != NodeState.RUNNING) {
                IllegalStateException e = new IllegalStateException(
                        "All nodes must be running before sending them HUP "
                                + "commands!");
                this.logger.throwing(this.getClass().getName(), "hupAllNodes",
                        e);
                throw e;
            }
        }

        // send a HUP signal to all nodes
        for (ProxyNode node : this.nodes.values()) {
            this.logger.log(Level.FINE, "Sending HUP to node "
                    + node.toString());
            node.hup();
        }

        // no retries left and not all nodes are up; log exiting and return
        // failure
        this.logger.exiting(this.getClass().getName(), "hupAllNodes");
    }

    public void hupAllDirectories() throws PuppeTorException, RemoteException {

        // log entering
        this.logger.entering(this.getClass().getName(), "hupAllDirectories");

        // check if all directory nodes are running
        for (ProxyNode node : this.nodes.values()) {
            if (node instanceof DirectoryNode
                    && node.getNodeState() != NodeState.RUNNING) {
                IllegalStateException e = new IllegalStateException(
                        "All directory nodes must be running before sending "
                                + "them HUP commands!");
                this.logger.throwing(this.getClass().getName(),
                        "hupAllDirectories", e);
                throw e;
            }
        }

        // send a HUP signal to all nodes
        for (ProxyNode node : this.nodes.values()) {
            if (node instanceof DirectoryNode) {
                this.logger.log(Level.FINE, "Sending HUP to node "
                        + node.toString());
                node.hup();
            }
        }

        // no retries left and not all nodes are up; log exiting and return
        // failure
        this.logger.exiting(this.getClass().getName(), "hupAllDirectories");
    }

    public void shutdownNodes() throws PuppeTorException, RemoteException {

        // log entering
        this.logger.entering(this.getClass().getName(), "shutdownNodes");

        // iteratively shut down all running nodes; if an exception is caught,
        // continue shutting down the other nodes and throw the first exception
        // subsequently
        PuppeTorException firstCaughtException = null;
        for (ProxyNode node : this.nodes.values()) {
            if (node.getNodeState() == NodeState.RUNNING) {
                try {
                    node.shutdown();
                } catch (PuppeTorException e) {
                    if (firstCaughtException == null) {
                        firstCaughtException = e;
                    }
                }
            }
        }

        // if an exception was caught during shutting down nodes, throw the
        // first caught exception
        if (firstCaughtException != null) {
            this.logger.throwing(this.getClass().getName(), "shutdownNodes",
                    firstCaughtException);
            throw firstCaughtException;
        }

        // log exiting
        this.logger.exiting(this.getClass().getName(), "shutdownNodes");
    }

    public boolean startNodes(long maximumTimeToWaitInMillis)
            throws PuppeTorException, RemoteException {

        // log entering
        this.logger.entering(this.getClass().getName(), "startNodes",
                maximumTimeToWaitInMillis);

        // check node states
        for (ProxyNode node : this.nodes.values()) {
            if (node.getNodeState() != NodeState.CONFIGURATION_WRITTEN) {
                IllegalStateException e = new IllegalStateException(
                        "All configurations must be written before starting "
                                + "nodes!");
                this.logger
                        .throwing(this.getClass().getName(), "startNodes", e);
                throw e;
            }
        }

        // check parameter
        if (maximumTimeToWaitInMillis < 0) {
            IllegalArgumentException e = new IllegalArgumentException();
            this.logger.throwing(this.getClass().getName(), "startNodes", e);
            throw e;
        }

        // remember time when we begin starting the nodes
        long before = System.currentTimeMillis();

        // start nodes in parallel
        Set<NodeStarter> allNodeStarters = new HashSet<NodeStarter>();
        for (ProxyNode node : this.nodes.values()) {
            NodeStarter nodeStarter = new NodeStarter(node,
                    maximumTimeToWaitInMillis);
            allNodeStarters.add(nodeStarter);
            nodeStarter.start();
        }

        // wait for all node starts to complete
        for (NodeStarter nodeStarter : allNodeStarters) {

            // join node starts one after the other
            try {
                nodeStarter.join();
            } catch (InterruptedException e) {
                // this happens?! we have some kind of problem here!
                this.logger.log(Level.WARNING,
                        "Interrupt while joining node starter!");

                // log exiting and return false
                this.logger.exiting(this.getClass().getName(), "startNodes",
                        false);
                return false;
            }

            // if any thread has caught an exception, throw that exception now
            Exception caughtException = nodeStarter.caughtException;
            if (caughtException != null) {
                PuppeTorException ex = new PuppeTorException(
                        "Exception while starting node "
                                + nodeStarter.node.getNodeName(),
                        caughtException);
                this.logger.throwing(this.getClass().getName(), "startNodes",
                        ex);
                throw ex;
            }

            // if node start did not succeed in the given time, fail
            if (!nodeStarter.success) {
                this.logger.log(Level.WARNING,
                        "Starting nodes was not successful in "
                                + (maximumTimeToWaitInMillis / 1000)
                                + " seconds.", this.networkName);

                // log exiting and return false
                this.logger.exiting(this.getClass().getName(), "startNodes",
                        false);
                return false;
            }
        }

        // determine how long we took to start all nodes
        long after = System.currentTimeMillis();
        this.logger.log(Level.FINE, "Starting nodes was successful and took "
                + ((after - before) / 1000) + " seconds.", this.networkName);

        // log exiting and return true
        this.logger.exiting(this.getClass().getName(), "startNodes", true);
        return true;
    }

    @Override
    public String toString() {
        return this.getClass().getSimpleName() + ": networkName=\""
                + this.networkName;
    }

    public String getNetworkName() {
        return this.networkName;
    }

    public void writeConfigurations() throws PuppeTorException, RemoteException {

        // log entering
        this.logger.entering(this.getClass().getName(), "writeConfigurations");

        // write configurations for all nodes
        for (ProxyNode node : this.nodes.values()) {
            node.writeConfiguration();
        }

        // log exiting
        this.logger.exiting(this.getClass().getName(), "writeConfigurations");
    }

    public void configureAsInterconnectedPrivateNetwork(Network remoteNetwork)
            throws PuppeTorException, RemoteException {

        // log entering
        logger.entering(this.getClass().getName(), "mergeNetworks",
                remoteNetwork);

        // collect dir strings from this and the remote network
        List<String> ourDirServerStrings = new ArrayList<String>();
        for (DirectoryNode directory : this.getAllDirectoryNodes().values()) {
            ourDirServerStrings.add(directory.getDirServerString());
        }
        List<String> remoteDirServerStrings = new ArrayList<String>();
        for (DirectoryNode directory : remoteNetwork.getAllDirectoryNodes()
                .values()) {
            remoteDirServerStrings.add(directory.getDirServerString());
        }

        // add dir strings of local directories to all nodes of the other
        // network and vice versa
        for (ProxyNode node : remoteNetwork.getAllNodes().values()) {
            node.addConfigurations(ourDirServerStrings);
        }
        for (ProxyNode node : this.getAllNodes().values()) {
            node.addConfigurations(remoteDirServerStrings);
        }

        // collect router fingerprints from all routers in this and the remote
        // network
        Set<String> ourApprovedRoutersStrings = new TreeSet<String>();
        for (RouterNode router : this.getAllRouterNodes().values()) {
            ourApprovedRoutersStrings.add(router.getFingerprint());
        }
        Set<String> remoteApprovedRoutersStrings = new TreeSet<String>();
        for (RouterNode router : remoteNetwork.getAllRouterNodes().values()) {
            remoteApprovedRoutersStrings.add(router.getFingerprint());
        }

        // add the fingerprints of local routers and directories to the
        // directories of the other network and vice versa
        for (DirectoryNode node : remoteNetwork.getAllDirectoryNodes().values()) {
            node.addApprovedRouters(ourApprovedRoutersStrings);
        }
        for (DirectoryNode node : this.getAllDirectoryNodes().values()) {
            node.addApprovedRouters(remoteApprovedRoutersStrings);
        }

        // log exiting
        logger.exiting(this.getClass().getName(), "mergeNetworks");
    }

    public void addTemplateConfiguration(Class<? extends ProxyNode> nodeClass,
            String templateConfigurationString) {

        // log entering
        logger.entering(this.getClass().getName(), "addTemplateConfiguration",
                new Object[] { nodeClass, templateConfigurationString });

        // check parameters
        if (nodeClass == null || templateConfigurationString == null
                || templateConfigurationString.length() < 1
                || !templateConfigurationString.contains(" ")) {
            IllegalArgumentException e = new IllegalArgumentException();
            this.logger.throwing(this.getClass().getName(),
                    "addTemplateConfiguration", e);
            throw e;
        }

        // add template string to appropriate template configuration
        if (nodeClass == ProxyNode.class) {
            ProxyNodeImpl.templateConfiguration
                    .add(templateConfigurationString);
        } else if (nodeClass == RouterNode.class) {
            RouterNodeImpl.templateConfiguration
                    .add(templateConfigurationString);
        } else if (nodeClass == DirectoryNode.class) {
            DirectoryNodeImpl.templateConfiguration
                    .add(templateConfigurationString);
        } else {
            IllegalArgumentException e = new IllegalArgumentException();
            this.logger.throwing(this.getClass().getName(),
                    "addTemplateConfiguration", e);
            throw e;
        }

        // log exiting
        logger.exiting(this.getClass().getName(), "addTemplateConfiguration");
    }

    public List<String> getTemplateConfiguration(
            Class<? extends ProxyNode> nodeClass) {

        // log entering
        logger.entering(this.getClass().getName(), "getTemplateConfiguration",
                nodeClass);

        // check parameter
        if (nodeClass == null) {
            IllegalArgumentException e = new IllegalArgumentException();
            this.logger.throwing(this.getClass().getName(),
                    "getTemplateConfiguration", e);
            throw e;
        }

        // obtain reference on appropriate template configuration
        List<String> result = null;
        if (nodeClass == ProxyNode.class) {
            result = new ArrayList<String>(ProxyNodeImpl.templateConfiguration);
        } else if (nodeClass == RouterNode.class) {
            result = new ArrayList<String>(RouterNodeImpl.templateConfiguration);
        } else if (nodeClass == DirectoryNode.class) {
            result = new ArrayList<String>(
                    DirectoryNodeImpl.templateConfiguration);
        } else {
            IllegalArgumentException e = new IllegalArgumentException();
            this.logger.throwing(this.getClass().getName(),
                    "getTemplateConfiguration", e);
            throw e;
        }

        // log exiting and return result
        logger.exiting(this.getClass().getName(), "getTemplateConfiguration",
                result);
        return result;
    }

    public void removeTemplateConfiguration(
            Class<? extends ProxyNode> nodeClass,
            String templateConfigurationKey) {

        // log entering
        logger.entering(this.getClass().getName(),
                "removeTemplateConfiguration", new Object[] { nodeClass,
                        templateConfigurationKey });

        // check parameters
        if (nodeClass == null || templateConfigurationKey == null
                || templateConfigurationKey.length() < 1) {
            IllegalArgumentException e = new IllegalArgumentException();
            this.logger.throwing(this.getClass().getName(),
                    "removeTemplateConfiguration", e);
            throw e;
        }

        // obtain reference on appropriate template configuration
        List<String> templateConfig = null;
        if (nodeClass == ProxyNode.class) {
            templateConfig = ProxyNodeImpl.templateConfiguration;
        } else if (nodeClass == RouterNode.class) {
            templateConfig = RouterNodeImpl.templateConfiguration;
        } else if (nodeClass == DirectoryNode.class) {
            templateConfig = DirectoryNodeImpl.templateConfiguration;
        } else {
            IllegalArgumentException e = new IllegalArgumentException();
            this.logger.throwing(this.getClass().getName(),
                    "removeTemplateConfiguration", e);
            throw e;
        }

        // iterate over existing template configuration strings and remove all
        // configuration strings that have the given configuration key
        List<String> configurationStringsToRemove = new ArrayList<String>();
        for (String currentConfigurationString : templateConfig) {
            String currentConfigurationKey = currentConfigurationString
                    .substring(0, currentConfigurationString.indexOf(" "));
            if (currentConfigurationKey.equals(templateConfigurationKey)) {
                configurationStringsToRemove.add(currentConfigurationString);
            }
        }
        templateConfig.removeAll(configurationStringsToRemove);

        // log exiting
        logger
                .exiting(this.getClass().getName(),
                        "removeTemplateConfiguration");
    }

    public boolean bindAtRmiregistry() throws RemoteException {

        // log entering
        logger.entering(this.getClass().getName(), "bindAtRmiregistry");

        // set the RMISecurityManager
        System.setSecurityManager(new RMISecurityManager());

        // bind the network to the rmiregistry
        try {
            Naming.rebind("//127.0.0.1/" + networkName, this);
        } catch (MalformedURLException e) {
            this.logger.log(Level.WARNING,
                    "URL to bind this network to is malformed!", e);
            logger.exiting(this.getClass().getName(), "bindAtRmiregistry",
                    false);
            return false;
        }

        // log exiting
        logger.exiting(this.getClass().getName(), "bindAtRmiregistry", true);
        return true;
    }

    /**
     * Returns the current port number and increments it afterwards.
     * 
     * @return The current port number.
     */
    int getNextPortNumber() {
        return this.portCounter++;
    }
}
