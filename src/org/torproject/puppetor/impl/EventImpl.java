/*
 * Copyright 2007 Karsten Loesing
 * Copyright 2008-2009 Karsten Loesing and Sebastian Hahn
 * See LICENSE for licensing information
 */
package org.torproject.puppetor.impl;

import java.util.Date;

import org.torproject.puppetor.Event;
import org.torproject.puppetor.EventType;


/**
 * Implementation of <code>Event</code>.
 * 
 * @author kloesing
 */
@SuppressWarnings("serial")
public class EventImpl implements Event {

    /**
     * The source of this event.
     */
    private String source;

    /**
     * The type of this event.
     */
    private EventType type;

    /**
     * Either the log message that led to firing this event, or an internal
     * message.
     */
    private String message;

    /**
     * The occurrence time of the event or of the corresponding log statement.
     */
    private long occurrenceTime;

    /**
     * Creates a new <code>EventImpl</code>.
     * 
     * @param occurrenceTime
     *            The occurrence time of the event or of the corresponding log
     *            statement.
     * @param source
     *            The source of this event.
     * @param type
     *            The type of this event.
     * @param message
     *            Either the log message that led to firing this event, or an
     *            internal message.
     */
    EventImpl(long occurrenceTime, String source, EventType type, String message) {
        this.occurrenceTime = occurrenceTime;
        this.source = source;
        this.type = type;
        this.message = message;
    }

    /**
     * Creates a new <code>EventImpl</code>.
     * 
     * @param source
     *            The source of this event.
     * @param message
     *            Either the log message that led to firing this event, or an
     *            internal message.
     */
    EventImpl(String source, String message) {
        this.source = source;
        this.message = message;
    }

    public String getSource() {
        return this.source;
    }

    public EventType getType() {
        return this.type;
    }

    /**
     * Sets the event type to the given type.
     * 
     * @param type
     *            The type of this event.
     */
    void setType(EventType type) {
        this.type = type;
    }

    public String getMessage() {
        return this.message;
    }

    public long getOccurrenceTime() {
        return this.occurrenceTime;
    }

    @Override
    public String toString() {
        return this.getClass().getSimpleName() + ": occurenceTime="
                + new Date(this.occurrenceTime) + ", source=\"" + this.source
                + "\", type=" + this.type.getTypeName() + ", message=\""
                + this.message + "\"";
    }

    /**
     * Sets the occurrence time to the given time.
     * 
     * @param occurrenceTime
     *            The occurrence time of the event or of the corresponding log
     *            statement.
     */
    void setOccurenceTime(long occurrenceTime) {
        this.occurrenceTime = occurrenceTime;
    }
}
