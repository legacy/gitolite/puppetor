/*
 * Copyright 2007 Karsten Loesing
 * Copyright 2008-2009 Karsten Loesing and Sebastian Hahn
 * See LICENSE for licensing information
 */
package org.torproject.puppetor.impl;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.torproject.puppetor.HiddenService;
import org.torproject.puppetor.PuppeTorException;


/**
 * Implementation of <code>HiddenService</code>.
 * 
 * @author kloesing
 */
@SuppressWarnings("serial")
public class HiddenServiceImpl extends UnicastRemoteObject implements
        HiddenService {

    /**
     * Logger for this node which is called "hidserv." plus the name of this
     * hidden service.
     */
    protected Logger logger;

    /**
     * The node at which this hidden service is configured.
     */
    private ProxyNodeImpl node;

    /**
     * Name of the hidden service that will be used as name for the hidden
     * service directory.
     */
    private String serviceName;

    /**
     * The TCP port on which the service will be available for requests.
     */
    private int servicePort;

    /**
     * The virtual TCP port that this hidden service runs on as it is announced
     * to clients.
     */
    private int virtualPort;

    /**
     * Adds the entries for a hidden service to the configuration of this node.
     * 
     * Creates a new <code>HiddenServiceImpl</code>.
     * 
     * @param node
     *            The node at which this hidden service is configured.
     * @param serviceName
     *            Name of the hidden service that will be used as name for the
     *            hidden service directory. May neither be <code>null</code>
     *            or a zero-length string.
     * @param servicePort
     *            The TCP port on which the service will be available for
     *            requests. This can, but need not be different from the virtual
     *            port that is announced to clients. May not be negative or
     *            greater than 65535.
     * @param virtualPort
     *            The virtual TCP port that this hidden service runs on as it is
     *            announced to clients. May not be negative or greater than
     *            65535.
     * @throws IllegalArgumentException
     *             Thrown if an invalid value is given for either of the
     *             parameters.
     * @throws RemoteException
     *             Thrown if an error occurs when accessed remotely.
     */
    HiddenServiceImpl(ProxyNodeImpl node, String serviceName, int servicePort,
            int virtualPort) throws RemoteException {

        // check if networkName can be used as logger name
        if (serviceName == null || serviceName.length() == 0) {
            throw new IllegalArgumentException("Invalid serviceName: "
                    + serviceName);
        }

        // create logger
        this.logger = Logger.getLogger("hidserv." + serviceName);

        // log entering
        this.logger.entering(this.getClass().getName(), "HiddenServiceImpl",
                new Object[] { node, serviceName, servicePort, virtualPort });

        // check parameters
        if (serviceName == null || serviceName.length() == 0 || servicePort < 0
                || servicePort > 65535 || virtualPort < 0
                || virtualPort > 65535) {
            this.logger.log(Level.SEVERE,
                    "Illegal argument when adding hidden service!");
            IllegalArgumentException e = new IllegalArgumentException();
            this.logger.throwing(this.getClass().getName(), "addHiddenService",
                    e);
            throw e;
        }

        // store parameter values
        this.node = node;
        this.serviceName = serviceName;
        this.servicePort = servicePort;
        this.virtualPort = virtualPort;

        // log exiting
        this.logger.exiting(this.getClass().getName(), "HiddenServiceImpl");
    }

    public String determineOnionAddress() throws PuppeTorException {

        // log entering
        this.logger.entering(this.getClass().getName(), "getOnionAddress");

        // check if hidden service directory exists
        File hiddenServiceFile = new File(this.node.getWorkingDir()
                .getAbsolutePath()
                + File.separator
                + this.serviceName
                + File.separator
                + "hostname");
        if (!hiddenServiceFile.exists()) {
            this.logger.log(Level.SEVERE,
                    "Hidden service directory or hostname file does not exist: "
                            + hiddenServiceFile.getAbsolutePath());

            PuppeTorException e = new PuppeTorException(
                    "Hidden service directory or hostname file does not exist: "
                            + hiddenServiceFile.getAbsolutePath());
            this.logger.throwing(this.getClass().getName(), "getOnionAddress",
                    e);
            throw e;
        }

        // read hostname from file
        String address = null;
        try {
            BufferedReader br = new BufferedReader(new FileReader(
                    hiddenServiceFile));
            address = br.readLine();
            br.close();
        } catch (IOException e) {
            PuppeTorException ex = new PuppeTorException(
                    "Could not read hostname file!", e);
            this.logger.throwing(this.getClass().getName(), "getOnionAddress",
                    ex);
            throw ex;
        }

        // log exiting and return address
        this.logger.exiting(this.getClass().getName(), "getOnionAddress",
                address);
        return address;
    }

    public String getServiceName() {
        return serviceName;
    }

    public int getServicePort() {
        return servicePort;
    }

    public int getVirtualPort() {
        return virtualPort;
    }
}
