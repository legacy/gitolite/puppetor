/*
 * Copyright 2007 Karsten Loesing
 * Copyright 2008-2009 Karsten Loesing and Sebastian Hahn
 * See LICENSE for licensing information
 */
package org.torproject.puppetor.impl;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.net.InetSocketAddress;
import java.net.Proxy;
import java.net.Socket;
import java.net.SocketTimeoutException;
import java.net.Proxy.Type;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.torproject.puppetor.ClientApplication;
import org.torproject.puppetor.ClientEventType;


/**
 * Implementation of <code>ClientApplication</code>.
 * 
 * @author kloesing
 */
@SuppressWarnings("serial")
public class ClientApplicationImpl extends UnicastRemoteObject implements
        ClientApplication {

    /**
     * Internal thread class that is used to perform requests.
     */
    private class RequestThread extends Thread {

        /**
         * Flag to remember whether requests are performed at the moment (<code>true</code>),
         * or have been stopped (<code>false</code>).
         */
        private boolean connected;

        /**
         * Number of retries to be performed.
         */
        private int retries;

        /**
         * Flag that determines whether requests shall be stopped after the
         * first successful reply (<code>true</code>), or not (<code>false</code>).
         */
        private boolean stopOnSuccess;

        /**
         * Timeout in milliseconds for each retry.
         */
        private long timeoutForEachRetry;

        /**
         * Creates a new thread, but does not start performing requests, yet.
         * 
         * @param retries
         *            Number of retries to be performed.
         * @param timeoutForEachRetry
         *            Timeout in milliseconds for each retry.
         * @param stopOnSuccess
         *            Flag that determines whether requests shall be stopped
         *            after the first successful reply (<code>true</code>),
         *            or not (<code>false</code>).
         */
        RequestThread(int retries, long timeoutForEachRetry,
                boolean stopOnSuccess) {

            // log entering
            logger
                    .entering(this.getClass().getName(), "RequestThread",
                            new Object[] { retries, timeoutForEachRetry,
                                    stopOnSuccess });

            // check parameters
            if (retries < 0 || timeoutForEachRetry < 0) {
                IllegalArgumentException e = new IllegalArgumentException();
                logger.throwing(this.getClass().getName(), "RequestThread", e);
                throw e;
            }

            // remember parameters
            this.retries = retries;
            this.timeoutForEachRetry = timeoutForEachRetry;
            this.stopOnSuccess = stopOnSuccess;

            // start connected
            this.connected = true;

            // log exiting
            logger.exiting(this.getClass().getName(), "RequestThread");
        }

        /**
         * Perform one or more requests.
         */
        @Override
        public void run() {

            // log entering
            logger.entering(this.getClass().getName(), "run");

            try {

                // set Tor as proxy
                InetSocketAddress isa = new InetSocketAddress("127.0.0.1",
                        socksPort);
                Proxy p = new Proxy(Type.SOCKS, isa);

                // create target address for socket -- don't resolve the target
                // name to an IP address!
                InetSocketAddress hs = InetSocketAddress.createUnresolved(
                        targetName, targetPort);

                // start retry loop
                for (int i = 0; connected && i < retries; i++) {

                    // log this try
                    logger.log(Level.FINE, "Trying to perform request");

                    // remember when we started
                    long timeBeforeConnectionAttempt = System
                            .currentTimeMillis();

                    // send event to event manager
                    eventManager.observeInternalEvent(
                            timeBeforeConnectionAttempt,
                            ClientApplicationImpl.this
                                    .getClientApplicationName(),
                            ClientEventType.CLIENT_SENDING_REQUEST,
                            "Sending request.");

                    Socket s = null;
                    try {

                        // create new socket using Tor as proxy
                        s = new Socket(p);

                        // try to connect to remote server
                        s.connect(hs, (int) timeoutForEachRetry);

                        // open output stream to write request
                        PrintStream out = new PrintStream(s.getOutputStream());
                        out.print("GET / HTTP/1.0\r\n\r\n");

                        // open input stream to read reply
                        BufferedReader in = new BufferedReader(
                                new InputStreamReader(s.getInputStream()));

                        // read the response
                        String line = null;
                        StringBuilder response = new StringBuilder();
                        while ((line = in.readLine()) != null) {
                            response.append(line + "\n");
                        }

                        // send event to event manager
                        eventManager.observeInternalEvent(System
                                .currentTimeMillis(),
                                ClientApplicationImpl.this
                                        .getClientApplicationName(),
                                ClientEventType.CLIENT_REPLY_RECEIVED,
                                "Received response: " + response.toString());

                        // if we should stop on success, stop further connection
                        // attempts
                        if (this.stopOnSuccess) {
                            this.connected = false;
                        }

                        // clean up socket
                        in.close();
                        out.close();
                        s.close();

                    } catch (SocketTimeoutException e) {

                        // log warning
                        logger.log(Level.WARNING,
                                "Connection to remote server timed out!", e);

                        // send event to event manager
                        eventManager.observeInternalEvent(System
                                .currentTimeMillis(),
                                ClientApplicationImpl.this
                                        .getClientApplicationName(),
                                ClientEventType.CLIENT_GAVE_UP_REQUEST,
                                "Giving up request.");

                        // try again immediately, if there are retries left

                    } catch (IOException e) {

                        // log warning
                        logger.log(Level.WARNING,
                                "Connection to remote server could not be "
                                        + "established!", e);

                        // send event to event manager
                        eventManager.observeInternalEvent(System
                                .currentTimeMillis(),
                                ClientApplicationImpl.this
                                        .getClientApplicationName(),
                                ClientEventType.CLIENT_GAVE_UP_REQUEST,
                                "Giving up request.");

                        // wait for the rest of the timeout
                        long timeOfTimeoutLeft = timeBeforeConnectionAttempt
                                + this.timeoutForEachRetry
                                - System.currentTimeMillis();
                        if (timeOfTimeoutLeft > 0) {
                            try {
                                Thread.sleep(timeOfTimeoutLeft);
                            } catch (InterruptedException ex) {
                                // do nothing
                            }
                        }

                    } finally {

                        // close connection
                        try {

                            // try to close socket
                            logger.log(Level.FINER, "Trying to close socket.");
                            s.close();
                            logger.log(Level.FINE, "Socket closed.");

                        } catch (Exception e1) {

                            // log warning
                            logger.log(Level.WARNING,
                                    "Exception when trying to close socket!",
                                    e1);
                        }
                    }
                }

            } catch (Exception e) {

                // log that we have been interrupted
                logger.log(Level.WARNING, "Client has been interrupted!", e);

            } finally {

                // we are done here
                logger.log(Level.FINE, "Requests performed!");

                // send event to event manager
                eventManager.observeInternalEvent(System.currentTimeMillis(),
                        ClientApplicationImpl.this.getClientApplicationName(),
                        ClientEventType.CLIENT_REQUESTS_PERFORMED,
                        "Requests performed.");

                // log exiting
                logger.exiting(this.getClass().getName(), "run");
            }
        }

        /**
         * Immediately stops this and all possibly subsequent requests.
         */
        public void stopRequest() {

            // log entering
            logger.entering(this.getClass().getName(), "stopRequest");

            // change connected state to false and interrupt thread
            this.connected = false;
            this.interrupt();

            // log exiting
            logger.exiting(this.getClass().getName(), "stopRequest");
        }
    }

    /**
     * Name of this client application that is used as logger name of this node.
     */
    private String clientApplicationName;

    /**
     * Thread that performs the requests in the background.
     */
    private RequestThread clientThread;

    /**
     * Event manager that handles all events concerning this client application.
     */
    private EventManagerImpl eventManager;

    /**
     * Logger for this client which is called "client." plus the name of this
     * client application.
     */
    private Logger logger;

    /**
     * SOCKS port of the local Tor node to which requests are sent.
     */
    private int socksPort;

    /**
     * Target name for the requests sent by this client; can be a publicly
     * available URL or an onion address.
     */
    private String targetName;

    /**
     * Target port for the requests sent by this client; can be either a server
     * port or a virtual port of a hidden service.
     */
    private int targetPort;

    /**
     * Creates a new HTTP client within this JVM, but does not start sending
     * requests.
     * 
     * @param network
     *            Network to which this HTTP client belongs; at the moment this
     *            is only used to determine the event manager instance.
     * @param clientApplicationName
     *            Name of this client that is used as part of the logger name.
     * @param targetName
     *            Target name for requests; can be either a server name/address
     *            or an onion address.
     * @param targetPort
     *            Target port for requests; can be either a server port or a
     *            virtual port of a hidden service.
     * @param socksPort
     *            SOCKS port of the local Tor node.
     * @throws IllegalArgumentException
     *             If at least one of the parameters is <code>null</code> or
     *             has an invalid value.
     * @throws RemoteException
     *             Thrown if an error occurs when accessed remotely.
     */
    ClientApplicationImpl(NetworkImpl network, String clientApplicationName,
            String targetName, int targetPort, int socksPort)
            throws RemoteException {

        // check if clientApplicationName can be used as logger name
        if (clientApplicationName == null
                || clientApplicationName.length() == 0) {
            throw new IllegalArgumentException(
                    "Invalid clientApplicationName: " + clientApplicationName);
        }

        // create logger
        this.logger = Logger.getLogger("client." + clientApplicationName);

        // log entering
        this.logger.entering(this.getClass().getName(),
                "ClientApplicationImpl", new Object[] { network,
                        clientApplicationName, targetName, targetPort,
                        socksPort });

        // check parameters
        if (network == null || targetName == null || targetName.length() == 0
                || targetPort < 0 || targetPort > 65535 || socksPort < 0
                || socksPort > 65535) {
            IllegalArgumentException e = new IllegalArgumentException();
            this.logger.throwing(this.getClass().getName(),
                    "ClientApplicationImpl", e);
            throw e;
        }

        // remember parameters
        this.clientApplicationName = clientApplicationName;
        this.targetName = targetName;
        this.targetPort = targetPort;
        this.socksPort = socksPort;

        // obtain and store reference on event manager
        this.eventManager = network.getEventManagerImpl();

        // log exiting
        this.logger.exiting(this.getClass().getName(), "ClientApplicationImpl");
    }

    public synchronized void startRequests(int retries,
            long timeoutForEachRetry, boolean stopOnSuccess) {

        // log entering
        this.logger.entering(this.getClass().getName(), "performRequest",
                new Object[] { retries, timeoutForEachRetry, stopOnSuccess });

        // check parameters
        if (retries <= 0 || timeoutForEachRetry < 0) {
            IllegalArgumentException e = new IllegalArgumentException();
            this.logger
                    .throwing(this.getClass().getName(), "performRequest", e);
            throw e;
        }

        // check if we already have started a request (TODO change this to allow
        // multiple requests in parallel? would be possible)
        if (this.clientThread != null) {
            IllegalStateException e = new IllegalStateException(
                    "Another request has already been started!");
            this.logger
                    .throwing(this.getClass().getName(), "performRequest", e);
            throw e;
        }

        // create a thread that performs requests in the background
        this.clientThread = new RequestThread(retries, timeoutForEachRetry,
                stopOnSuccess);
        this.clientThread.setName("Request Thread");
        this.clientThread.setDaemon(true);
        this.clientThread.start();

        // log exiting
        this.logger.exiting(this.getClass().getName(), "performRequest");
    }

    public synchronized void stopRequest() {

        // log entering
        this.logger.entering(this.getClass().getName(), "stopRequest");

        // check if a request is running
        if (this.clientThread == null) {
            IllegalStateException e = new IllegalStateException("Cannot stop "
                    + "request, because no request has been started!");
            this.logger.throwing(this.getClass().getName(), "stopRequest", e);
            throw e;
        }

        // log this event
        this.logger.log(Level.FINE, "Shutting down client");

        // interrupt thread
        this.clientThread.stopRequest();

        // log exiting
        this.logger.exiting(this.getClass().getName(), "stopRequest");
    }

    @Override
    public String toString() {
        return this.getClass().getSimpleName() + ": clientApplicationName=\""
                + this.clientApplicationName + "\", targetAddress=\""
                + this.targetName + "\", targetPort=" + this.targetPort
                + ", socksPort=" + this.socksPort;
    }

    public String getClientApplicationName() {
        return clientApplicationName;
    }

    public int getSocksPort() {
        return socksPort;
    }

    public String getTargetName() {
        return targetName;
    }

    public int getTargetPort() {
        return targetPort;
    }
}
