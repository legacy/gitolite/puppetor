/*
 * Copyright 2007 Karsten Loesing
 * Copyright 2008-2009 Karsten Loesing and Sebastian Hahn
 * See LICENSE for licensing information
 */
package org.torproject.puppetor.impl;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.Map.Entry;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.torproject.puppetor.Event;
import org.torproject.puppetor.EventListener;
import org.torproject.puppetor.EventManager;
import org.torproject.puppetor.EventType;
import org.torproject.puppetor.HiddenServiceEventType;
import org.torproject.puppetor.NodeEventType;


/**
 * Implementation of <code>EventManager</code>.
 * 
 * @author kloesing
 */
@SuppressWarnings("serial")
public class EventManagerImpl extends UnicastRemoteObject implements
        EventManager {

    /**
     * Registered event handlers for specific sources.
     */
    private Map<String, Set<EventListener>> eventHandlers;

    /**
     * Registered event handlers for all sources.
     */
    private Set<EventListener> eventHandlersForAllSources;

    /**
     * Logger for this event manager which is called "event." plus the name of
     * the network.
     */
    private Logger logger;

    /**
     * Events observed so far.
     */
    private Map<String, List<Event>> observedEvents;

    /**
     * Set of all registered event sources. This is required to ensure that
     * requests for events from a given source specify valid event sources.
     */
    private Set<String> eventSources;

    /**
     * Creates a new <code>EventManagerImpl</code> for the network with name
     * <code>networkName</code> and initializes it.
     * 
     * @param networkName
     *            Name of this event manager that is used as part of the logger
     *            name.
     * @throws IllegalArgumentException
     *             Thrown if the given <code>networkName</code> is either
     *             <code>null</code> or a zero-length string.
     * @throws RemoteException
     *             Thrown if an error occurs when accessed remotely.
     */
    EventManagerImpl(String networkName) throws RemoteException {

        // check if networkName can be used as logger name
        if (networkName == null || networkName.length() == 0) {
            throw new IllegalArgumentException("Invalid networkName: "
                    + networkName);
        }

        // create logger
        this.logger = Logger.getLogger("event." + networkName);

        // log entering
        this.logger.entering(this.getClass().getName(), "EventManagerImpl",
                networkName);

        // create data structures
        this.observedEvents = new HashMap<String, List<Event>>();
        this.eventHandlers = new HashMap<String, Set<EventListener>>();
        this.eventHandlersForAllSources = new HashSet<EventListener>();
        this.eventSources = new HashSet<String>();

        // start thread to parse events
        Thread eventParseThread = new Thread() {
            @Override
            public void run() {
                while (true) {
                    parseNextEvent();
                }
            }
        };
        eventParseThread.setDaemon(true);
        eventParseThread.start();

        // initialize event type patterns
        initializeEventTypePatterns();

        // log exiting
        this.logger.exiting(this.getClass().getName(), "EventManagerImpl");
    }

    public synchronized List<Event> addEventListener(String source,
            EventListener listener) {

        // log entering
        this.logger.entering(this.getClass().getName(), "addEventListener",
                new Object[] { source, listener });

        // check parameters
        if (source == null || listener == null
                || !this.eventSources.contains(source)) {
            IllegalArgumentException e = new IllegalArgumentException();
            this.logger.throwing(this.getClass().getName(), "addEventListener",
                    e);
            throw e;
        }

        // if necessary, create new event listener set for source
        if (!this.eventHandlers.containsKey(source)) {
            this.eventHandlers.put(source, new HashSet<EventListener>());
        }

        // add listener
        this.eventHandlers.get(source).add(listener);

        // log change
        this.logger
                .log(Level.FINE, "Added event listener for source " + source);

        // log exiting and return
        List<Event> result = getEventHistory(source);
        this.logger.exiting(this.getClass().getName(), "addEventListener",
                result);
        return result;
    }

    public synchronized void addEventListener(EventListener listener) {

        // log entering
        this.logger.entering(this.getClass().getName(), "addEventListener",
                new Object[] { listener });

        // check parameters
        if (listener == null) {
            IllegalArgumentException e = new IllegalArgumentException();
            this.logger.throwing(this.getClass().getName(), "addEventListener",
                    e);
            throw e;
        }

        // add listener
        this.eventHandlersForAllSources.add(listener);

        // log change
        this.logger.log(Level.FINE, "Added event listener for all sources.");

        // log exiting and return
        this.logger.exiting(this.getClass().getName(), "addEventListener");
        return;
    }

    public synchronized List<Event> getEventHistory(String source) {

        // log entering
        this.logger.entering(this.getClass().getName(), "getEventHistory",
                source);

        // check parameter
        if (source == null || !this.eventSources.contains(source)) {
            IllegalArgumentException e = new IllegalArgumentException();
            this.logger.throwing(this.getClass().getName(), "getEventHistory",
                    e);
            throw e;
        }

        // prepare result
        List<Event> result = new ArrayList<Event>();

        // did we already observe events for this source?
        if (this.observedEvents.containsKey(source)) {
            // yes, add all events to result list
            result.addAll(this.observedEvents.get(source));
        }

        // log exiting and return result
        this.logger.exiting(this.getClass().getName(), "getEventHistory",
                result);
        return result;
    }

    public synchronized boolean hasEventOccured(String source, EventType type) {

        // log entering
        this.logger.entering(this.getClass().getName(), "hasEventOccured",
                new Object[] { source, type });

        // check parameters
        if (source == null || type == null
                || !this.eventSources.contains(source)) {
            IllegalArgumentException e = new IllegalArgumentException();
            this.logger.throwing(this.getClass().getName(), "hasEventOccured",
                    e);
            throw e;
        }

        // determine result
        boolean result = false;
        if (this.observedEvents.containsKey(source)) {
            for (Event event : this.observedEvents.get(source)) {
                if (event.getType() == type) {
                    result = true;
                    break;
                }
            }
        }

        // log exiting and return result
        this.logger.exiting(this.getClass().getName(), "hasEventOccured",
                result);
        return result;
    }

    /**
     * An ordered list of all log statements that are still unparsed.
     */
    private List<EventImpl> unparsedLogStatements = new LinkedList<EventImpl>();

    /**
     * Stores the occurrence of an unparsed Tor log events that might result in
     * an event. All such unparsed events are later parsed by a background
     * thread in invocation order. Then, the occurrence time and the event type
     * will be parsed from the log message; if the log message does not contain
     * anything of interest, the event will be discarded.
     * 
     * @param source
     *            The event source.
     * @param logMessage
     *            The log message.
     * @throws IllegalArgumentException
     *             Thrown if the source is unknown.
     */
    synchronized void observeUnparsedEvent(String source, String logMessage) {
        this.unparsedLogStatements.add(new EventImpl(source, logMessage));
        notifyAll();
    }

    /**
     * Add an internal event to the event queue.
     * 
     * @param occurrenceTime
     *            The occurrence time of the event.
     * @param source
     *            The event source.
     * @param type
     *            The event type.
     * @param message
     *            The event message.
     * @throws IllegalArgumentException
     *             Thrown if the source is unknown.
     */
    synchronized void observeInternalEvent(long occurrenceTime, String source,
            EventType type, String message) {

        if (!this.eventSources.contains(source)) {
            IllegalArgumentException e = new IllegalArgumentException();
            this.logger.throwing(this.getClass().getName(),
                    "observeInternalEvent", e);
            throw e;
        }

        this.unparsedLogStatements.add(new EventImpl(occurrenceTime, source,
                type, message));
        notifyAll();
    }

    /**
     * Parses a log statement coming from Tor and decides whether it is
     * interesting for us.
     */
    void parseNextEvent() {

        // wait for the next event in the queue
        EventImpl event = null;
        synchronized (this) {
            while (this.unparsedLogStatements.isEmpty()) {
                try {
                    wait();
                } catch (InterruptedException e) {
                }
            }
            event = this.unparsedLogStatements.remove(0);
        }

        // does the event contain a known source? if not, discard it
        if (!this.eventSources.contains(event.getSource())) {
            this.logger.log(Level.WARNING,
                    "Unknown event source while parsing an event: "
                            + event.getSource());
            return;
        }

        // does the event require parsing? if not, process immediately
        if (event.getType() != null) {
            observeEvent(event);
        } else {
            String line = event.getMessage();

            /*
             * the logging output of Tor does not contain a year component; put
             * in the year at the time of parsing (which happens approx. 10 ms
             * after the logging took place) in the good hope that this tool is
             * not run at midnight on New Year's Eve...
             */
            Calendar c = Calendar.getInstance();
            int currentYear = c.get(Calendar.YEAR);

            // try to apply one of the event type patterns
            for (Entry<Pattern, EventType> entry : eventTypePatterns.entrySet()) {
                Matcher matcher = entry.getKey().matcher(line);
                if (matcher.find()) {
                    SimpleDateFormat sdf = new SimpleDateFormat(
                            "MMM dd HH:mm:ss.SSS", Locale.US);
                    Date logTime = sdf.parse(line, new ParsePosition(0));
                    c.setTimeInMillis(logTime.getTime());
                    c.set(Calendar.YEAR, currentYear);
                    long t = c.getTimeInMillis();
                    event.setOccurenceTime(t);
                    event.setType(entry.getValue());
                    observeEvent(event);
                    break;
                }
            }
        }
    }

    /**
     * Map of all patterns, that should be included when parsing log statements
     * coming from Tor, and the respective event types of the events that should
     * be fired.
     */
    Map<Pattern, EventType> eventTypePatterns;

    public synchronized void registerEventTypePattern(String patternString,
            EventType eventType) {
        eventTypePatterns.put(Pattern.compile(patternString), eventType);
    }

    /**
     * Initializes the parsing engine with the standard log message patterns
     * that should be included in current Tor. Any further patterns need to be
     * added by the test application manually.
     */
    private void initializeEventTypePatterns() {
        eventTypePatterns = new HashMap<Pattern, EventType>();
        registerEventTypePattern("Opening Control listener on .*",
                NodeEventType.NODE_CONTROL_PORT_OPENED);
        registerEventTypePattern("Tor has successfully opened a circuit. "
                + "Looks like client functionality is working.",
                NodeEventType.NODE_CIRCUIT_OPENED);
        registerEventTypePattern(
                "Established circuit .* as introduction "
                        + "point for service .*",
                HiddenServiceEventType.BOB_BUILT_INTRO_CIRC_SENDING_ESTABLISH_INTRO);
        registerEventTypePattern("Received INTRO_ESTABLISHED cell on "
                + "circuit .* for service .*",
                HiddenServiceEventType.BOB_INTRO_ESTABLISHED_RECEIVED);
        registerEventTypePattern("Sending publish request for hidden "
                + "service .*", HiddenServiceEventType.BOB_SENDING_PUBLISH_DESC);
        registerEventTypePattern("Uploaded rendezvous descriptor",
                HiddenServiceEventType.BOB_DESC_PUBLISHED_RECEIVED);
        registerEventTypePattern("Received INTRODUCE2 cell for service .* "
                + "on circ .*", HiddenServiceEventType.BOB_INTRODUCE2_RECEIVED);
        registerEventTypePattern("Done building circuit .* to rendezvous "
                + "with cookie .* for service .*",
                HiddenServiceEventType.BOB_BUILT_REND_CIRC_SENDING_RENDEZVOUS1);
        registerEventTypePattern("begin is for rendezvous",
                HiddenServiceEventType.BOB_APP_CONN_OPENED);
        registerEventTypePattern("Got a hidden service request for ID '.*'",
                HiddenServiceEventType.ALICE_ONION_REQUEST_RECEIVED);
        registerEventTypePattern("Fetching rendezvous descriptor for "
                + "service .*", HiddenServiceEventType.ALICE_SENDING_FETCH_DESC);
        registerEventTypePattern("Sending fetch request for v2 descriptor "
                + "for service",
                HiddenServiceEventType.ALICE_SENDING_FETCH_DESC);
        registerEventTypePattern("Received rendezvous descriptor",
                HiddenServiceEventType.ALICE_DESC_FETCHED_RECEIVED);
        registerEventTypePattern(
                "Sending an ESTABLISH_RENDEZVOUS cell",
                HiddenServiceEventType.ALICE_BUILT_REND_CIRC_SENDING_ESTABLISH_RENDEZVOUS);
        registerEventTypePattern("Got rendezvous ack. This circuit is now "
                + "ready for rendezvous",
                HiddenServiceEventType.ALICE_RENDEZVOUS_ESTABLISHED_RECEIVED);
        registerEventTypePattern("introcirc is open",
                HiddenServiceEventType.ALICE_BUILT_INTRO_CIRC);
        registerEventTypePattern("Sending an INTRODUCE1 cell",
                HiddenServiceEventType.ALICE_SENDING_INTRODUCE1);
        registerEventTypePattern("Received ack. Telling rend circ",
                HiddenServiceEventType.ALICE_INTRODUCE_ACK_RECEIVED);
        registerEventTypePattern(
                "Got RENDEZVOUS2 cell from hidden service",
                HiddenServiceEventType.ALICE_RENDEZVOUS2_RECEIVED_APP_CONN_OPENED);
        registerEventTypePattern("Handling rendezvous descriptor post",
                HiddenServiceEventType.DIR_PUBLISH_DESC_RECEIVED);
        registerEventTypePattern("Handling rendezvous descriptor get",
                HiddenServiceEventType.DIR_FETCH_DESC_RECEIVED);
        registerEventTypePattern(
                "Received an ESTABLISH_INTRO request on circuit .*",
                HiddenServiceEventType.IPO_RECEIVED_ESTABLISH_INTRO_SENDING_INTRO_ESTABLISHED);
        registerEventTypePattern(
                "Received an INTRODUCE1 request on circuit .*",
                HiddenServiceEventType.IPO_RECEIVED_INTRODUCE1_SENDING_INTRODUCE2_AND_INTRODUCE_ACK);
        registerEventTypePattern(
                "Received an ESTABLISH_RENDEZVOUS request on circuit .*",
                HiddenServiceEventType.RPO_RECEIVED_ESTABLISH_RENDEZVOUS_SENDING_RENDEZVOUS_ESTABLISHED);
        registerEventTypePattern(
                "Got request for rendezvous from circuit .* to cookie .*",
                HiddenServiceEventType.RPO_RECEIVING_RENDEZVOUS1_SENDING_RENDEZVOUS2);
    }

    /**
     * Stores the given <code>event</code> from <code>source</code> to the
     * event history and propagates its occurrence to all registered event
     * handlers.
     * 
     * @param event
     *            The observed event.
     */
    private synchronized void observeEvent(Event event) {

        // log entering
        this.logger.entering(this.getClass().getName(), "observeEvent", event);

        String source = event.getSource();
        this.logger.log(Level.FINE, "Observed event " + event + " from source "
                + source + "!");

        // remember observed event
        if (!this.observedEvents.containsKey(event.getSource())) {
            this.observedEvents.put(source, new ArrayList<Event>());
        }
        this.observedEvents.get(source).add(event);

        // notify waiting threads
        notifyAll();

        // inform event listeners
        if (this.eventHandlers.containsKey(source)) {

            // make a copy of the event handler set, because some event handlers
            // might want to remove themselves from this set while handling the
            // event
            Set<EventListener> copyOfEventHandlers = new HashSet<EventListener>(
                    this.eventHandlers.get(source));

            for (EventListener eventHandler : copyOfEventHandlers) {

                this.logger.log(Level.FINE, "Informing event listener "
                        + eventHandler + " about recently observed event "
                        + event + " from source " + source + "!");
                try {
                    eventHandler.handleEvent(event);
                } catch (RemoteException e) {
                    this.logger.log(Level.WARNING,
                            "Cannot inform remote event listener about "
                                    + "event! Removing event listener!", e);
                    this.eventHandlers.remove(eventHandler);
                }
            }
        }

        // make a copy of the event handler set for all sources, because some
        // event handlers might want to remove themselves from this set while
        // handling the event
        Set<EventListener> copyOfEventHandlersForAllSources = new HashSet<EventListener>(
                this.eventHandlersForAllSources);

        for (EventListener eventHandler : copyOfEventHandlersForAllSources) {

            this.logger.log(Level.FINE, "Informing event listener "
                    + eventHandler + " about recently observed event " + event
                    + " from source " + source + "!");
            try {
                eventHandler.handleEvent(event);
            } catch (RemoteException e) {
                this.logger.log(Level.WARNING,
                        "Cannot inform remote event listener about "
                                + "event! Removing event listener!", e);
                this.eventHandlers.remove(eventHandler);
            }
        }

        // log exiting
        this.logger.exiting(this.getClass().getName(), "observeEvent");
    }

    public synchronized void removeEventListener(EventListener eventListener) {

        // log entering
        this.logger.entering(this.getClass().getName(), "removeEventListener",
                eventListener);

        // check parameters
        if (eventListener == null) {
            IllegalArgumentException e = new IllegalArgumentException();
            this.logger.throwing(this.getClass().getName(),
                    "removeEventListener", e);
            throw e;
        }

        // don't know to which source this listener has been added (may to more
        // than one), so remove it from all possible sets
        for (Set<EventListener> set : eventHandlers.values()) {
            if (set.remove(eventListener)) {
                logger.log(Level.FINE, "Removed event listener!");
            }
        }

        // remove from event listeners for all sources
        if (this.eventHandlersForAllSources.remove(eventListener)) {
            logger.log(Level.FINE, "Removed event listener!");
        }

        // log exiting
        this.logger.exiting(this.getClass().getName(), "removeEventListener");
    }

    public synchronized void waitForAnyOccurence(String source, EventType event) {

        // log entering
        this.logger.entering(this.getClass().getName(), "waitForAnyOccurence",
                new Object[] { source, event });

        // check parameters
        if (source == null || event == null
                || !this.eventSources.contains(source)) {
            IllegalArgumentException e = new IllegalArgumentException();
            this.logger.throwing(this.getClass().getName(),
                    "waitForAnyOccurence", e);
            throw e;
        }

        // invoke overloaded method with maximumTimeToWaitInMillis of -1L which
        // means to wait forever
        waitForAnyOccurence(source, event, -1L);

        // log exiting
        this.logger.exiting(this.getClass().getName(), "waitForAnyOccurence");

    }

    public synchronized boolean waitForAnyOccurence(String source,
            EventType event, long maximumTimeToWaitInMillis) {

        // log entering
        this.logger.entering(this.getClass().getName(), "waitForAnyOccurence",
                new Object[] { source, event, maximumTimeToWaitInMillis });

        // check parameters
        if (source == null || event == null
                || !this.eventSources.contains(source)) {
            IllegalArgumentException e = new IllegalArgumentException();
            this.logger.throwing(this.getClass().getName(),
                    "waitForAnyOccurence", e);
            throw e;
        }

        // check if we have already observed the event
        if (this.hasEventOccured(source, event)) {
            this.logger.log(Level.FINE, "Waiting for any occurence of event "
                    + event + " returned immediately!");
            this.logger.exiting(this.getClass().getName(),
                    "waitForAnyOccurence", true);
            return true;
        }

        // invoke method that waits for next occurence of the event
        boolean result = waitForNextOccurence(source, event,
                maximumTimeToWaitInMillis);

        // log exiting and return result
        this.logger.exiting(this.getClass().getName(), "waitForAnyOccurence",
                result);
        return result;
    }

    public synchronized void waitForNextOccurence(String source, EventType event) {

        // log entering
        this.logger.entering(this.getClass().getName(), "waitForNextOccurence",
                new Object[] { source, event });

        // check parameters
        if (source == null || event == null
                || !this.eventSources.contains(source)) {
            IllegalArgumentException e = new IllegalArgumentException();
            this.logger.throwing(this.getClass().getName(),
                    "waitForNextOccurence", e);
            throw e;
        }

        // invoke overloaded method with maximumTimeToWaitInMillis of -1L which
        // means to wait forever
        waitForNextOccurence(source, event, -1L);

        // log exiting
        this.logger.exiting(this.getClass().getName(), "waitForNextOccurence");
    }

    public synchronized boolean waitForNextOccurence(String source,
            EventType type, long maximumTimeToWaitInMillis) {

        // log entering
        this.logger.entering(this.getClass().getName(), "waitForNextOccurence",
                new Object[] { source, type, maximumTimeToWaitInMillis });

        // check parameters
        if (source == null || type == null
                || !this.eventSources.contains(source)) {
            IllegalArgumentException e = new IllegalArgumentException();
            this.logger.throwing(this.getClass().getName(),
                    "waitForNextOccurence", e);
            throw e;
        }

        // distinguish between negative waiting time (wait forever) and zero or
        // positive waiting time
        if (maximumTimeToWaitInMillis < 0) {

            // wait forever
            while (!this.hasEventOccured(source, type)) {

                this.logger.log(Level.FINEST,
                        "We will wait infinetely for the next occurence of "
                                + "event type " + type + " from source "
                                + source + "...");
                try {
                    wait();
                } catch (InterruptedException e) {
                    // don't handle
                }

                this.logger.log(Level.FINEST,
                        "We have been notified about an observed event while "
                                + "waiting for events of type " + type
                                + " from source " + source
                                + "; need to check whether the observed event "
                                + "is what we are looking for...");
            }

            this.logger.log(Level.FINE, "Waiting for occurence of event type "
                    + type + " succeeded!");

            // log exiting and return result
            this.logger.exiting(this.getClass().getName(),
                    "waitForNextOccurence", true);
            return true;

        } else {

            // wait for the given time at most
            long endOfTime = System.currentTimeMillis()
                    + maximumTimeToWaitInMillis;
            long timeLeft = 0;
            while (!this.hasEventOccured(source, type)
                    && (timeLeft = endOfTime - System.currentTimeMillis()) > 0) {

                this.logger.log(Level.FINEST, "We will wait for " + timeLeft
                        + " milliseconds for the next occurence of event type "
                        + type + " from source " + source + "...");

                try {
                    wait(timeLeft);
                } catch (InterruptedException e) {
                    // don't handle
                }

                this.logger.log(Level.FINEST,
                        "We have been notified about an observed event while "
                                + "waiting for events of type " + type
                                + " from source " + source
                                + "; need to check whether the observed event "
                                + "is what we are looking for...");
            }

            // determine result
            boolean result = this.hasEventOccured(source, type);

            this.logger.log(Level.FINE,
                    "Waiting for next occurence of event type " + type
                            + " from source " + source
                            + (result ? " succeeded!" : " did not succeed!"));

            // log exiting and return result
            this.logger.exiting(this.getClass().getName(),
                    "waitForNextOccurence", result);
            return result;
        }
    }

    /**
     * Adds the given <code>source</code> as possible event source.
     * 
     * @param source
     *            The name of the node, client, or server to add.
     * @throws IllegalArgumentException
     *             Thrown if there is already an event source with this name.
     */
    void addEventSource(String source) {

        // log entering
        this.logger.entering(this.getClass().getName(), "addEventSource",
                source);

        // check if source name is unique in this network
        if (this.eventSources.contains(source)) {
            IllegalArgumentException e = new IllegalArgumentException(
                    "There is already an event source with name " + source
                            + " in this network!");
            this.logger
                    .throwing(this.getClass().getName(), "addEventSource", e);
            throw e;
        }

        // add event source name
        this.eventSources.add(source);

        // log exiting
        this.logger.exiting(this.getClass().getName(), "addEventSource");
    }

    @Override
    public String toString() {
        return this.getClass().getSimpleName();
    }
}
