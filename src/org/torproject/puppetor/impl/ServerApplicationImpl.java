/*
 * Copyright 2007 Karsten Loesing
 * Copyright 2008-2009 Karsten Loesing and Sebastian Hahn
 * See LICENSE for licensing information
 */
package org.torproject.puppetor.impl;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.torproject.puppetor.ServerApplication;
import org.torproject.puppetor.ServerEventType;


/**
 * Implementation of <code>ServerApplication</code>.
 * 
 * @author kloesing
 */
@SuppressWarnings("serial")
public class ServerApplicationImpl extends UnicastRemoteObject implements
        ServerApplication {

    /**
     * Internal thread class that is used to process an incoming request.
     */
    private class HandlerThread extends Thread {

        /**
         * Accepted socket on which the request came in.
         */
        private Socket handleSocket = null;

        /**
         * Creates a new thread to handle the request coming in on
         * <code>handleSocket</code>, but does not start handling it.
         * 
         * @param handleSocket
         *            Accepted socket on which the request came in.
         */
        public HandlerThread(Socket handleSocket) {

            // log entering
            logger.entering(this.getClass().getName(), "HandlerThread",
                    handleSocket);

            // remember parameter
            this.handleSocket = handleSocket;

            // log exiting
            logger.exiting(this.getClass().getName(), "HandlerThread");
        }

        @Override
        public void run() {

            // log entering
            logger.entering(this.getClass().getName(), "run");

            try {

                // wait for request (don't mind the content)
                BufferedReader in = new BufferedReader(new InputStreamReader(
                        handleSocket.getInputStream()));
                in.read();

                // send event to event manager
                eventManager.observeInternalEvent(System.currentTimeMillis(),
                        ServerApplicationImpl.this.getServerApplicationName(),
                        ServerEventType.SERVER_RECEIVING_REQUEST_SENDING_REPLY,
                        "Receiving request.");

                // write response
                PrintStream out = new PrintStream(handleSocket
                        .getOutputStream());
                out.print("HTTP/1.0 200 OK\r\n");

            } catch (IOException e) {
                logger.log(Level.SEVERE,
                        "I/O exception while handling incoming request!");
                // we can't do more, because nobody takes notice of this thread.

                // log exiting
                logger.exiting(this.getClass().getName(), "run");
                return;
                // TODO do we need more?
            }

            // close socket
            try {
                handleSocket.close();
            } catch (IOException e) {
                logger
                        .log(Level.WARNING,
                                "I/O exception while closing socket!");

                // log exiting
                logger.exiting(this.getClass().getName(), "run");
                return;
            }

            // log exiting
            logger.exiting(this.getClass().getName(), "run");
        }
    }

    /**
     * Internal thread class that is used to listen for requests.
     */
    private class ListenThread extends Thread {

        /**
         * Flag to remember whether this thread listens for requests at the
         * moment (<code>true</code>), or has been stopped (<code>false</code>).
         */
        private boolean connected;

        /**
         * Creates a new thread to listen for requests, but does not start
         * listening, yet.
         */
        ListenThread() {

            // log entering
            logger.entering(this.getClass().getName(), "ListenThread");

            // start connected
            this.connected = true;

            // log exiting
            logger.exiting(this.getClass().getName(), "ListenThread");
        }

        @Override
        public void run() {

            // log entering
            logger.entering(this.getClass().getName(), "run");

            try {

                // create server socket
                ServerSocket serverSocket = null;
                try {
                    serverSocket = new ServerSocket(serverPort);
                } catch (IOException ioe) {
                    logger.log(Level.SEVERE,
                            "Can't open server socket on port " + serverPort
                                    + "!");

                    // log exiting
                    logger.exiting(this.getClass().getName(), "run");
                    return;
                }

                // as long as we are connected, accept incoming requests
                logger.log(Level.FINE, "Listening on port " + serverPort
                        + "...");
                while (connected) {
                    Socket incomingConnection = null;
                    try {
                        incomingConnection = serverSocket.accept();
                    } catch (Exception e) {
                        logger
                                .log(
                                        Level.SEVERE,
                                        "Exception while accepting socket requests! Stopping listening!",
                                        e);
                        break;
                    }
                    new HandlerThread(incomingConnection).start();
                }

            } catch (Exception e) {

                // log that we have been interrupted
                logger.log(Level.WARNING, "Server has been interrupted!", e);
            }

            // mark as disconnected
            this.connected = false;

            // log exiting
            logger.exiting(this.getClass().getName(), "run");
        }

        /**
         * Stops listening on server socket.
         */
        public void stopListening() {

            // log entering
            logger.entering(this.getClass().getName(), "stopListening");

            // change connected state to false and interrupt thread
            this.connected = false;
            this.interrupt();

            // log exiting
            logger.exiting(this.getClass().getName(), "stopListening");
        }
    }

    /**
     * Event manager that handles all events concerning this server application.
     */
    private EventManagerImpl eventManager;

    /**
     * Logger for this server which is called "server." plus the name of this
     * server application.
     */
    private Logger logger;

    /**
     * Name of this server application that is used as logger name of this node.
     */
    private String serverApplicationName;

    /**
     * Port on which this server will listen for incoming requests.
     */
    private int serverPort;

    /**
     * Thread that listens for requests in the background.
     */
    private Thread serverThread;

    /**
     * Creates a new HTTP server application within this JVM, but does not yet
     * listen for incoming requests.
     * 
     * @param network
     *            Network to which this HTTP server belongs; at the moment this
     *            is only used to determine the event manager instance.
     * @param serverApplicationName
     *            Name of this server that is used as part of the logger name.
     * @param serverPort
     *            Port on which this server will listen for incoming requests.
     * @throws IllegalArgumentException
     *             If at least one of the parameters is <code>null</code> or
     *             has an invalid value.
     * @throws RemoteException
     *             Thrown if an error occurs when accessed remotely.
     */
    ServerApplicationImpl(NetworkImpl network, String serverApplicationName,
            int serverPort) throws RemoteException {

        // check if serverApplicationName can be used as logger name
        if (serverApplicationName == null
                || serverApplicationName.length() == 0) {
            throw new IllegalArgumentException(
                    "Invalid serverApplicationName: " + serverApplicationName);
        }

        // create logger
        this.logger = Logger.getLogger("server." + serverApplicationName);

        // log entering
        this.logger.entering(this.getClass().getName(),
                "ServerApplicationImpl", new Object[] { network,
                        serverApplicationName, serverPort });

        // check parameters
        if (network == null || serverPort < 0 || serverPort > 65535) {
            IllegalArgumentException e = new IllegalArgumentException();
            this.logger.throwing(this.getClass().getName(),
                    "ServerApplicationImpl", e);
            throw e;
        }

        // remember parameters
        this.serverApplicationName = serverApplicationName;
        this.serverPort = serverPort;

        // obtain reference on event manager
        this.eventManager = network.getEventManagerImpl();

        // log exiting
        this.logger.exiting(this.getClass().getName(), "ServerApplicationImpl");
    }

    public synchronized void startListening() {

        // log entering
        this.logger.entering(this.getClass().getName(), "listen");

        // check if we are already listening
        if (this.serverThread != null) {
            IllegalStateException e = new IllegalStateException(
                    "We are already listening!");
            this.logger.throwing(this.getClass().getName(), "listen", e);
            throw e;
        }

        // create a thread that listens in the background
        this.serverThread = new ListenThread();
        this.serverThread.setName("Reply Thread");
        this.serverThread.setDaemon(true);
        this.serverThread.start();

        // log exiting
        this.logger.exiting(this.getClass().getName(), "listen");
    }

    public synchronized void stopListening() {

        // log entering
        this.logger.entering(this.getClass().getName(), "stopListening");

        // check if we are listening
        if (this.serverThread == null) {
            IllegalStateException e = new IllegalStateException(
                    "We are not listening!");
            this.logger.throwing(this.getClass().getName(), "stopListening", e);
            throw e;
        }

        // log this event
        this.logger.log(Level.FINE, "Shutting down server");

        // interrupt thread
        this.serverThread.interrupt();

        // unset listening thread
        this.serverThread = null;

        // log exiting
        this.logger.exiting(this.getClass().getName(), "stopListening");
    }

    public synchronized boolean isListening() {
        return (this.serverThread != null);
    }

    @Override
    public String toString() {
        return this.getClass().getSimpleName() + ": serverApplicationName=\""
                + this.serverApplicationName + "\", serverPort="
                + this.serverPort;
    }

    public String getServerApplicationName() {
        return this.serverApplicationName;
    }

    public int getServerPort() {
        return this.serverPort;
    }
}
