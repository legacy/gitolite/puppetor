/*
 * Copyright 2007 Karsten Loesing
 * Copyright 2008-2009 Karsten Loesing and Sebastian Hahn
 * See LICENSE for licensing information
 */
package org.torproject.puppetor.impl;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.regex.Pattern;

import org.torproject.puppetor.PuppeTorException;
import org.torproject.puppetor.RouterNode;


/**
 * Implementation of <code>RouterNode</code>.
 * 
 * @author kloesing
 */
@SuppressWarnings("serial")
public class RouterNodeImpl extends ProxyNodeImpl implements RouterNode {

    /**
     * Internal thread class that is used to determine fingerprints in parallel,
     * which can take a few seconds.
     */
    private class FingerprintThread extends Thread {

        @Override
        public void run() {

            // log entering
            logger.entering(this.getClass().getName(), "run");

            // create file reference for temporary config file
            File tempConfigFile = new File(RouterNodeImpl.this.workingDir
                    .getAbsolutePath()
                    + File.separator + "torrc.tmp");

            // compose a modified config file, including a DirServer option with
            // false fingerprint; this is necessary, because otherwise Tor
            // would not accept that this router node might have a private IP
            // address, but connects to the public directory servers
            List<String> copyOfConfig = new ArrayList<String>(
                    RouterNodeImpl.this.configuration);
            String fakeDirServerString = "DirServer "
                    + RouterNodeImpl.this.nodeName + " 127.0.0.1:"
                    + RouterNodeImpl.this.dirPort
                    + " 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000";
            copyOfConfig.add(fakeDirServerString);

            // write config file
            try {
                BufferedWriter bw = new BufferedWriter(new FileWriter(
                        tempConfigFile));
                for (String c : copyOfConfig) {
                    bw.write(c + "\n");
                }
                bw.close();
            } catch (IOException e) {
                PuppeTorException ex = new PuppeTorException(
                        "Could not write configuration file!", e);
                logger.log(Level.WARNING, "Could not start Tor process!", ex);
                RouterNodeImpl.this.setCaughtException(ex);
                return;
            }

            // start process with option --list-fingerprint
            ProcessBuilder processBuilder = new ProcessBuilder(torExecutable
                    .getPath(), "--list-fingerprint", "-f", "torrc.tmp");
            processBuilder.directory(RouterNodeImpl.this.workingDir);
            processBuilder.redirectErrorStream(true);
            Process tmpProcess = null;
            try {
                tmpProcess = processBuilder.start();
            } catch (IOException e) {
                PuppeTorException ex = new PuppeTorException(
                        "Could not start Tor process temporarily with "
                                + "--list-fingerprint option!", e);
                logger.log(Level.WARNING, "Could not start Tor process!", ex);
                RouterNodeImpl.this.setCaughtException(ex);
                return;
            }

            // wait for process to terminate
            int exitValue = 0;
            try {
                exitValue = tmpProcess.waitFor();
            } catch (InterruptedException e) {
                PuppeTorException ex = new PuppeTorException(
                        "Interrupted while waiting for Tor process to exit!", e);
                logger.log(Level.WARNING,
                        "Temporary Tor process was interrupted!", ex);
                RouterNodeImpl.this.setCaughtException(ex);
                return;
            }

            if (exitValue != 0) {
                PuppeTorException ex = new PuppeTorException(
                        "Could not start Tor process temporarily with "
                                + "--list-fingerprint option! Tor exited with "
                                + "exit value " + exitValue
                                + "! Please go check the config options in "
                                + tempConfigFile + " manually!");
                logger.log(Level.WARNING, "Could not start Tor process!", ex);
                RouterNodeImpl.this.setCaughtException(ex);
                return;
            }

            // read fingerprint from file
            File fingerprintFile = new File(RouterNodeImpl.this.workingDir
                    .getAbsolutePath()
                    + File.separator + "fingerprint");
            try {
                BufferedReader br2 = new BufferedReader(new FileReader(
                        fingerprintFile));
                RouterNodeImpl.this.setFingerprint(br2.readLine());
                br2.close();
            } catch (IOException e) {
                PuppeTorException ex = new PuppeTorException(
                        "Could not read fingerprint from file!", e);
                logger.log(Level.WARNING, "Could not read fingerprint file!",
                        ex);
                RouterNodeImpl.this.setCaughtException(ex);
                return;
            }

            // delete temporary config file
            tempConfigFile.delete();

            // log exiting
            logger.exiting(this.getClass().getName(), "run");
        }
    }

    /**
     * Invoked by the fingerprint thread: sets the determined fingerprint string
     * 
     * @param fingerprint
     *            The determined fingerprint string.
     */
    private synchronized void setFingerprint(String fingerprint) {

        // log entering
        this.logger.entering(this.getClass().getName(), "setFingerprint",
                fingerprint);

        // remember fingerprint and notify all waiting threads
        this.fingerprint = fingerprint;
        this.notifyAll();

        // log exiting
        this.logger.exiting(this.getClass().getName(), "setFingerprint");
    }

    /**
     * Invoked by the fingerprint thread: sets the exception that occurred when
     * trying to determine the fingerprint.
     * 
     * @param caughtException
     *            The exception that occurred when trying to determine the
     *            fingerprint.
     */
    protected synchronized void setCaughtException(
            PuppeTorException caughtException) {

        // log entering
        this.logger.entering(this.getClass().getName(), "setCaughtException",
                caughtException);

        // remember caught exception and notify all waiting threads
        this.caughtException = caughtException;
        this.notifyAll();

        // log exiting
        this.logger.exiting(this.getClass().getName(), "setCaughtException");
    }

    /**
     * Port on which the Tor node will be listening for directory requests from
     * other Tor nodes.
     */
    protected int dirPort;

    /**
     * The IP v4 address on which the node will listen in dotted decimal
     * notation.
     */
    protected String serverIpAddress;

    /**
     * The fingerprint of this node that is determined as hash value of its
     * onion key. It is initialized with <code>null</code> and set by the
     * fingerprint thread as soon as it is determined.
     */
    private String fingerprint;

    /**
     * The exception that was caught when determining the fingerprint of this
     * node, if any.
     */
    protected PuppeTorException caughtException;

    /**
     * The pattern for valid IP v4 addresses in dotted decimal notation.
     */
    private static final Pattern validIpAddressPattern = Pattern
            .compile("([0-9]|[1-9][0-9]|1[0-9][0-9]|2[0-4][0-9]|25[0-5])"
                    + "(\\.([0-9]|[1-9][0-9]|1[0-9][0-9]|2[0-4][0-9]|25[0-5])){1,3}");

    /**
     * Port on which the Tor node will be listening for onion requests by other
     * Tor nodes.
     */
    protected int orPort;

    /**
     * Creates a new <code>RouterNodeImpl</code> and adds it to the given
     * <code>network</code>, but does not yet write its configuration to disk
     * or start the corresponding Tor process.
     * 
     * @param network
     *            Network configuration to which this node belongs.
     * @param nodeName
     *            The name of the new node which may only consist of between 1
     *            and 19 alpha-numeric characters.
     * @param controlPort
     *            Port on which the Tor node will be listening for us as its
     *            controller. May not be negative or greater than 65535.
     * @param socksPort
     *            Port on which the Tor node will be listening for SOCKS
     *            connection requests. May not be negative or greater than
     *            65535.
     * @param orPort
     *            Port on which the Tor node will be listening for onion
     *            requests by other Tor nodes. May not be negative or greater
     *            than 65535.
     * @param dirPort
     *            Port on which the Tor node will be listening for directory
     *            requests from other Tor nodes. May not be negative or greater
     *            than 65535.
     * @param serverIpAddress
     *            The IP address on which the node will listen. Must be a valid
     *            IP v4 address in dotted decimal notation. May not be
     *            <code>null</code>.
     * @throws IllegalArgumentException
     *             If at least one of the parameters is <code>null</code> or
     *             has an invalid value.
     * @throws RemoteException
     *             Thrown if an error occurs when accessed remotely.
     */
    RouterNodeImpl(NetworkImpl network, String nodeName, int controlPort,
            int socksPort, int orPort, int dirPort, String serverIpAddress)
            throws RemoteException {

        // create superclass instance; parameter checking is done in super
        // constructor
        super(network, nodeName, controlPort, socksPort);

        // log entering
        this.logger.entering(this.getClass().getName(), "RouterNodeImpl",
                new Object[] { network, nodeName, controlPort, socksPort,
                        orPort, dirPort, serverIpAddress });

        // check parameters
        if (orPort < 0 || orPort > 65535 || dirPort < 0 || dirPort > 65535
                || serverIpAddress == null
                || !validIpAddressPattern.matcher(serverIpAddress).matches()) {
            IllegalArgumentException e = new IllegalArgumentException(
                    "nodeName=" + nodeName + ", controlPort=" + controlPort
                            + ", socksPort=" + socksPort + ", orPort="
                            + orPort + ", dirPort=" + dirPort
                            + ", serverIpAddress='" + serverIpAddress + "'");
            this.logger
                    .throwing(this.getClass().getName(), "RouterNodeImpl", e);
            throw e;
        }

        // remember parameters
        this.orPort = orPort;
        this.dirPort = dirPort;
        this.serverIpAddress = serverIpAddress;

        // extend configuration by template configuration of router nodes
        this.configuration.addAll(templateConfiguration);

        // add further configuration to make this node a router node
        this.configuration.add("ORPort " + orPort);
        this.configuration.add("Nickname " + nodeName);

        // all routers mirror the directory
        this.configuration.add("DirPort " + dirPort);

        // the address of this node should be manually specified and not guessed
        // by Tor
        this.configuration.add("Address " + serverIpAddress);

        // the OR port may only be contacted locally
        this.configuration.add("ORListenAddress " + serverIpAddress);

        // offer directory only locally (either by being an authority, or by
        // mirroring it)
        this.configuration.add("DirListenAddress " + serverIpAddress);

        // start a thread to determine the node's fingerprint in the background
        this.determineFingerprint();

        // log exiting
        this.logger.exiting(this.getClass().getName(), "RouterNodeImpl");
    }

    public synchronized String getFingerprint() throws PuppeTorException,
            RemoteException {

        // log entering
        this.logger.entering(this.getClass().getName(), "getFingerprint");

        // wait until either the fingerprint has been determined or an exception
        // was caught
        while (this.fingerprint == null && this.caughtException == null) {
            try {
                wait();
            } catch (InterruptedException e) {
                // do nothing
            }
        }

        if (this.caughtException != null) {
            this.logger.throwing(this.getClass().getName(), "getFingerprint",
                    this.caughtException);
            throw this.caughtException;
        }

        // log exiting
        this.logger.exiting(this.getClass().getName(), "getFingerprint",
                this.fingerprint);
        return this.fingerprint;
    }

    @Override
    public String toString() {
        return super.toString() + ", orPort=" + this.orPort + ", dirPort="
                + this.dirPort;
    }

    public int getDirPort() {
        return this.dirPort;
    }

    public int getOrPort() {
        return this.orPort;
    }

    /**
     * Determines the fingerprint of this node by starting a background thread
     * that performs this operation.
     */
    protected synchronized void determineFingerprint() {

        // log entering
        this.logger.entering(this.getClass().getName(), "determineFingerprint");

        // start a thread to determine this node's fingerprint
        FingerprintThread fingerprintThread = new FingerprintThread();
        fingerprintThread.setName(nodeName + " Fingerprint Resolver");
        fingerprintThread.start();

        // log exiting
        this.logger.exiting(this.getClass().getName(), "determineFingerprint");
    }

    /**
     * Template configuration of router nodes.
     */
    static List<String> templateConfiguration;

    static {
        templateConfiguration = new ArrayList<String>();

        templateConfiguration.add("ContactInfo wont@reply.org");
        templateConfiguration.add("HidServDirectoryV2 1");
    }
}
