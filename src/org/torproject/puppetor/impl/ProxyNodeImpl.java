/*
 * Copyright 2007 Karsten Loesing
 * Copyright 2008-2009 Karsten Loesing and Sebastian Hahn
 * See LICENSE for licensing information
 */
package org.torproject.puppetor.impl;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.torproject.puppetor.HiddenService;
import org.torproject.puppetor.NodeEventType;
import org.torproject.puppetor.NodeState;
import org.torproject.puppetor.ProxyNode;
import org.torproject.puppetor.PuppeTorException;

import net.freehaven.tor.control.TorControlConnection;

/**
 * Implementation of <code>ProxyNode</code>.
 * 
 * @author kloesing
 */
@SuppressWarnings("serial")
public class ProxyNodeImpl extends UnicastRemoteObject implements ProxyNode {

    /**
     * Executable file containing Tor.
     */
    protected File torExecutable = new File("tor");

    /**
     * The <code>torrc</code> configuration file of this Tor node.
     */
    protected File configFile;

    /**
     * Collects all configuration strings for this node during the configuration
     * phase in the order they are added.
     */
    protected List<String> configuration;

    /**
     * Connection via Tor controller.
     */
    protected TorControlConnection conn;

    /**
     * Port on which the Tor node will be listening for us as its controller.
     */
    protected int controlPort;

    /**
     * Event manager to which all events concerning this node are notified.
     */
    private EventManagerImpl eventManager;

    /**
     * Logger for this node which is called "node." plus the name of this node.
     */
    protected Logger logger;

    /**
     * Network to which this node belongs.
     */
    protected NetworkImpl network;

    /**
     * Name of this node that is used as part of the working directory, as
     * logger name of this node, and as event source.
     */
    protected String nodeName;

    /**
     * The state of this node.
     */
    protected NodeState nodeState = NodeState.CONFIGURING;

    /**
     * Port on which the Tor node will be listening for SOCKS connection
     * requests.
     */
    protected int socksPort;

    /**
     * The running Tor process that belongs to this node.
     */
    protected Process torProcess;

    /**
     * Directory in which all information concerning this node is stored.
     */
    protected File workingDir;

    /**
     * Returns this node's working directory.
     * 
     * @return This node's working directory.
     */
    File getWorkingDir() {
        return this.workingDir;
    }

    /**
     * Creates a new <code>ProxyNodeImpl</code> and adds it to the given
     * <code>network</code>, but does not yet write its configuration to disk
     * or start the corresponding Tor process.
     * 
     * @param network
     *            Network configuration to which this node belongs.
     * @param nodeName
     *            The name of the new node which may only consist of between 1
     *            and 19 alpha-numeric characters.
     * @param controlPort
     *            Port on which the Tor node will be listening for us as its
     *            controller. May not be negative or greater than 65535.
     * @param socksPort
     *            Port on which the Tor node will be listening for SOCKS
     *            connection requests. May not be negative or greater than
     *            65535.
     * @throws IllegalArgumentException
     *             If at least one of the parameters is <code>null</code> or
     *             has an invalid value.
     * @throws RemoteException
     *             Thrown if an error occurs when accessed remotely.
     */
    ProxyNodeImpl(NetworkImpl network, String nodeName, int controlPort,
            int socksPort) throws RemoteException {

        // make sure that nodeName is a valid logger name
        if (nodeName == null || nodeName.length() < 1 || nodeName.length() > 19
                || !nodeName.matches("[a-zA-Z0-9]*")) {
            String reason = "\"" + nodeName + "\" is not a valid node name!";
            IllegalArgumentException e = new IllegalArgumentException(reason);
            throw e;
        }

        // create logger
        this.logger = Logger.getLogger(nodeName + "."
                + this.getClass().getName());

        this.logger.setLevel(Level.ALL);

        // log entering
        this.logger.entering(this.getClass().getName(), "ProxyNodeImpl",
                new Object[] { network, nodeName, controlPort, socksPort });

        // check remaining parameters
        if (network == null || controlPort < 0 || controlPort > 65535
                || socksPort < 0 || socksPort > 65535) {
            IllegalArgumentException e = new IllegalArgumentException();
            this.logger.throwing(this.getClass().getName(), "ProxyNodeImpl", e);
            throw e;
        }

        // store parameter values
        this.network = network;
        this.nodeName = nodeName;
        this.controlPort = controlPort;
        this.socksPort = socksPort;

        // obtain reference on event manager from network
        this.eventManager = network.getEventManagerImpl();

        // create working directory
        this.workingDir = new File(this.network.getWorkingDirectory()
                .getAbsolutePath()
                + File.separator + nodeName + File.separator);
        this.workingDir.mkdirs();
        this.logger.log(Level.FINE, "Created working directory \""
                + this.workingDir.getAbsolutePath() + "\"");

        // create reference on config file
        this.configFile = new File(this.workingDir.getAbsolutePath()
                + File.separator + "torrc");

        // initialize configuration
        this.configuration = new ArrayList<String>(templateConfiguration);
        this.configuration.add("ControlPort " + controlPort);
        this.configuration.add("SocksPort " + socksPort);

        // initialize state
        this.nodeState = NodeState.CONFIGURING;

        // log exiting
        this.logger.exiting(this.getClass().getName(), "ProxyNodeImpl");
    }

    public void addConfiguration(String configurationString) {

        // log entering
        this.logger.entering(this.getClass().getName(), "addConfiguration",
                configurationString);

        // check parameter
        if (configurationString == null || configurationString.length() < 1
                || !configurationString.contains(" ")) {
            IllegalArgumentException e = new IllegalArgumentException();
            this.logger.throwing(this.getClass().getName(), "addConfiguration",
                    e);
            throw e;
        }

        // add configuration string
        this.configuration.add(configurationString);

        // log exiting
        this.logger.exiting(this.getClass().getName(), "addConfiguration");
    }

    public void addConfigurations(List<String> configurationStrings) {

        // log entering
        this.logger.entering(this.getClass().getName(), "addConfigurations",
                configurationStrings);

        // check parameter
        if (configurationStrings == null) {
            IllegalArgumentException e = new IllegalArgumentException();
            this.logger.throwing(this.getClass().getName(),
                    "addConfigurations", e);
            throw e;
        }

        // add configuration strings one by one
        for (String conf : configurationStrings) {
            this.addConfiguration(conf);
        }

        // log exiting
        this.logger.exiting(this.getClass().getName(), "addConfigurations");
    }

    public void replaceConfiguration(String configurationString) {

        // log entering
        this.logger.entering(this.getClass().getName(), "replaceConfiguration",
                configurationString);

        // check parameter
        if (configurationString == null || configurationString.length() < 1
                || !configurationString.contains(" ")) {
            IllegalArgumentException e = new IllegalArgumentException();
            this.logger.throwing(this.getClass().getName(),
                    "replaceConfiguration", e);
            throw e;
        }

        // extract configuration key
        String configurationKey = configurationString.substring(0,
                configurationString.indexOf(" "));

        // iterate over existing configuration strings and replace the first
        // occurrence of configuration key with new configuration string
        Iterator<String> it = this.configuration.listIterator();
        boolean replaced = false;
        for (int counter = 0; !replaced && it.hasNext(); counter++) {
            String currentConfigurationString = it.next();
            String currentConfigurationKey = currentConfigurationString
                    .substring(0, currentConfigurationString.indexOf(" "));
            if (currentConfigurationKey.equals(configurationKey)) {
                this.configuration.set(counter, configurationString);
                replaced = true;
            }
        }

        // if no such configuration key was found, append the configuration
        // string
        if (!replaced) {
            this.configuration.add(configurationString);
        }

        // log exiting
        this.logger.exiting(this.getClass().getName(), "replaceConfiguration");
    }

    public void removeConfiguration(String configurationKey)
            throws RemoteException {

        // log entering
        this.logger.entering(this.getClass().getName(), "deleteConfiguration",
                configurationKey);

        // check parameter
        if (configurationKey == null || configurationKey.length() < 1) {
            IllegalArgumentException e = new IllegalArgumentException();
            this.logger.throwing(this.getClass().getName(),
                    "deleteConfiguration", e);
            throw e;
        }

        // iterate over existing configuration strings and remove all
        // configuration strings that have the given configuration key
        List<String> configurationStringsToRemove = new ArrayList<String>();
        for (String currentConfigurationString : this.configuration) {
            String currentConfigurationKey = currentConfigurationString
                    .substring(0, currentConfigurationString.indexOf(" "));
            if (currentConfigurationKey.equals(configurationKey)) {
                configurationStringsToRemove.add(currentConfigurationString);
            }
        }
        this.configuration.removeAll(configurationStringsToRemove);

        // log exiting
        this.logger.exiting(this.getClass().getName(), "deleteConfiguration");
    }

    public synchronized HiddenService addHiddenService(String serviceName,
            int servicePort, int virtualPort) throws RemoteException {

        // log entering
        this.logger.entering(this.getClass().getName(), "addHiddenService",
                new Object[] { serviceName, servicePort, virtualPort });

        // create hidden service object; parameter checking is done in
        // constructor
        HiddenService result = new HiddenServiceImpl(this, serviceName,
                servicePort, virtualPort);

        // add hidden service using Tor controller
        this.configuration.add("HiddenServiceDir "
                + workingDir.getAbsolutePath() + File.separator + serviceName
                + "\nHiddenServicePort " + virtualPort + " 127.0.0.1:"
                + servicePort);

        // log exiting and return hidden service object
        this.logger.exiting(this.getClass().getName(), "addHiddenService",
                result);
        return result;
    }

    public synchronized HiddenService addHiddenService(String serviceName,
            int servicePort) throws RemoteException {

        // log entering
        this.logger.entering(this.getClass().getName(), "addHiddenService",
                new Object[] { serviceName, servicePort });

        // invoke overloaded method
        HiddenService result = this.addHiddenService(serviceName, servicePort,
                80);

        // log exiting and return hidden service
        this.logger.exiting(this.getClass().getName(), "addHiddenService",
                result);
        return result;
    }

    public synchronized HiddenService addHiddenService(String serviceName)
            throws RemoteException {

        // log entering
        this.logger.entering(this.getClass().getName(), "addHiddenService",
                serviceName);

        // invoke overloaded method
        HiddenService result = this.addHiddenService(serviceName, this.network
                .getNextPortNumber(), 80);

        // log exiting and return hidden service
        this.logger.exiting(this.getClass().getName(), "addHiddenService",
                result);
        return result;
    }

    public String getNodeName() {
        return this.nodeName;
    }

    public synchronized NodeState getNodeState() {
        return this.nodeState;
    }

    public synchronized void hup() throws PuppeTorException {

        // log entering
        this.logger.entering(this.getClass().getName(), "hup");

        // check state
        if (this.nodeState != NodeState.RUNNING || this.conn == null) {
            IllegalStateException e = new IllegalStateException(
                    "Cannot hup a process when it's not running or there is "
                            + "no connection to its control port!");
            this.logger.throwing(this.getClass().getName(), "hup", e);
            throw e;
        }

        // send HUP signal to Tor process
        try {
            this.conn.signal("HUP");
        } catch (IOException e) {
            PuppeTorException ex = new PuppeTorException(
                    "Could not send the command HUP to the Tor process!", e);
            this.logger.throwing(this.getClass().getName(), "hup", ex);
            throw ex;
        } catch (NullPointerException e) {
            // TODO sometimes, this throws a NullPointerException...
            this.logger.log(Level.SEVERE, "is conn null? "
                    + (this.conn == null));
            this.logger.log(Level.SEVERE,
                    "NullPointerException while sending HUP signal to node "
                            + this.toString());
            throw e;
        }

        // log exiting
        this.logger.exiting(this.getClass().getName(), "hup");
    }

    public synchronized void shutdown() throws PuppeTorException {

        // log entering
        this.logger.entering(this.getClass().getName(), "shutdown");

        // check state
        if (this.nodeState != NodeState.RUNNING) {
            IllegalStateException e = new IllegalStateException();
            this.logger.throwing(this.getClass().getName(), "shutdown", e);
            throw e;
        }

        // we cannot simply kill the Tor process, because we have established a
        // controller connection to it which would interpret a closed socket as
        // failure and throw a RuntimeException
        try {
            this.conn.shutdownTor("SHUTDOWN");
            this.conn.shutdownTor("SHUTDOWN");
        } catch (IOException e) {
            PuppeTorException ex = new PuppeTorException(
                    "Could not send shutdown command to Tor process!", e);
            this.logger.throwing(this.getClass().getName(), "shutdown", ex);
            throw ex;
        }

        // change state
        this.nodeState = NodeState.SHUT_DOWN;

        // fire event
        eventManager.observeInternalEvent(System.currentTimeMillis(), this
                .getNodeName(), NodeEventType.NODE_STOPPED, "Node stopped.");

        // log exiting
        this.logger.exiting(this.getClass().getName(), "shutdown");
    }

    /**
     * Helper thread that waits for a given time for a given process to
     * potentially terminate in order to find out if there are problems. If
     * either the process terminates cleanly within this timeout, or does not
     * terminate, the exit value will be 0; otherwise it will contain the exit
     * code of the terminated process. This functionality is added, because it
     * is not provided by Process.
     * This class is not threadsafe.
     * XXX Some stuff in here looks still dodgy. What happens if we get an
     * InterruptedException in run and thus don't set exitValue?-SH
     */
    private class ProcessWaiter extends Thread {

        /** The process to wait for. */
        private Process process;

        /** The exit value or 0 if the process is still running. */
        private int exitValue;

        /**
         * Creates a new <code>ProcessWaiter</code> for process
         * <code>process</code>, but does not start it, yet.
         * 
         * @param process
         *            The process to wait for.
         */
        ProcessWaiter(Process process) {
            this.process = process;
        }

        @Override
        public void run() {
            try {
                this.exitValue = process.waitFor();
            } catch (InterruptedException e) {
            }
        }

        /**
         * Causes the current thread to wait until the process has terminated or
         * the <code>timeoutInMillis</code> has expired. This method returns
         * immediately if the subprocess has already terminated.
         * 
         * @param timeoutInMillis
         *            The maximum time to wait for the process to terminate.
         * @return The exit value of the terminated process or 0 if the process
         *         is still running.
         */
        public synchronized int waitFor(long timeoutInMillis) {
            try {
                sleep(timeoutInMillis);
            } catch (InterruptedException e) {
            }
            this.interrupt();
            return this.exitValue;
        }
    }

    public synchronized boolean startNode(long maximumTimeToWaitInMillis)
            throws PuppeTorException {

        // log entering
        this.logger.entering(this.getClass().getName(), "startNode",
                maximumTimeToWaitInMillis);

        // check state
        if (this.nodeState != NodeState.CONFIGURATION_WRITTEN) {
            String reason = "Node is not in state "
                    + "NodeState.CONFIGURATION_WRITTEN!";
            IllegalStateException e = new IllegalStateException(reason);
            this.logger.throwing(this.getClass().getName(), "startNode", e);
            throw e;
        }

        // start process
        ProcessBuilder processBuilder = new ProcessBuilder(torExecutable
                .getPath(), "-f", "torrc");
        processBuilder.directory(this.workingDir);
        processBuilder.redirectErrorStream(true);
        try {
            this.torProcess = processBuilder.start();
            this.logger.log(Level.FINE, "Started Tor process successfully!");
        } catch (IOException e) {
            String reason = "Could not start Tor process!";
            PuppeTorException ex = new PuppeTorException(reason, e);
            this.logger.throwing(this.getClass().getName(), "startNode", ex);
            throw ex;
        }

        // start thread to parse output
        final BufferedReader br = new BufferedReader(new InputStreamReader(
                this.torProcess.getInputStream()));
        Thread outputThread = new Thread() {
            @Override
            public void run() {

                // log entering
                logger.entering(this.getClass().getName(), "run");

                // read output from Tor to parse it
                String line = null;
                try {
                    while ((line = br.readLine()) != null) {
                        eventManager.observeUnparsedEvent(ProxyNodeImpl.this
                                .getNodeName(), line);
                    }
                } catch (IOException e) {

                    // only print out a warning for this exception if this node
                    // is running; otherwise, silently ignore it...
                    if (nodeState == NodeState.RUNNING) {
                        String reason = "IOException when reading output from Tor "
                                + "process "
                                + ProxyNodeImpl.this.getNodeName()
                                + "!";
                        logger.log(Level.WARNING, reason, e);
                    }
                }

                // log exiting
                logger.exiting(this.getClass().getName(), "run");
            }
        };
        outputThread.setDaemon(true);
        outputThread.setName(this.nodeName + " Output Parser");
        outputThread.start();
        this.logger.log(Level.FINE, "Started thread to parse output!");

        // add shutdown hook that kills the process on JVM exit
        final Process p = this.torProcess;
        Runtime.getRuntime().addShutdownHook(new Thread() {
            @Override
            public void run() {

                // log entering
                logger.entering(this.getClass().getName(), "run");

                // destroy Tor process
                p.destroy();

                // log exiting
                logger.exiting(this.getClass().getName(), "run");
            }
        });
        this.logger.log(Level.FINER,
                "Started shutdown hook that will destroy the Tor process on "
                        + "JVM exit!");

        // wait to see if the process is started or exited immediately; wait for
        // one second to be sure that Tor terminates if there is an error,
        // especially if the computer is very busy and many nodes are created
        ProcessWaiter waiter = new ProcessWaiter(this.torProcess);
        waiter.start();
        int exitValue = waiter.waitFor(1000);
        if (exitValue != 0) {
            // Tor did not manage to start correctly
            this.logger.log(Level.WARNING, "Could not start Tor process! Tor "
                    + "exited with exit value " + exitValue
                    + "! Please go check the config options in "
                    + this.configFile + " manually!");

            // log exiting
            this.logger.exiting(this.getClass().getName(), "startNode", false);
            return false;
        }

        // wait for Tor to open the control port
        this.logger.log(Level.FINER,
                "Waiting for Tor to open its control port...");
        if (!this.eventManager.waitForAnyOccurence(this.nodeName,
                NodeEventType.NODE_CONTROL_PORT_OPENED,
                maximumTimeToWaitInMillis)) {

            // Tor did not open its control port
            this.logger.log(Level.WARNING, "Tor node " + this.nodeName
                    + " did not manage to open its control port within "
                    + maximumTimeToWaitInMillis + " milliseconds!");

            // log exiting
            this.logger.exiting(this.getClass().getName(), "startNode", false);
            return false;
        }
        this.logger.log(Level.FINE,
                "Tor has successfully opened its control port and told us "
                        + "about that!");

        // connect to the controller
        this.logger.log(Level.FINER, "Connecting to control port...");
        try {
            Socket controlSocket = new java.net.Socket("127.0.0.1", controlPort);
            this.conn = TorControlConnection.getConnection(controlSocket);
            this.conn.authenticate(new byte[0]);
        } catch (IOException e) {
            String reason = "Could not connect to control port " + controlPort
                    + "!";
            PuppeTorException ex = new PuppeTorException(reason, e);
            this.logger.throwing(this.getClass().getName(), "startNode", ex);
            throw ex;
        }
        this.logger.log(Level.FINE, "Connected to control port successfully!");

        // set state to RUNNING
        this.nodeState = NodeState.RUNNING;

        // fire event
        eventManager.observeInternalEvent(System.currentTimeMillis(), this
                .getNodeName(), NodeEventType.NODE_STARTED, "Node started.");

        // log exiting and return with success
        this.logger.exiting(this.getClass().getName(), "startNode", true);
        return true;
    }

    @Override
    public String toString() {
        return this.getClass().getSimpleName() + ": nodeName=\""
                + this.nodeName + "\", controlPort=" + this.controlPort
                + ", socksPort=" + this.socksPort;
    }

    public synchronized void writeConfiguration() throws PuppeTorException {

        // log entering
        this.logger.entering(this.getClass().getName(), "writeConfiguration");

        // write config file
        try {
            BufferedWriter bw = new BufferedWriter(new FileWriter(
                    this.configFile));
            for (String c : this.configuration) {
                bw.write(c + "\n");
            }
            bw.close();
        } catch (IOException e) {
            PuppeTorException ex = new PuppeTorException(
                    "Could not write configuration file!", e);
            this.logger.throwing(this.getClass().getName(),
                    "writeConfigurationFile", ex);
            throw ex;
        }

        // change state, if necessary
        if (this.nodeState == NodeState.CONFIGURING) {
            this.nodeState = NodeState.CONFIGURATION_WRITTEN;
        }

        // log exiting
        this.logger.exiting(this.getClass().getName(), "writeConfiguration");
    }

    public int getSocksPort() {
        return this.socksPort;
    }

    public int getControlPort() {
        return this.controlPort;
    }

    public List<String> getConfiguration() {
        return new ArrayList<String>(this.configuration);
    }

    public void setTorExecutable(File executable) {

        // log entering
        this.logger.entering(this.getClass().getName(), "setTorExecutable");

        // check state
        if (this.nodeState != NodeState.CONFIGURING) {
            IllegalStateException e = new IllegalStateException(
                    "Cannot change the tor executable after configuring!");
            this.logger.throwing(this.getClass().getName(), "setTorExecutable",
                    e);
            throw e;
        }

        // check if the file exists and is not a directory
        if (executable == null || !executable.exists() ||
                executable.isDirectory()) {
            IllegalArgumentException e = new IllegalArgumentException(
                    "tor executable " + (executable == null ? "(null)"
                    : executable.getAbsolutePath()) + " is not a valid "
                    + "file!");
            this.logger.throwing(this.getClass().getName(), "setTorExecutable",
                    e);
            throw e;
        }

        // set executable
        this.torExecutable = executable;

        // log exiting
        this.logger.exiting(this.getClass().getName(), "setTorExecutable");
    }

    /**
     * Template configuration of proxy nodes.
     */
    static List<String> templateConfiguration;

    static {
        templateConfiguration = new ArrayList<String>();

        templateConfiguration.add("DataDirectory .");
        templateConfiguration.add("SafeLogging 0");
        templateConfiguration.add("UseEntryGuards 0");

        templateConfiguration.add("Log info stdout");
        templateConfiguration.add("Log info file log");

    }
}
