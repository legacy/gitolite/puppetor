/*
 * Copyright 2007 Karsten Loesing
 * Copyright 2008-2009 Karsten Loesing and Sebastian Hahn
 * See LICENSE for licensing information
 */
package org.torproject.puppetor.impl;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;
import java.util.logging.Level;

import org.torproject.puppetor.DirectoryNode;
import org.torproject.puppetor.PuppeTorException;


/**
 * Implementation of <code>DirectoryNode</code>.
 * 
 * @author kloesing
 */
@SuppressWarnings("serial")
public class DirectoryNodeImpl extends RouterNodeImpl implements DirectoryNode {

    /**
     * Executable file for generating v3 directory authority certificates.
     * 
     * TODO make this configurable!
     */
    protected static final File torGencertExecutable = new File(
            "tor-gencert");

    /**
     * Internal thread class that is used to generate v3 directory authority
     * certificates in parallel, which can take a few seconds.
     */
    public class GenerateCertificateThread extends Thread {

        @Override
        public void run() {

            // log entering
            logger.entering(this.getClass().getName(), "run");

            // run tor-gencert
            ProcessBuilder processBuilder = new ProcessBuilder(
                    torGencertExecutable.getPath(), "--create-identity-key"// );
                    , "--passphrase-fd", "0");
            File workingDirectory = new File(DirectoryNodeImpl.this.workingDir
                    .getAbsolutePath()
                    + File.separator + "keys" + File.separator);

            // create working directory
            workingDirectory.mkdirs();

            processBuilder.directory(workingDirectory);
            processBuilder.redirectErrorStream(true);
            Process tmpProcess = null;
            try {
                tmpProcess = processBuilder.start();
            } catch (IOException e) {
                PuppeTorException ex = new PuppeTorException(
                        "Could not start tor-gencert process for generating a "
                                + "v3 directory authority certificate!", e);
                logger.log(Level.WARNING, "Could not start tor-gencert "
                        + "process for generating a v3 directory authority "
                        + "certificate!", ex);
                DirectoryNodeImpl.this.setCaughtException(ex);
                return;
            }

            BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(
                    tmpProcess.getOutputStream()));
            try {
                writer.write("somepassword\n");
                writer.close();
            } catch (IOException e1) {
                System.out.println("Exception at write! " + e1.getMessage());
                e1.printStackTrace();
            }

            InputStream stderr = tmpProcess.getInputStream();
            InputStreamReader isr = new InputStreamReader(stderr);
            BufferedReader br = new BufferedReader(isr);
            String line = null;
            try {
                while ((line = br.readLine()) != null)
                    ;
            } catch (IOException e1) {
                e1.printStackTrace();
            }

            // wait for process to terminate
            int exitValue = 0;
            try {
                exitValue = tmpProcess.waitFor();
            } catch (InterruptedException e) {
                PuppeTorException ex = new PuppeTorException(
                        "Interrupted while waiting for tor-gencert process to exit!",
                        e);
                logger.log(Level.WARNING,
                        "tor-gencert process was interrupted!", ex);
                DirectoryNodeImpl.this.setCaughtException(ex);
                return;
            }

            if (exitValue != 0) {
                PuppeTorException ex = new PuppeTorException(
                        "Could not start tor-gencert process! tor-gencert exited with "
                                + "exit value " + exitValue + "!");
                logger.log(Level.WARNING,
                        "Could not start tor-gencert process!", ex);
                DirectoryNodeImpl.this.setCaughtException(ex);
                return;
            }

            // read fingerprint from file
            File authorityCertificateFile = new File(workingDirectory
                    .getAbsolutePath()
                    + File.separator + "authority_certificate");
            String identity;
            try {
                BufferedReader br2 = new BufferedReader(new FileReader(
                        authorityCertificateFile));
                while ((line = br2.readLine()) != null
                        && !line.startsWith("fingerprint "))
                    ;
                if (line == null) {
                    PuppeTorException ex = new PuppeTorException(
                            "Could not find fingerprint line in file "
                                    + "authority_certificate!");
                    logger.log(Level.WARNING,
                            "Could not find fingerprint line in file "
                                    + "authority_certificate!", ex);
                    DirectoryNodeImpl.this.setCaughtException(ex);
                    return;
                }
                identity = line.substring(line.indexOf(" ") + 1);
                br2.close();
            } catch (IOException e) {
                PuppeTorException ex = new PuppeTorException(
                        "Could not read fingerprint from file!", e);
                logger.log(Level.WARNING, "Could not read fingerprint file!",
                        ex);
                DirectoryNodeImpl.this.setCaughtException(ex);
                return;
            }

            DirectoryNodeImpl.this.setV3Identity(identity);

            // log exiting
            logger.exiting(this.getClass().getName(), "run");
        }
    }

    /**
     * Set of routers that are approved by this directory node.
     */
    private SortedSet<String> approvedRouters;

    /**
     * Creates a <code>DirectoryNodeImpl</code> and adds it to the given
     * <code>network</code>, but does not yet write its configuration to disk
     * or start the corresponding Tor process.
     * 
     * @param network
     *            Network configuration to which this node belongs.
     * @param nodeName
     *            The name of the new node which may only consist of between 1
     *            and 19 alpha-numeric characters.
     * @param controlPort
     *            Port on which the Tor node will be listening for us as its
     *            controller. May not be negative or greater than 65535.
     * @param socksPort
     *            Port on which the Tor node will be listening for SOCKS
     *            connection requests. May not be negative or greater than
     *            65535.
     * @param orPort
     *            Port on which the Tor node will be listening for onion
     *            requests by other Tor nodes. May not be negative or greater
     *            than 65535.
     * @param dirPort
     *            Port on which the Tor node will be listening for directory
     *            requests from other Tor nodes. May not be negative or greater
     *            than 65535.
     * @param serverIpAddress
     *            The IP address on which the node will listen. Must be a valid
     *            IP v4 address in dotted decimal notation. May not be
     *            <code>null</code>.
     * @throws IllegalArgumentException
     *             If at least one of the parameters is <code>null</code> or
     *             has an invalid value.
     * @throws RemoteException
     *             Thrown if an error occurs when accessed remotely.
     * @throws PuppeTorException
     *             Thrown if an I/O problem occurs while writing the temporary
     *             <code>approved-routers</code> file.
     */
    DirectoryNodeImpl(NetworkImpl network, String nodeName, int controlPort,
            int socksPort, int orPort, int dirPort, String serverIpAddress)
            throws RemoteException {

        // create superclass instance; parameter checking is done in super
        // constructor
        super(network, nodeName, controlPort, socksPort, orPort, dirPort,
                serverIpAddress);

        // log entering
        this.logger.entering(this.getClass().getName(), "DirectoryNodeImpl",
                new Object[] { network, nodeName, controlPort, socksPort,
                        orPort, dirPort });

        // initialize attribute
        this.approvedRouters = new TreeSet<String>();

        // extend configuration by template configuration of directory nodes
        this.configuration.addAll(templateConfiguration);

        // log exiting
        this.logger.exiting(this.getClass().getName(), "DirectoryNodeImpl");
    }

    /**
     * Invoked by the certificate generating thread: sets the generated v3
     * identity string.
     * 
     * @param v3Identity
     *            The generated v3 identity string.
     */
    private synchronized void setV3Identity(String v3Identity) {
        // log entering
        this.logger.entering(this.getClass().getName(), "setV3Identity",
                v3Identity);

        // remember fingerprint and notify all waiting threads
        this.v3Identity = v3Identity;
        this.notifyAll();

        // log exiting
        this.logger.exiting(this.getClass().getName(), "setV3Identity");
    }

    /**
     * The generated v3 identity string.
     */
    private String v3Identity;

    public synchronized String getV3Identity() throws PuppeTorException {

        // log entering
        this.logger.entering(this.getClass().getName(), "getV3Identity");

        // wait until either the v3 identity has been determined or an exception
        // was caught
        while (this.v3Identity == null && this.caughtException == null) {

            try {
                wait(500);
            } catch (InterruptedException e) {
                // do nothing
            }
        }

        if (this.caughtException != null) {
            this.logger.throwing(this.getClass().getName(), "getV3Identity",
                    this.caughtException);
            throw this.caughtException;
        }

        // log exiting
        this.logger.exiting(this.getClass().getName(), "getV3Identity",
                this.v3Identity);
        return this.v3Identity;
    }

    public synchronized String getDirServerString() throws PuppeTorException,
            RemoteException {

        // log entering
        this.logger.entering(this.getClass().getName(), "getDirServerString");

        // determine fingerprint
        String fingerprint = this.getFingerprint();

        // cut off router nickname
        fingerprint = fingerprint.substring(fingerprint.indexOf(" ") + 1);

        // determine v3 identity
        String determinedV3Identity = this.getV3Identity();

        // put everything together
        String dirServerString = "DirServer " + this.nodeName + " v3ident="
                + determinedV3Identity + " orport=" + this.orPort + " "
                + this.serverIpAddress + ":" + this.dirPort + " " + fingerprint;

        // log exiting and return dir server string
        this.logger.exiting(this.getClass().getName(), "getDirServerString",
                dirServerString);
        return dirServerString;
    }

    public void addApprovedRouters(Set<String> routers) throws RemoteException {

        // log entering
        this.logger.entering(this.getClass().getName(), "addApprovedRouters",
                routers);

        // check parameter
        if (routers == null) {
            IllegalArgumentException e = new IllegalArgumentException();
            this.logger.throwing(this.getClass().getName(),
                    "addApprovedRouters", e);
            throw e;
        }

        // add the given approved router strings to the sorted set of already
        // known strings (if any)
        this.approvedRouters.addAll(routers);

        // log exiting
        this.logger.exiting(this.getClass().getName(), "addApprovedRouters");
    }

    @Override
    protected synchronized void determineFingerprint() {

        // log entering
        this.logger.entering(this.getClass().getName(), "determineFingerprint");

        // start a thread to generate the directory's certificate
        GenerateCertificateThread certificateThread = new GenerateCertificateThread();
        certificateThread.setName(nodeName + " Certificate Generator");
        certificateThread.start();

        // wait (non-blocking) for the v3 identity string
        try {
            this.getV3Identity();
        } catch (PuppeTorException e1) {
            PuppeTorException ex = new PuppeTorException(
                    "Could not read v3 identity string!", e1);
            this.caughtException = ex;
            return;
        }

        // create an empty approved-routers file to make Tor happy
        try {
            new File(this.workingDir.getAbsolutePath() + File.separator
                    + "approved-routers").createNewFile();
        } catch (IOException e) {
            PuppeTorException ex = new PuppeTorException(
                    "Could not write empty approved-routers file!", e);
            this.caughtException = ex;
            return;
        }

        // invoke overwritten method
        super.determineFingerprint();
    }

    @Override
    public synchronized void writeConfiguration() throws PuppeTorException {

        // log entering
        this.logger.entering(this.getClass().getName(), "writeConfiguration");

        // write approved-routers file
        try {
            File approvedRoutersFile = new File(this.workingDir
                    .getAbsolutePath()
                    + File.separator + "approved-routers");
            BufferedWriter bw = new BufferedWriter(new FileWriter(
                    approvedRoutersFile));
            for (String approvedRouter : this.approvedRouters) {
                bw.write(approvedRouter + "\n");
            }
            bw.close();
        } catch (IOException e) {
            PuppeTorException ex = new PuppeTorException(e);
            this.logger.throwing(this.getClass().getName(),
                    "writeConfiguration", ex);
            throw ex;
        }

        // invoke overridden method
        super.writeConfiguration();

        // log exiting
        this.logger.exiting(this.getClass().getName(), "writeConfiguration");
    }

    /**
     * Template configuration of directory nodes.
     */
    static List<String> templateConfiguration;

    static {
        templateConfiguration = new ArrayList<String>();

        // configure this node as an authoritative directory
        templateConfiguration.add("AuthoritativeDirectory 1");
        templateConfiguration.add("V2AuthoritativeDirectory 1");
        templateConfiguration.add("V3AuthoritativeDirectory 1");
        templateConfiguration.add("DirAllowPrivateAddresses 1");
        templateConfiguration.add("MinUptimeHidServDirectoryV2 0 minutes");
    }
}
